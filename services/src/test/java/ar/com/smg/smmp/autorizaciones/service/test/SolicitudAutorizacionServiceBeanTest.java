/**
 * 
 */
package ar.com.smg.smmp.autorizaciones.service.test;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;

import ar.com.smg.smmp.autorizaciones.service.implementations.SolicitudAutorizacionServiceBean;
import ar.com.smg.smmp.autorizaciones.service.implementations.TiposServiceBean;
import ar.com.smg.smmp.model.autorizaciones.solicitud.Estado;
import ar.com.smg.smmp.model.autorizaciones.solicitud.SolicitudAutorizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAutorizacion;

/**
 * @author MaMilicich
 *
 */
public class SolicitudAutorizacionServiceBeanTest {

	
	private SolicitudAutorizacionServiceBean solicitudAutorizacionService;
	
	private static final Logger LOGGER = Logger.getLogger(SolicitudAutorizacionServiceBeanTest.class);
	
	@Before
	public void init() throws Exception {
		
		this.solicitudAutorizacionService = new SolicitudAutorizacionServiceBean();

	}

	
	/**
	 * Test method for {@link ar.com.smg.smmp.autorizaciones.service.implementations.SolicitudAutorizacionServiceBean#registrarSolicitudAutorizacion(ar.com.smg.smmp.model.autorizaciones.solicitud.SolicitudAutorizacion, java.lang.Integer, java.lang.Integer)}.
	 * @throws IOException 
	 */
	//@Test
	public void testRegistrarSolicitudAutorizacion() throws IOException {
		
		SolicitudAutorizacion solicitudAutorizacion = new SolicitudAutorizacion();
		
		solicitudAutorizacion.setTipoAutorizacion(buildRandomTipoAutorizacion());
		solicitudAutorizacion.setEstado(buildEstado());
		solicitudAutorizacion.setObservacion("JAVA JUnit Test");
		
		Integer etapaId = Integer.valueOf(0);
		Integer workflowId = Integer.valueOf(0);
		
		Integer solicitudID = this.solicitudAutorizacionService.registrarSolicitudAutorizacion(solicitudAutorizacion, etapaId, workflowId);
		
		Assert.assertNotNull(solicitudID);
		Assert.assertTrue(solicitudID > 0);
		
		LOGGER.info("solicitudID: " + solicitudID);
		
	}
	
	
	private Estado buildEstado() {

		Estado estado = new Estado();
		estado.setId(0);

		return estado;
	}
	
	
	private TipoAutorizacion buildRandomTipoAutorizacion() throws IOException {
		
		TiposServiceBean tiposService = new TiposServiceBean();
		
		List<TipoAutorizacion> tiposAutorizacion = tiposService.getTiposAutorizaciones();
		
		return chooseRandom(tiposAutorizacion);
	}

    private TipoAutorizacion chooseRandom(List<TipoAutorizacion> list) {
    	
    	Random randomGenerator = new Random();
    	
        int index = randomGenerator.nextInt(list.size());
        return list.get(index);
    }
}
