package ar.com.smg.smmp.autorizaciones.service.interfaces;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface PresupuestoService {

	List<Map<String, Object>> motivosPresupuesto(Map<String, Object> tipoTrm) throws IOException;

	void saveReqTrmPresupuesto(HashMap<String, Object> dataReqPresupuesto) throws IOException;

	Map<String, Object> reqTrmPresupuesto(Long processId) throws IOException;

	List<Map<String, Object>> tramitesPresupuestoAsociado(Long processId) throws IOException;

	void updateReqTrmPresupuesto(HashMap<String, Object> dataReqPresupuesto) throws IOException;
}
