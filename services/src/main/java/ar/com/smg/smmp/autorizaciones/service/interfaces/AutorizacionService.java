package ar.com.smg.smmp.autorizaciones.service.interfaces;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.com.smg.smmp.model.autorizaciones.autorizacion.Internacion;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Prorroga;
import ar.com.smg.smmp.model.autorizaciones.common.DatosEmail;
import ar.com.smg.smmp.model.autorizaciones.gestion.Autorizacion;
import ar.com.smg.smmp.model.autorizaciones.gestion.AutorizacionNormalizado;

public interface AutorizacionService {

	Autorizacion traerAutorizacion(Integer etapaId) throws IOException;
	Prorroga validarProrrogaAutorizacion(Integer sucursalId, Integer numeroAutorizacion, 
	String contrato, String integrante, Integer prepaga) throws IOException;
	List<Internacion> traerInternacionesAutorizacion(Integer sucursalId, Integer numeroAutorizacion) throws IOException;
	List<Prorroga> traerAutorizacionesAsociadas(Integer sucursalId, Integer numeroAutorizacion) throws IOException;
	DatosEmail traerDatosEmailAutorizacion(Integer sucursalId, Integer numeroAutorizacion) throws IOException;
	Integer ingresarProrrogaAutorizacion(Prorroga prorroga) throws IOException;
	AutorizacionNormalizado traerAutorizacionXSucursalYId(Integer sucursal, Integer autorizacionId) throws IOException;
	List<Map<String, Object>> traeAutorizacionConEquipos()throws IOException;
	List<Map<String, Object>> traeEquiposXAutorizacion(Integer sucursal, Integer autorizacionID)throws IOException;
	List<Map<String, Object>> traeModulosXAutorizacion(Integer sucursal, Integer autorizacionID)throws IOException;
	List<Map<String, Object>> obtenerLogsAutorizacionXSucursalYId(Integer sucursal, Integer autorizacionID)throws IOException;
	Map<String, Object> recoverAutoByWorkflowId(Integer workflowId) throws IOException;
	DatosEmail getMailDataPrestador(HashMap<String, Object> params) throws IOException;
	Map<String, Object> expedientesAutorizacion(Integer sucur, Integer auto);
	Map<String, Object> validaTopesPrestacion(HashMap<String, Object> params);
	Map<String, Object> mandatariaByRudiId(Integer rudiId);
}
