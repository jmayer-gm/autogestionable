package ar.com.smg.smmp.autorizaciones.service.implementations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import ar.com.smg.smmp.autorizaciones.persistence.dao.GestionAutorizacionTransactionManager;
import ar.com.smg.smmp.autorizaciones.service.interfaces.ExcepcionAutorizacionServiceLocal;
import ar.com.smg.smmp.autorizaciones.service.interfaces.ExcepcionAutorizacionServiceRemote;
import ar.com.smg.smmp.model.autorizaciones.gestion.Excepcion;

@Stateless
public class ExcepcionAutorizacionServiceBean implements ExcepcionAutorizacionServiceLocal, ExcepcionAutorizacionServiceRemote {

	private static final Logger LOGGER = Logger.getLogger(ExcepcionAutorizacionServiceBean.class);
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void ingresarExcepcionAutorizacion(Excepcion excepcion, Integer etapaId) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("ID etapa: " + etapaId);
		}
		
		map.put("excepcion", excepcion);
		map.put("etapaId", etapaId);
		
		gestionAutorizacionTransactionManager.ingresarExcepcionAutorizacion(map);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void aprobarExcepcionAutorizacion(Excepcion excepcion, Integer etapaId) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("ID etapa: " + etapaId);
			LOGGER.debug("ID Excepcion: " + excepcion.getId());
		}
		
		map.put("excepcion", excepcion);
		map.put("etapaId", etapaId);
		
		gestionAutorizacionTransactionManager.aprobarExcepcionAutorizacion(map);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Excepcion traerExcepcionAutorizacion(Integer etapaId) throws IOException {
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("ID etapa: " + etapaId);
		}
		
		return gestionAutorizacionTransactionManager.traerExcepcionAutorizacion(etapaId);
	}
	
}
