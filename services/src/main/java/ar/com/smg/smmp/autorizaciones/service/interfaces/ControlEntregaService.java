package ar.com.smg.smmp.autorizaciones.service.interfaces;

import java.io.IOException;

import ar.com.smg.smmp.model.autorizaciones.gestion.ControlEntrega;

public interface ControlEntregaService {

	void ingresarControlEntrega(ControlEntrega controlEntrega, Integer etapaId) throws IOException;
	ControlEntrega traerControlEntrega(Integer etapaId) throws IOException;
	
}