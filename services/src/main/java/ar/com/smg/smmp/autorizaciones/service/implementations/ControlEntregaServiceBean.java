package ar.com.smg.smmp.autorizaciones.service.implementations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import ar.com.smg.smmp.autorizaciones.persistence.dao.GestionAutorizacionTransactionManager;
import ar.com.smg.smmp.autorizaciones.service.interfaces.ControlEntregaServiceLocal;
import ar.com.smg.smmp.autorizaciones.service.interfaces.ControlEntregaServiceRemote;
import ar.com.smg.smmp.model.autorizaciones.gestion.ControlEntrega;

@Stateless
public class ControlEntregaServiceBean implements ControlEntregaServiceLocal, ControlEntregaServiceRemote {

	private static final Logger LOGGER = Logger.getLogger(ControlEntregaServiceBean.class);
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void ingresarControlEntrega(ControlEntrega controlEntrega, Integer etapaId) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("ID etapa: " + etapaId);
		}
		
		map.put("controlEntrega", controlEntrega);
		map.put("etapaId", etapaId);
		
		gestionAutorizacionTransactionManager.ingresarControlEntrega(map);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ControlEntrega traerControlEntrega(Integer etapaId) throws IOException {
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("ID etapa: " + etapaId);
		}
		
		return gestionAutorizacionTransactionManager.traerControlEntrega(etapaId);
	}
}
