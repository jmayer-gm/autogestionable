package ar.com.smg.smmp.autorizaciones.service.implementations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import ar.com.smg.smmp.autorizaciones.persistence.dao.AutorizacionTransactionManager;
import ar.com.smg.smmp.autorizaciones.service.interfaces.AutogestionService;;

@Stateless
public class AutogestionableServiceBean implements AutogestionService {

	@Override
	public Map<String, Object> getAutogestionable(Map<String, Object> map) {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		List<Map<String, Object>> coberturaMadre = autorizacionTransactionManager.traerCoberturaMadre(map);
		Map<String, Object> autoge = autorizacionTransactionManager.traerAutogestionable(map);
		if (autoge != null)
			autoge.put("coberturas_madre", coberturaMadre);
		return autoge;
	}

	@Override
	public List<Map<String, Object>> getAutogestionables() {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		return autorizacionTransactionManager.traerAutogestionables();
	}

	@Override
	public Map<String, Object> getTramite(HashMap<String, Object> body, String tramite) {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		return autorizacionTransactionManager.traerTramite(body, tramite);

	}

	@Override
	public List<Map<String, Object>> getDiagnosticos(Map<String, Object> map) {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		return autorizacionTransactionManager.traerDiagnosticos(map);
	}

	@Override
	public List<Map<String, Object>> getCoberturaMadre(Map<String, Object> map) {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		return autorizacionTransactionManager.traerCoberturaMadre(map);
	}

}
