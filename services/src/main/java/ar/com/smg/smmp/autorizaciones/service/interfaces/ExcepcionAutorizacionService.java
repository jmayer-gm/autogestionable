package ar.com.smg.smmp.autorizaciones.service.interfaces;

import java.io.IOException;

import ar.com.smg.smmp.model.autorizaciones.gestion.Excepcion;

public interface ExcepcionAutorizacionService {

	void ingresarExcepcionAutorizacion(Excepcion excpecion, Integer etapaId) throws IOException;
	void aprobarExcepcionAutorizacion(Excepcion excepcion, Integer etapaId)	throws IOException;
	Excepcion traerExcepcionAutorizacion(Integer etapaId) throws IOException;

}
