package ar.com.smg.smmp.autorizaciones.service.interfaces;

import java.io.IOException;

import ar.com.smg.smmp.model.autorizaciones.gestion.Derivacion;

public interface DerivacionService {

	void ingresarDerivacionMedica(Derivacion derivacion, Integer etapaId) throws IOException;
	Derivacion traerDerivacionMedica(Integer etapaId) throws IOException;
	
}
