package ar.com.smg.smmp.autorizaciones.service.implementations;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import ar.com.smg.smmp.autorizaciones.persistence.dao.PresupuestoTransactionManager;
import ar.com.smg.smmp.autorizaciones.service.interfaces.PresupuestoServiceLocal;
import ar.com.smg.smmp.autorizaciones.service.interfaces.PresupuestoServiceRemote;

@Stateless
public class PresupuestoServiceBean implements PresupuestoServiceLocal, PresupuestoServiceRemote {

	private PresupuestoTransactionManager presupuestoTransactionManager;

	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> motivosPresupuesto(Map<String, Object> tipoTrm) throws IOException {
		presupuestoTransactionManager = new PresupuestoTransactionManager();
		return presupuestoTransactionManager.motivosPresupuesto(tipoTrm);
	}

	@Override
	public void saveReqTrmPresupuesto(HashMap<String, Object> dataReqPresupuesto) throws IOException {
		presupuestoTransactionManager = new PresupuestoTransactionManager();
		presupuestoTransactionManager.saveReqTrmPresupuesto(dataReqPresupuesto);
		
	}

	@Override
	public void updateReqTrmPresupuesto(HashMap<String, Object> dataReqPresupuesto) throws IOException {
		presupuestoTransactionManager = new PresupuestoTransactionManager();
		presupuestoTransactionManager.updateReqTrmPresupuesto(dataReqPresupuesto);
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Object> reqTrmPresupuesto(Long processId) throws IOException {
		presupuestoTransactionManager = new PresupuestoTransactionManager();
		return presupuestoTransactionManager.reqTrmPresupuesto(processId);
	}


	@Override
	public List<Map<String, Object>> tramitesPresupuestoAsociado(Long processId) throws IOException {
		presupuestoTransactionManager = new PresupuestoTransactionManager();
		return presupuestoTransactionManager.tramitesPresupuestoAsociado(processId);
	}
}
