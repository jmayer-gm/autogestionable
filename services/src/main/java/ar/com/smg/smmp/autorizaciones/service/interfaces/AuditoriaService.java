package ar.com.smg.smmp.autorizaciones.service.interfaces;

import java.io.IOException;

import ar.com.smg.smmp.model.autorizaciones.gestion.Auditoria;

public interface AuditoriaService {

	void ingresarAuditoriaMedica(Auditoria auditoria, Integer etapaId) throws IOException;
	Auditoria traerAuditoriaMedica(Integer etapaId) throws IOException;
	
}
