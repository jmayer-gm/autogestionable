package ar.com.smg.smmp.autorizaciones.service.implementations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import ar.com.smg.smmp.PropertiesManager;
import ar.com.smg.smmp.PropertiesManagerJNDIBean;
import ar.com.smg.smmp.autorizaciones.persistence.dao.AutorizacionTransactionManager;
import ar.com.smg.smmp.autorizaciones.persistence.dao.GestionAutorizacionTransactionManager;
import ar.com.smg.smmp.autorizaciones.persistence.dao.SupportTransactionManager;
import ar.com.smg.smmp.autorizaciones.service.interfaces.AutorizacionServiceLocal;
import ar.com.smg.smmp.autorizaciones.service.interfaces.AutorizacionServiceRemote;
import ar.com.smg.smmp.autorizaciones.service.util.ServiceHelper;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Internacion;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Prorroga;
import ar.com.smg.smmp.model.autorizaciones.common.DatosEmail;
import ar.com.smg.smmp.model.autorizaciones.gestion.Autorizacion;
import ar.com.smg.smmp.model.autorizaciones.gestion.AutorizacionNormalizado;

@Stateless
public class AutorizacionServiceBean implements AutorizacionServiceLocal, AutorizacionServiceRemote {

	private static final Logger LOGGER = Logger.getLogger(AutorizacionServiceBean.class);
	
	private static final String SUCURSAL_ID = "sucursalId";
	private static final String ID_SUCURSAL = "Id sucursal: ";
	private static final String AUTORIZACION_NUMERO = "Autorizacion Nro: ";
	private static final String NUMERO_AUTORIZACION = "numeroAutorizacion";

	private static final char ITEMS_SEPARATOR = '&';
	private static final char KEY_VALUE_SEPARATOR = '=';
	
	private static final String  PDF_ROOT_URL = "svc-smmp-autorizaciones-rest-api.pdf-root-url";
	
	private PropertiesManager propertiesManager = new PropertiesManagerJNDIBean();
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Autorizacion traerAutorizacion(Integer etapaId) throws IOException {
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("ID etapa: " + etapaId);
		}
		
		return gestionAutorizacionTransactionManager.traerAutorizacion(etapaId);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Internacion> traerInternacionesAutorizacion(Integer sucursalId, Integer numeroAutorizacion) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(ID_SUCURSAL + sucursalId);
			LOGGER.debug(AUTORIZACION_NUMERO + numeroAutorizacion);
		}
		
		map.put(SUCURSAL_ID, sucursalId);
		map.put(NUMERO_AUTORIZACION, numeroAutorizacion);
		map.put("opcion", "T"); //T: Todas; P: Parcial (solo salas con dias autorizados)
		
		return gestionAutorizacionTransactionManager.traerInternacionesAutorizacion(map);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Prorroga> traerAutorizacionesAsociadas(Integer sucursalId, Integer numeroAutorizacion) throws IOException {
		List<Map<String, Object>> autorizacionesAsociadasConSala = null;
		List<Prorroga> autorizacionesAsociadas = null;
		
		Map<String, Object> map = new HashMap<String, Object>();
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(ID_SUCURSAL + sucursalId);
			LOGGER.debug(AUTORIZACION_NUMERO + numeroAutorizacion);
		}
		
		map.put(SUCURSAL_ID, sucursalId);
		map.put(NUMERO_AUTORIZACION, numeroAutorizacion);
		
		autorizacionesAsociadasConSala = gestionAutorizacionTransactionManager.traerAutorizacionesAsociadasConSala(map);
		autorizacionesAsociadas = gestionAutorizacionTransactionManager.traerAutorizacionesAsociadas(map);
		
		return this.construirListadoProrrogas(autorizacionesAsociadas, autorizacionesAsociadasConSala);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Prorroga validarProrrogaAutorizacion(Integer sucursalId, Integer numeroAutorizacion, 
			String contrato, String integrante, Integer prepaga) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(ID_SUCURSAL + sucursalId);
			LOGGER.debug(AUTORIZACION_NUMERO + numeroAutorizacion);
			LOGGER.debug("Contrato: " + contrato);
			LOGGER.debug("Integrante: " + integrante);
			LOGGER.debug("prepaga: " + prepaga);
		}
		
		map.put(SUCURSAL_ID, sucursalId);
		map.put(NUMERO_AUTORIZACION, numeroAutorizacion);
		map.put("contrato", contrato);
		map.put("integrante", integrante);
		map.put("prepaga", prepaga);
		
		return gestionAutorizacionTransactionManager.validarProrrogaAutorizacion(map);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DatosEmail traerDatosEmailAutorizacion(Integer sucursalId, Integer numeroAutorizacion) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		List<String> mensajes = null;
		
		SupportTransactionManager supportTransactionManager = new SupportTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(ID_SUCURSAL + sucursalId);
			LOGGER.debug(AUTORIZACION_NUMERO + numeroAutorizacion);
		}
		
		map.put(SUCURSAL_ID, sucursalId);
		map.put(NUMERO_AUTORIZACION, numeroAutorizacion);
		
		mensajes = supportTransactionManager.traerDatosEmailAutorizacion(map);
		
		return ServiceHelper.buildDatosEmail(mensajes, ITEMS_SEPARATOR, KEY_VALUE_SEPARATOR);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DatosEmail getMailDataPrestador(HashMap<String, Object> params) throws IOException {
		List<String> mensajes = null;
		
		SupportTransactionManager supportTransactionManager = new SupportTransactionManager();
		
		mensajes = supportTransactionManager.getMailDataPrestador(params);
		
		return ServiceHelper.buildDatosEmail(mensajes, ITEMS_SEPARATOR, KEY_VALUE_SEPARATOR);
	}

	@Override
	@TransactionAttribute(value=TransactionAttributeType.REQUIRED)
	public Integer ingresarProrrogaAutorizacion(Prorroga prorroga) throws IOException {
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(ID_SUCURSAL + prorroga.getSucursalId());
			LOGGER.debug(AUTORIZACION_NUMERO + prorroga.getNumero());
		}
		
		gestionAutorizacionTransactionManager.ingresarProrrogaAutorizacion(prorroga);
		
		this.ingresarSalasAutorizacion(prorroga);
		
		return prorroga.getNumeroAutorizacion();
	}
	
	public AutorizacionNormalizado traerAutorizacionXSucursalYId(Integer sucursalId, Integer autorizacionId) throws IOException{
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("traerAutorizacion Sucursal: " + sucursalId +" ID: "+ autorizacionId);
		}
		
		AutorizacionNormalizado autorizacion = autorizacionTransactionManager.traerAutorizacionXSucursalYId(sucursalId, autorizacionId);
		
		if (autorizacion != null) {
			autorizacion.setPdfUrl(buildPdfUrl(sucursalId, autorizacionId));
		}
		
		return autorizacion;
	}

	@TransactionAttribute(value=TransactionAttributeType.REQUIRED)
	private void ingresarSalasAutorizacion(Prorroga prorroga) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		map.put("prorroga", prorroga);

		for (Internacion internacion : prorroga.getInternaciones()) {
			if (internacion.getDiasProrrogados() != null && internacion.getDiasProrrogados() > 0) {
				map.put("internacion", internacion);	
				gestionAutorizacionTransactionManager.ingresarSalaAutorizacion(map);				
			}
		}
	}
	
	private List<Prorroga> construirListadoProrrogas(List<Prorroga> autorizacionesAsociadas,
			List<Map<String, Object>> autorizacionesAsociadasConSala) {
		Integer sucursalId = null;
		Integer numeroAutorizacion = null;
		Internacion internacion = null;
		
		if (autorizacionesAsociadas != null && autorizacionesAsociadasConSala != null) {
			for (Prorroga prorroga : autorizacionesAsociadas) {
				for (Map<String, Object> unMapa : autorizacionesAsociadasConSala) {
					sucursalId = (Integer) unMapa.get(SUCURSAL_ID);
					numeroAutorizacion = (Integer) unMapa.get("numero");
					internacion = new Internacion();
					internacion.getSala().setId((Integer) unMapa.get("salaId"));
					internacion.getSala().setDescripcion((String) unMapa.get("salaDescripcion"));
					internacion.setDiasPorAutorizacionesAsociadas((Integer) unMapa.get("diasPorAutorizacionesAsociadas"));
					if (prorroga.getSucursalId().equals(sucursalId) 
							&& prorroga.getNumero().equals(numeroAutorizacion)) {
						if (prorroga.getInternaciones() == null) {
							prorroga.setInternaciones(new ArrayList<Internacion>());
						}
						prorroga.getInternaciones().add(internacion);
					}
				}
			}
		}
		return autorizacionesAsociadas;
	}
	
	
	@Override
	public List<Map<String, Object>> traeAutorizacionConEquipos() throws IOException {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		return autorizacionTransactionManager.traeAutorizacionConEquipos();
	}
	
	
	
	/**
	 * Construimos la URL del PDF asociado a una Autorizacion (es un servicio externo)
	 * 
	 * @param urlBase
	 * @param sucur
	 * @param auto
	 * @return
	 */
	private String buildPdfUrl(Integer sucur, Integer auto) {

		// Obtenemos la URL raiz del Servicio de PDF Autorizaciones
		String pdfRootUrl = propertiesManager.getValue(PDF_ROOT_URL);
		
		if (pdfRootUrl == null || pdfRootUrl.trim().equals("")) {
			LOGGER.warn("Atencion! no se encontró valor de configuración para la DB Property [" + PDF_ROOT_URL + "]");
		}

		return pdfRootUrl + auto + "/" + sucur + "/pdf";
	}

	@Override
	public List<Map<String, Object>> traeEquiposXAutorizacion(Integer sucursal, Integer autorizacionID) throws IOException {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		return autorizacionTransactionManager.buscarEquiposXAutorizacion(sucursal, autorizacionID);
	}
	
	@Override
	public List<Map<String, Object>> traeModulosXAutorizacion(Integer sucursal, Integer autorizacionID) throws IOException {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		return autorizacionTransactionManager.buscarModulosXAutorizacion(sucursal, autorizacionID);
	}
	
	@Override
	public List<Map<String, Object>> obtenerLogsAutorizacionXSucursalYId(Integer sucursal, Integer autorizacionID) throws IOException {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		return autorizacionTransactionManager.obtenerLogsAutorizacionXSucursalYId(sucursal, autorizacionID);
	}

	@Override
	public Map<String, Object> recoverAutoByWorkflowId(Integer workflowId) throws IOException {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		return autorizacionTransactionManager.recoverAutoByWorkflowId(workflowId);
	}
	
	@Override
	public Map<String, Object> expedientesAutorizacion(Integer sucur, Integer auto) {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sucur", sucur);
		map.put("auto", auto);
		map.put("expeOcuenta", "C");
		map.put("cuenta", null);
		autorizacionTransactionManager.expedientesAutorizacion(map);
		return map;
	}

	@Override
	public Map<String, Object> validaTopesPrestacion(HashMap<String, Object> params) {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		return autorizacionTransactionManager.validaTopesPrestacion(params);
	}

	@Override
	public Map<String, Object> mandatariaByRudiId(Integer rudiId) {
		AutorizacionTransactionManager autorizacionTransactionManager = new AutorizacionTransactionManager();
		return autorizacionTransactionManager.mandatariaByRudiId(rudiId);
	}

}
