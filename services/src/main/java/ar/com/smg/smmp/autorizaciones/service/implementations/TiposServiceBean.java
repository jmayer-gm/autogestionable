package ar.com.smg.smmp.autorizaciones.service.implementations;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import ar.com.smg.smmp.autorizaciones.persistence.dao.TipoTransactionManager;
import ar.com.smg.smmp.autorizaciones.service.interfaces.TiposServiceLocal;
import ar.com.smg.smmp.autorizaciones.service.interfaces.TiposServiceRemote;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoExcepcion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoFinalizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoRechazo;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAgrupacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAutorizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoGestion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoPaciente;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoProvisionObraSocial;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoResolucion;

@Stateless
public class TiposServiceBean implements TiposServiceLocal, TiposServiceRemote {

	private TipoTransactionManager tipoTransactionManager;

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<TipoAutorizacion> getTiposAutorizaciones() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeTiposAutorizacion();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<TipoGestion> getTiposGestion() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeTiposGestion();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<TipoPaciente> getTiposPaciente() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeTiposPaciente();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<TipoProvisionObraSocial> getTiposProvisionObraSocial() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeTiposProvisionObraSocial();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<TipoAgrupacion> getTiposAgrupacion() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeTiposAgrupacion();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<TipoResolucion> getTiposResolucion() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeTiposResolucion();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<TipoResolucion> getTiposResolucionST() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeTiposResolucionST();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<MotivoExcepcion> getMotivosExcepcion() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeMotivosExcepcion();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<MotivoRechazo> getMotivosRechazo() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeMotivosRechazo();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<MotivoFinalizacion> getMotivosFinalizacion(String tarea) throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeMotivosFinalizacion(tarea);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> getTiposPracticaOdo() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeTiposPracticaOdo();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> getMotivosFinalizacionOdo(Integer codigo, Integer grupo) throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeMotivosFinalizacionOdo(codigo, grupo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> getResolucionesOdo(Integer codigo) throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeResolucionesOdo(codigo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> traeMotivosNoReprogramacionCirugia() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeMotivosNoReprogramacionCirugia();
	}

	@Override
	public List<Map<String, Object>> traeEstudiosAdjuntosMdc(Long fecha) throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeEstudiosAdjuntosMdc(fecha);
	}

	@Override
	public List<Map<String, Object>> traeObrasSocialesMdc() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.traeObrasSocialesMdc();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> estadoAutorizacionesSGI() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.estadoAutorizacionesSGI();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> clasificacionAutorizacionesSGI() throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.clasificacionAutorizacionesSGI();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> obtenerMandatariasAutorizacionesSGI(String autorizaciones) throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.obtenerMandatariasAutorizacionesSGI(autorizaciones);
	}
	
	@Override
	public List<Map<String, Object>> obtenerEstudiosPorTipoAutorizacionSGI(Long fecha, String tipoAutorizacion) throws IOException {
		tipoTransactionManager = new TipoTransactionManager();
		return tipoTransactionManager.obtenerEstudiosPorTipoAutorizacionSGI(fecha, tipoAutorizacion);
	}
	
	@Override
	public List<Map<String, Object>> getSubmotivosFinalizacion(String tarea) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		this.tipoTransactionManager = new TipoTransactionManager();
		map.put("tarea", tarea);
		return tipoTransactionManager.traeSubmotivosFinalizacion(map);
	}
	
}
