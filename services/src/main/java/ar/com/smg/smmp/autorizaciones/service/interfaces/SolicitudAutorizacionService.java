package ar.com.smg.smmp.autorizaciones.service.interfaces;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.com.smg.smmp.model.autorizaciones.autorizacion.Internacion;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Prorroga;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Sala;
import ar.com.smg.smmp.model.autorizaciones.solicitud.SolicitudAutorizacion;
import ar.com.smg.smmp.model.global.Documentacion;
import ar.com.smg.smmp.model.prestadores.farmacia.Farmacia;

public interface SolicitudAutorizacionService {
	
	static final String DEFAULT_PROVIN = "00";
	static final String DEFAULT_REGION = "0000";
	static final String DEFAULT_ZONA = "0000";
	static final String DEFAULT_BARRIO = "00000";

	/**
	 * Se registra una solicitud de autorizacion.
	 * 
	 * @param solicitudAutorizacion la solicitud
	 * @param etapaId id de la etapa generada previamente
	 * @param workflowId id del workflow asociado al contacto
	 * @throws IOException
	 * 
	 * Se utiliza para registrar una solicitud con mas datos que los minimos requeridos
	 * 
	 * DEVUELVE UN Integer: El ID de la Solicitud registrada.
	 * 
	 */
	Integer registrarSolicitudAutorizacion(SolicitudAutorizacion solicitudAutorizacion, Integer etapaId, 
			Integer workflowId) throws IOException;
	
	/**
	 * Se ingresan datos propios de la solicitud.
	 * 
	 * @param solicitudAutorizacion la solicitud
	 * @param etapaId id de la etapa generada previamente
	 * @param workflowId id del workflow asociado al contacto
	 * @throws IOException
	 * 
	 * Se debe utilizar cuando se ingresa una solicitud solo con datos requeridos
	 * 
	 * DEVUELVE UN Integer: El ID de la Solicitud registrada.
	 * 
	 */
	Integer ingresarSolicitudAutorizacion(SolicitudAutorizacion solicitudAutorizacion, Integer etapaId, 
			Integer workflowId) throws IOException;
	
	/**
	 * Se actualiza la solicitud con el id de la tarea.
	 * 
	 * @param etapaId id de la etapa generada previamente
	 * @param tareaId id de la tarea
	 * @throws IOException
	 * 
	 * Se debe utilizar cuando se desea actualizar la solicitud con el id de la tarea que se genera en el bpm
	 * 
	 */
	void actualizarSolicitudAutorizacion(Integer etapaId, Long tareaId)
			throws IOException;
	
	/**
	 * Trae la solicitud de autorizacion.
	 * 
	 * @param etapaId id de la etapa
	 * @return SolicitudAutorizacion la solicitud
	 * @throws IOException
	 */
	SolicitudAutorizacion traerSolicitudAutorizacion(Integer etapaId) throws IOException;
	
	/**
	 * Trae la solicitud de autorizacion que aun no se ha persistido dentro de una transaccion.
	 * 
	 * @param etapaId id de la etapa
	 * @return SolicitudAutorizacion la solicitud
	 * @throws IOException
	 */
	SolicitudAutorizacion traerSolicitudAutorizacionTx(Integer etapaId) throws IOException;

	/**
	 * Trae las solicitudes pendientes.
	 * 
	 * @param prepaga id del afiliado
	 * @param contrato del afiliado
	 * @param numero de integrante del afiliado
	 * @param fechaInicio fecha de inicio del intervalo de busqueda
	 * @param fechaFin fecha de fin del intervalo de busqueda. Debe ser al menos 3 meses mayor que la fecha de inicio
	 * @return Listado de SolicitudAutorizacion
	 * @throws IOException
	 */
	List<SolicitudAutorizacion> traerSolicitudesPendientes(Integer prepaga, String contra, String inte, 
			Date fechaInicio, Date fechaFin) throws IOException;

	
	
	Prorroga validarProrroga(Integer sucursalId, Integer numeroAutorizacion, Integer prepaga, String contrato, String integrante) throws IOException;
	
	List<Prorroga> traerAutorizacionesAsociadas(Integer sucursalId, Integer numeroAutorizacion) throws IOException;

	List<Sala> traerSalas(Date bajaFecha) throws IOException;

	List<Internacion> traerInternaciones(Integer sucursalId, Integer numeroAutorizacion) throws IOException;

	List<Documentacion> traerDocumentacionSituacionTerapeutica(Integer precarga_id) throws IOException;
	
	Map<String, Object> traerAutorizacionesPorPrestador(String fechaDesde, String fechaHasta, Integer prestadorId, String tipo, String estado, Integer dias, Integer pagina, Integer cantRegPorPag) throws IOException;

	List<Map<String, Object>> traerAutorizacionesPorAfiliadoEfector(Integer prepaga, String contra, String inte, String efectorId, Integer sucur, Integer auto, Integer nomen, String prestacion, String fechaConsumo) throws IOException;

	Map<String, Object> tieneAutorizacionVinculable(Integer prepaga, String contra, String inte) throws IOException;
	
	/**
	 * Returns Farmacias by farma and razon
	 * 
	 * @param id
	 * @param razon
	 * @return {@link List<Farmacia>}
	 */
	List<Farmacia> traerFarmacias(Integer manda, Integer id, String razon);
	
	/**
	 * Returns Farmacias by location
	 * 
	 * @param manda
	 * @param provin
	 * @param loca
	 * @param razon 
	 * @return {@link List<Farmacia>}
	 */
	List<Farmacia> traerFarmaciasByUbicacion(Integer manda, String provin, String loca, String razon);

	List<Map<String, Object>> traerAutorizacionesOdontoAfiliado(String contra, Integer prepaga, String inte, String fechaDesde, String fechaHasta) throws IOException;

	Map<String, Object> traerDatosCabAutorizacionesOdo(Integer auto, Integer sucur) throws IOException;

	List<Map<String, Object>> traerPrestacionesAutorizacionesOdo(Integer auto, Integer sucur) throws IOException;
	
	List<Map<String, Object>> traerAutorizacionesOdontoPrestador(Integer codigoPrestador, String fechaDesde, String fechaHasta) throws IOException;

	List<Map<String, Object>> validarAutorizacionesSolapadas(HashMap<String, Object> params);

	Map<String, Object> validarConvenio(HashMap<String, Object> params);

	Map<String, Object> validarConvenioInter(HashMap<String, Object> params);

	Map<String, Object> validarConvenioCiru(HashMap<String, Object> params);
	
	Map<String, Object> validarCobertura(HashMap<String, Object> params);
	
	Map<String, Object> validarGeneroYEdad(HashMap<String, Object> params);

	Map<String, Object> validarCarencia(HashMap<String, Object> params);

	Map<String, Object> traeCopagoPrestacion(HashMap<String, Object> params);
	
	List<Map<String, Object>> traerHistorialValorLey(Integer nomen, String prestacion);

	List<Map<String, Object>> traerEstudiosTramite(Integer workflowId) throws IOException;

	List<Map<String, Object>> plnsConfigModulos(Map<String, Object> map) throws IOException;

	List<Map<String, Object>> traerClasificaciones(Integer sucursalId, Integer numeroAutorizacion);

	List<Map<String, Object>> traePrestacionesAutorizacion(Integer sucursalId, Integer numeroAutorizacion);
	
	List<Map<String, Object>> traerDescripcionRecha(Integer recha, Integer manual);

	void auditarAutorizacion(Map<String, Object> map)throws IOException;

	Map<String, Object> obtenerProceDtoProducto(HashMap<String, Object> params);

	Map<String, Object> validarProducto(HashMap<String, Object> params);
	
	Map<String, Object> registraEnvioAFarmalink(Map<String, Object> params);

	List<Map<String, Object>> traerRazonesOdo();

	List<Map<String, Object>> traerMotivosOdo(Map<String, Object> params);
	
	Map<String, Object> obtenerConversionProductoSap(HashMap<String, Object> params);
}
