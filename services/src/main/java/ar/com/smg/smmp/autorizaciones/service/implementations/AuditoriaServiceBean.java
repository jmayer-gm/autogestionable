package ar.com.smg.smmp.autorizaciones.service.implementations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import ar.com.smg.smmp.autorizaciones.persistence.dao.GestionAutorizacionTransactionManager;
import ar.com.smg.smmp.autorizaciones.service.interfaces.AuditoriaServiceLocal;
import ar.com.smg.smmp.autorizaciones.service.interfaces.AuditoriaServiceRemote;
import ar.com.smg.smmp.model.autorizaciones.gestion.Auditoria;

@Stateless
public class AuditoriaServiceBean implements AuditoriaServiceLocal, AuditoriaServiceRemote {

	private static final Logger LOGGER = Logger.getLogger(AuditoriaServiceBean.class);
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void ingresarAuditoriaMedica(Auditoria auditoria, Integer etapaId) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("ID etapa: " + etapaId);
		}
		
		map.put("auditoria", auditoria);
		map.put("etapaId", etapaId);
		
		gestionAutorizacionTransactionManager.ingresarAuditoriaMedica(map);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Auditoria traerAuditoriaMedica(Integer etapaId) throws IOException {
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("ID etapa: " + etapaId);
		}
		
		return gestionAutorizacionTransactionManager.traerAuditoriaMedica(etapaId);
	}
	
}