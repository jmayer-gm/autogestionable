package ar.com.smg.smmp.autorizaciones.service.implementations;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import ar.com.smg.smmp.autorizaciones.persistence.dao.GestionAutorizacionTransactionManager;
import ar.com.smg.smmp.autorizaciones.service.interfaces.SolicitudAutorizacionServiceLocal;
import ar.com.smg.smmp.autorizaciones.service.interfaces.SolicitudAutorizacionServiceRemote;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Internacion;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Prorroga;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Sala;
import ar.com.smg.smmp.model.autorizaciones.solicitud.SolicitudAutorizacion;
import ar.com.smg.smmp.model.global.Documentacion;
import ar.com.smg.smmp.model.prestadores.LugarDeAtencion;
import ar.com.smg.smmp.model.prestadores.farmacia.Farmacia;
import ar.com.smg.smmp.model.prestadores.medicoasistencial.PrestadorMedicoAsistencial;
import ar.com.smg.smmp.model.prestadores.medicoasistencial.PrestadorMedicoAsistencialProfesional;

@Stateless
public class SolicitudAutorizacionServiceBean implements SolicitudAutorizacionServiceLocal, SolicitudAutorizacionServiceRemote {

	private static final Logger LOGGER = Logger.getLogger(SolicitudAutorizacionServiceBean.class);

	private static final String TIPO_PROFESIONAL_EFECTOR = "E";
	private static final String TIPO_PROFESIONAL_PRESCRIPTOR = "P";
	private static final int RANGO_MESES_BUSQUEDA = 3;
	private static final String KEY_ETAPAID = "etapaId";

	private static final String PREPAGA = "prepaga";
	private static final String CONTRA = "contra";
	private static final String INTE = "inte";
	private static final String EFECTOR_ID = "efectorId";
	private static final String SUCUR = "sucur";
	private static final String AUTO = "auto";
	private static final String NOMEN = "nomen";
	private static final String PRESTACION = "prestacion";
	private static final String FECHA_CONSUMO = "fechaConsumo";
		
	private static final String PERCENTEAGE = "%";

	private static final String MENSAJE_INGRESO_SOLICITUD = "Ingresando la solicitud de autorizacion con id de etapa {0}";
	private static final String MENSAJE_ACTUALIZACION_SOLICITUD = "Actualizando la solicitud de autorizacion con id de tarea {0} par el id de etapa {1}";
	private static final String MENSAJE_INGRESO_PRESTADOR = "Ingresando los datos del prestador de la solicitud para el id de etapa {0}";
	private static final String MENSAJE_INGRESO_PROFESIONAL = "Ingresando los datos del profesional de la solicitud para el id de etapa {0}";
	private static final String MENSAJE_DATOS_SOLICITUD = "Se trae la solicitud de autorizacion para la etapa con id: {0}";
	private static final String MENSAJE_DATOS_PRESTADOR = "Se traen los datos del prestador para  la solicitud con id de etapa: {0}";
	private static final String MENSAJE_DATOS_PROFESIONALES = "Se traen los datos de los profesionales para  la solicitud con id de etapa: {0}";
	private static final String MENSAJE_SOLICITUDES_PENDIENTES = "Se traen las solicitudes pendientes para el afiliado con prepaga: {0}, contrato: {1}, integrante: {2}";

	private GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager;

	private GestionAutorizacionTransactionManager getGestionAutorizacionTransactionManager() {
		if (gestionAutorizacionTransactionManager == null) {
			gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		}
		return gestionAutorizacionTransactionManager;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer registrarSolicitudAutorizacion(SolicitudAutorizacion solicitudAutorizacion, 
			Integer etapaId, Integer workflowId) throws IOException {
		LOGGER.info( new StringBuilder("Registrando la solicitud de autorizacion..." )
		.append(workflowId)
		.toString()
				);

		Integer solicitudId = this.ingresarSolicitudAutorizacion(solicitudAutorizacion, etapaId, workflowId);

		PrestadorMedicoAsistencial pma = solicitudAutorizacion.getPrestador();
		LugarDeAtencion lat = solicitudAutorizacion.getLugarDeAtencionPrestador();
		if (pma.getId() != null) {
			this.ingresarPrestador(pma, lat, etapaId);
		}

		PrestadorMedicoAsistencialProfesional pmap = solicitudAutorizacion.getProfesionalEfector();
		LugarDeAtencion latp = solicitudAutorizacion.getLugarDeAtencionProfesionalEfector();
		if (pmap.getId() != null || pmap.getApeRazonSocial() != null) {
			this.ingresarProfesional(TIPO_PROFESIONAL_EFECTOR, pmap, latp, etapaId);
		}

		PrestadorMedicoAsistencialProfesional pmae = solicitudAutorizacion.getProfesionalPrescriptor();
		LugarDeAtencion late = solicitudAutorizacion.getLugarDeAtencionProfesionalPrescriptor();
		if (pmae.getId() != null || pmae.getApeRazonSocial() != null) {
			this.ingresarProfesional(TIPO_PROFESIONAL_PRESCRIPTOR, pmae, late, etapaId);
		}
		
		return solicitudId;
		
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer ingresarSolicitudAutorizacion(SolicitudAutorizacion solicitudAutorizacion, Integer etapaId, Integer workflowId) throws IOException {		
		Map<String, Object> map = new HashMap<String, Object>();

		LOGGER.info(MessageFormat.format(MENSAJE_INGRESO_SOLICITUD, etapaId));

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(new StringBuilder("ID etapa: ").append(etapaId)
					.append("ID workflow: ").append(workflowId)
					.append("ID Tipo Autorizacion: ").append(
							(solicitudAutorizacion != null && solicitudAutorizacion.getTipoAutorizacion() != null) 
							? solicitudAutorizacion.getTipoAutorizacion().getId() : null
							)
							.toString()
					);
		}

		map.put(KEY_ETAPAID, etapaId);
		map.put("workflowId", workflowId);
		map.put("solicitudAutorizacion", solicitudAutorizacion);

		Integer solicitudId = this.getGestionAutorizacionTransactionManager().ingresarSolicitudAutorizacion(map);
		
		LOGGER.info("Etapa generada: " + etapaId + " | Solicitud ID: " + solicitudId);
		
		return solicitudId;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void actualizarSolicitudAutorizacion(Integer etapaId, Long tareaId) throws IOException {		
		Map<String, Object> map = new HashMap<String, Object>();

		LOGGER.info(MessageFormat.format(MENSAJE_ACTUALIZACION_SOLICITUD, tareaId, etapaId));

		map.put(KEY_ETAPAID, etapaId);
		map.put("tareaId", tareaId);

		this.getGestionAutorizacionTransactionManager().actualizarSolicitudAutorizacion(map);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void ingresarPrestador(PrestadorMedicoAsistencial prestador, LugarDeAtencion lugarAtencion, 
			Integer etapaId) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();

		LOGGER.info(MessageFormat.format(MENSAJE_INGRESO_PRESTADOR, etapaId));

		map.put(KEY_ETAPAID, etapaId);
		map.put("prestadorId", prestador.getId());
		map.put("telefono", lugarAtencion.getTelefono());
		map.put("lugarAtencionId", lugarAtencion.getId());

		this.getGestionAutorizacionTransactionManager().ingresarPrestadorSolicitudAutorizacion(map);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void ingresarProfesional(String tipoProfesional, PrestadorMedicoAsistencialProfesional profesional, 
			LugarDeAtencion lugarAtencion, Integer etapaId) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();

		LOGGER.info(MessageFormat.format(MENSAJE_INGRESO_PROFESIONAL, etapaId));

		map.put("tipoProfesional", tipoProfesional);
		map.put("matricula", profesional.getNumeroMatriculaNacional());
		map.put("apellidoNombre", profesional.getApeRazonSocial());
		map.put("profesionalId", profesional.getId());
		map.put("lugarAtencionId", lugarAtencion.getId());
		map.put("telefono", lugarAtencion.getTelefono());
		map.put("email", profesional.getDireccionEmail());
		map.put(KEY_ETAPAID, etapaId);

		this.getGestionAutorizacionTransactionManager().ingresarProfesionalSolicitud(map);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SolicitudAutorizacion traerSolicitudAutorizacion(Integer etapaId) throws IOException {
		Map<String, Object> mapPrestador = null;
		Map<String, Object> mapProfesionales = null;
		SolicitudAutorizacion solicitudAutorizacion = null;
		LugarDeAtencion lugarDeAtencionPrestador = null;

		LOGGER.info(MessageFormat.format(MENSAJE_DATOS_SOLICITUD, etapaId));

		// Se traen los datos de la solicitud
		solicitudAutorizacion = this.traerDatosSolicitud(etapaId);

		// Se traen los datos del prestador de la solicitud
		mapPrestador = this.traerDatoPrestadorSolicitud(etapaId);

		if (mapPrestador != null) {
			lugarDeAtencionPrestador = (LugarDeAtencion) mapPrestador.get("lugarDeAtencion");
			solicitudAutorizacion.setPrestador((PrestadorMedicoAsistencial) mapPrestador.get("prestador"));
		}

		if (lugarDeAtencionPrestador != null && lugarDeAtencionPrestador.getId() != null) {
			solicitudAutorizacion.setLugarDeAtencionPrestador(lugarDeAtencionPrestador);
		}

		// Se traen los datos de los profesionales de la solicitud
		mapProfesionales = this.traerDatosProfesionalesSolicitud(etapaId);

		if (mapProfesionales != null) {
			solicitudAutorizacion.setProfesionalEfector((PrestadorMedicoAsistencialProfesional) mapProfesionales.get("profesionalEfector"));
			solicitudAutorizacion.setLugarDeAtencionProfesionalEfector((LugarDeAtencion) mapProfesionales.get("lugarDeAtencionProfesionalEfector"));
			solicitudAutorizacion.setProfesionalPrescriptor((PrestadorMedicoAsistencialProfesional) mapProfesionales.get("profesionalPrescriptor"));
			solicitudAutorizacion.setLugarDeAtencionProfesionalPrescriptor((LugarDeAtencion) mapProfesionales.get("lugarDeAtencionProfesionalPrescriptor"));
		}

		return solicitudAutorizacion;
	}

	@Override
	// Para consultar datos de la solicitud que no se han persistido dureante la transaccion
	public SolicitudAutorizacion traerSolicitudAutorizacionTx(Integer etapaId) throws IOException {
		SolicitudAutorizacion solicitudAutorizacion = this.traerDatosSolicitudTx(etapaId);
		Map<String, Object> mapPrestador = this.traerDatoPrestadorSolicitudTx(etapaId);
		if (mapPrestador != null) {
			solicitudAutorizacion.setPrestador((PrestadorMedicoAsistencial) mapPrestador.get("prestador"));
		}
		return solicitudAutorizacion;
	}

	private SolicitudAutorizacion traerDatosSolicitudTx(Integer etapaId) throws IOException {
		LOGGER.info("Se traen los datos de la solicitud dentro de la tx para el id de etapa: " + etapaId);
		GestionAutorizacionTransactionManager gestionAutorizacionTxManager = new GestionAutorizacionTransactionManager();
		return gestionAutorizacionTxManager.traerSolicitudAutorizacionTx(etapaId);
	}

	private Map<String, Object> traerDatoPrestadorSolicitudTx(Integer etapaId) throws IOException {
		LOGGER.info("Se traen los datos del prestador dentro de la tx para el id de etapa: " + etapaId);
		GestionAutorizacionTransactionManager gestionAutorizacionTxManager = new GestionAutorizacionTransactionManager();
		return gestionAutorizacionTxManager.traerPrestadorSolicitudAutorizacionTx(etapaId);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private SolicitudAutorizacion traerDatosSolicitud(Integer etapaId) throws IOException {
		SolicitudAutorizacion solicitudAutorizacion = null;
		GestionAutorizacionTransactionManager gestionAutorizacionTxManager = new GestionAutorizacionTransactionManager();

		solicitudAutorizacion = gestionAutorizacionTxManager.traerSolicitudAutorizacion(etapaId);

		if (LOGGER.isDebugEnabled()) {			
			LOGGER.debug(MessageFormat.format("ID Solicitud Autorizacion: {0}", 
					(solicitudAutorizacion != null) ? solicitudAutorizacion.getId() : null));
		}

		return solicitudAutorizacion;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private Map<String, Object> traerDatoPrestadorSolicitud(Integer etapaId) throws IOException {
		Map<String, Object> map = null;
		GestionAutorizacionTransactionManager gestionAutorizacionTxManager = new GestionAutorizacionTransactionManager();

		LOGGER.info(MessageFormat.format(MENSAJE_DATOS_PRESTADOR, etapaId));

		map = gestionAutorizacionTxManager.traerPrestadorSolicitudAutorizacion(etapaId);

		return map;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private Map<String, Object> traerDatosProfesionalesSolicitud(Integer etapaId) throws IOException {
		Map<String, Object> map = null;
		GestionAutorizacionTransactionManager gestionAutorizacionTxManager = new GestionAutorizacionTransactionManager();

		LOGGER.info(MessageFormat.format(MENSAJE_DATOS_PROFESIONALES, etapaId));

		map = gestionAutorizacionTxManager.traerProfesionalesSolicitudAutorizacion(etapaId);

		return map;
	}

	@Override
	public List<SolicitudAutorizacion> traerSolicitudesPendientes(Integer prepaga, String contrato, String integrante, 
			Date fechaInicio, Date fechaFin) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();

		//Correcciones Sonar - Para modificar los parametros de entrada
		Date unaFechaInicio = fechaInicio;
		Date unaFechaFin = fechaFin;

		LOGGER.info(MessageFormat.format(MENSAJE_SOLICITUDES_PENDIENTES, prepaga, contrato, integrante));

		// Si no es un intervalo valido se toman 3 meses considerando el dia actual como el inicio del mismo.
		if (!this.esIntervaloValido(unaFechaInicio, unaFechaFin)) {
			unaFechaInicio = new Date();
			Calendar unCalendarFin = Calendar.getInstance();
			unCalendarFin.setTime(unaFechaInicio);
			unCalendarFin.add(Calendar.MONTH, RANGO_MESES_BUSQUEDA);
			unaFechaFin = unCalendarFin.getTime();
		}

		map.put("prepaga", prepaga);
		map.put("contrato", contrato);
		map.put("integrante", integrante);
		map.put("fechaInicio", unaFechaInicio);
		map.put("fechaFin", unaFechaFin);

		return this.getGestionAutorizacionTransactionManager().traerSolicitudesPendientes(map);
	}
	
	private boolean esIntervaloValido(Date fechaInicio, Date fechaFin) {
		if (fechaInicio == null || fechaFin == null || fechaInicio.after(fechaFin)) {
			return false;
		}

		Calendar unaFechaFin = Calendar.getInstance();
		unaFechaFin.setTime(fechaFin);
		unaFechaFin.add(Calendar.MONTH, RANGO_MESES_BUSQUEDA);

		return fechaInicio.before(unaFechaFin.getTime());
	}
	
	@Override
	public Prorroga validarProrroga(Integer sucursalId, Integer numeroAutorizacion, Integer prepaga, String contrato, String integrante) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("sucursalId", sucursalId);
		map.put("numeroAutorizacion", numeroAutorizacion);
		map.put("prepaga", prepaga);
		map.put("contrato", contrato);
		map.put("integrante", integrante);

		return this.getGestionAutorizacionTransactionManager().validarProrrogaAutorizacion(map);
	}

	@Override
	public List<Prorroga> traerAutorizacionesAsociadas(Integer sucursalId, Integer numeroAutorizacion) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("sucursalId", sucursalId);
		map.put("numeroAutorizacion", numeroAutorizacion);

		return this.getGestionAutorizacionTransactionManager().traerAutorizacionesAsociadas(map);
	}

	@Override
	public List<Sala> traerSalas(Date bajaFecha) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bajaFecha", bajaFecha);
		
		return this.getGestionAutorizacionTransactionManager().traerSalas(map);
	}

	@Override
	public List<Internacion> traerInternaciones(Integer sucursalId, Integer numeroAutorizacion) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("sucursalId", sucursalId);
		map.put("numeroAutorizacion", numeroAutorizacion);

		return this.getGestionAutorizacionTransactionManager().traerInternaciones(map);
	}
	
	@Override
	public List<Documentacion> traerDocumentacionSituacionTerapeutica(Integer precarga_id) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("precarga_id", precarga_id);
		
		return this.getGestionAutorizacionTransactionManager().traerDocumentacionSituacionTerapeutica(map);
	}
	
	@Override
	public Map<String, Object> traerAutorizacionesPorPrestador(String fechaDesde, String fechaHasta, Integer prestadorId, String tipo, String estado, Integer dias, Integer pagina, Integer cantRegPorPag) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("fechaDesde", fechaDesde);
		map.put("fechaHasta", fechaHasta);
		map.put("prestadorId", prestadorId);
		map.put("tipo", tipo);
		map.put("estado", estado);
		map.put("dias", dias);
		map.put("pagina", pagina);
		map.put("cantRegPorPag", cantRegPorPag);
		map.put("cantTotalPag", null);
		
		return this.getGestionAutorizacionTransactionManager().traerAutorizacionesPorPrestador(map);
	}

	@Override
	public List<Map<String, Object>> traerAutorizacionesPorAfiliadoEfector(Integer prepaga, String contra, String inte, String efectorId, Integer sucur, Integer auto, Integer nomen, String prestacion, String fechaConsumo) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put(PREPAGA, prepaga);
		map.put(CONTRA, contra);
		map.put(INTE, inte);
		map.put(EFECTOR_ID, efectorId);
		map.put(SUCUR, sucur);
		map.put(AUTO, auto);
		map.put(NOMEN, nomen);
		map.put(PRESTACION, prestacion);
		map.put(FECHA_CONSUMO, fechaConsumo);
		
		return this.getGestionAutorizacionTransactionManager().traerAutorizacionesPorAfiliadoEfector(map);
	}

	@Override
	public Map<String, Object> tieneAutorizacionVinculable(Integer prepaga, String contra, String inte) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put(PREPAGA, prepaga);
		map.put(CONTRA, contra);
		map.put(INTE, inte);
		
		return this.getGestionAutorizacionTransactionManager().tieneAutorizacionVinculable(map);
	}	
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Farmacia> traerFarmacias(Integer manda, Integer id, String razon) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("manda", manda);
		map.put("id", id);
		map.put("razon", razon != null ? PERCENTEAGE + razon + PERCENTEAGE : null);
		return this.getGestionAutorizacionTransactionManager().traerFarmacias(map);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Farmacia> traerFarmaciasByUbicacion(Integer manda, String provin, String loca, String razon) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("manda", manda);
		map.put("razon", razon != null ? PERCENTEAGE + razon + PERCENTEAGE : null);
		map.put("provin", provin != null ? provin : DEFAULT_PROVIN);
		map.put("loca", loca);
		map.put("region", DEFAULT_REGION);
		map.put("zona", DEFAULT_ZONA);
		map.put("barrio", DEFAULT_BARRIO);
		return this.getGestionAutorizacionTransactionManager().traerFarmaciasByUbicacion(map);
	}

	@Override
	public List<Map<String, Object>> traerAutorizacionesOdontoAfiliado(String contra, Integer prepaga, String inte, String fechaDesde, String fechaHasta) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("prepaga", prepaga);
		map.put("contra", contra);
		map.put("inte", inte);
		map.put("fechaDesde", fechaDesde);
		map.put("fechaHasta", fechaHasta);
		return this.getGestionAutorizacionTransactionManager().traerAutorizacionesOdontoAfiliado(map);
	}

	@Override
	public Map<String, Object> traerDatosCabAutorizacionesOdo(Integer auto, Integer sucur) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("auto", auto);
		map.put("sucur", sucur);
		map.put("detalle", "C");
		return this.getGestionAutorizacionTransactionManager().traerDatosCabAutorizacionesOdo(map);
	}

	@Override
	public List<Map<String, Object>> traerPrestacionesAutorizacionesOdo(Integer auto, Integer sucur) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("auto", auto);
		map.put("sucur", sucur);
		map.put("detalle", null);
		return this.getGestionAutorizacionTransactionManager().traerPrestacionesAutorizacionesOdo(map);
	}
	
	@Override
	public List<Map<String, Object>> traerAutorizacionesOdontoPrestador(Integer codigoPrestador, String fechaDesde, String fechaHasta) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("prestad", codigoPrestador);
		map.put("fechaDesde", fechaDesde);
		map.put("fechaHasta", fechaHasta);
		return this.getGestionAutorizacionTransactionManager().traerAutorizacionesOdontoPrestador(map);
	}

	@Override
	public List<Map<String, Object>> validarAutorizacionesSolapadas(HashMap<String, Object> params) {
		return this.getGestionAutorizacionTransactionManager().validarAutorizacionesSolapadas(params);
	}
	
	@Override
	public Map<String, Object> validarConvenio(HashMap<String, Object> params) {
		this.getGestionAutorizacionTransactionManager().validarConvenio(params);
		Map<String, Object> treeMap = new TreeMap<String,Object>(params);
		return treeMap;
	}

	@Override
	public Map<String, Object> validarConvenioInter(HashMap<String, Object> params) {
		this.getGestionAutorizacionTransactionManager().validarConvenioInter(params);
		Map<String, Object> treeMap = new TreeMap<String,Object>(params);
		return treeMap;
	}

	@Override
	public Map<String, Object> validarConvenioCiru(HashMap<String, Object> params) {
		this.getGestionAutorizacionTransactionManager().validarConvenioCiru(params);
		Map<String, Object> treeMap = new TreeMap<String,Object>(params);
		return treeMap;
	}
	
	@Override
	public Map<String, Object> validarCobertura(HashMap<String, Object> params) {
		this.getGestionAutorizacionTransactionManager().validarCobertura(params);
		Map<String, Object> treeMap = new TreeMap<String,Object>(params);
		return treeMap;
	}
	
	@Override
	public Map<String, Object> validarGeneroYEdad(HashMap<String, Object> params) {
		this.getGestionAutorizacionTransactionManager().validarGeneroYEdad(params);
		Map<String, Object> treeMap = new TreeMap<String,Object>(params);
		return treeMap;
	}

	@Override
	public Map<String, Object> validarCarencia(HashMap<String, Object> params) {
		this.getGestionAutorizacionTransactionManager().validarCarencia(params);
		Map<String, Object> treeMap = new TreeMap<String,Object>(params);
		return treeMap;
	}

	@Override
	public Map<String, Object> traeCopagoPrestacion(HashMap<String, Object> params) {
		this.getGestionAutorizacionTransactionManager().traeCopagoPrestacion(params);
		Map<String, Object> treeMap = new TreeMap<String,Object>(params);
		return treeMap;
	}
	
	@Override
	public List<Map<String, Object>> traerHistorialValorLey(Integer nomen, String prestacion) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("nomen", nomen);
		map.put("prestacion", prestacion);
		return this.getGestionAutorizacionTransactionManager().traerHistorialValorLey(map);
	}

	@Override
	public List<Map<String, Object>> traerEstudiosTramite(Integer workflowId) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("workflowId", workflowId);
		return this.getGestionAutorizacionTransactionManager().traerEstudiosTramite(map);
	}
	
	@Override
	public List<Map<String, Object>> plnsConfigModulos(Map<String, Object> map) throws IOException {
		return this.getGestionAutorizacionTransactionManager().plnsConfigModulos(map);
	}

	@Override
	public List<Map<String, Object>> traerClasificaciones(Integer sucur, Integer auto) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sucur", sucur);
		map.put("auto", auto);
		return this.getGestionAutorizacionTransactionManager().traerClasificaciones(map);
	}

	@Override
	public List<Map<String, Object>> traePrestacionesAutorizacion(Integer sucur, Integer auto) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sucur", sucur);
		map.put("auto", auto);
		return this.getGestionAutorizacionTransactionManager().traePrestacionesAutorizacion(map);
	}
	
	@Override
	public List<Map<String, Object>> traerDescripcionRecha(Integer recha, Integer manual) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("recha", recha);
		map.put("manual", manual);
		return this.getGestionAutorizacionTransactionManager().traerDescripcionRecha(map);
	}
	
	@Override
	public void auditarAutorizacion(Map<String, Object> map) throws IOException{
		this.getGestionAutorizacionTransactionManager().auditarAutorizacion(map);
	}

	@Override
	public Map<String, Object> obtenerProceDtoProducto(HashMap<String, Object> params) {
		return this.getGestionAutorizacionTransactionManager().obtenerProceDtoProducto(params);
	}

	@Override
	public Map<String, Object> validarProducto(HashMap<String, Object> params) {
		return this.getGestionAutorizacionTransactionManager().validarProducto(params);
	}
	
	@Override
	public Map<String, Object> registraEnvioAFarmalink(Map<String, Object> params) {
		this.getGestionAutorizacionTransactionManager().registraEnvioAFarmalink(params);
		Map<String, Object> map = new HashMap<String, Object>();
		return map;
	}

	@Override
	public List<Map<String, Object>> traerRazonesOdo() {
		return this.getGestionAutorizacionTransactionManager().traerRazonesOdo();
	}

	@Override
	public List<Map<String, Object>> traerMotivosOdo(Map<String, Object> params) {
		return this.getGestionAutorizacionTransactionManager().traerMotivosOdo(params);
	}
	
	@Override
	public Map<String, Object> obtenerConversionProductoSap(HashMap<String, Object> params) {
		return this.getGestionAutorizacionTransactionManager().obtenerConversionProductoSap(params);
	}

}
