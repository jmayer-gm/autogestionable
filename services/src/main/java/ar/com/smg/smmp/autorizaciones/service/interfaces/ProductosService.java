package ar.com.smg.smmp.autorizaciones.service.interfaces;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ProductosService {

	List<Map<String, Object>> monodroga(String denoDroga) throws IOException;
	List<Map<String, Object>> productosSearch(String productoDeno, String troquel) throws IOException;
	List<Map<String, Object>>productosPresmedSearch(String productoCodigo) throws IOException;
	List<Map<String, Object>> productosByDroga(String droga) throws IOException;
	List<Map<String, Object>> obtenerProductosFarmaciaAutorizacionesSGI(Integer sucursal,Integer autorizacion) throws IOException;
	Map<String, Object> productoByCodigo(String produ) throws IOException;

}

