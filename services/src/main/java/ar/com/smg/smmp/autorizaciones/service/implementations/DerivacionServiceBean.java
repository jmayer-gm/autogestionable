package ar.com.smg.smmp.autorizaciones.service.implementations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import ar.com.smg.smmp.autorizaciones.persistence.dao.GestionAutorizacionTransactionManager;
import ar.com.smg.smmp.autorizaciones.service.interfaces.DerivacionServiceLocal;
import ar.com.smg.smmp.autorizaciones.service.interfaces.DerivacionServiceRemote;
import ar.com.smg.smmp.model.autorizaciones.gestion.Derivacion;

@Stateless
public class DerivacionServiceBean implements DerivacionServiceLocal, DerivacionServiceRemote {

	private static final Logger LOGGER = Logger.getLogger(DerivacionServiceBean.class);
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void ingresarDerivacionMedica(Derivacion derivacion, Integer etapaId) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("ID etapa: " + etapaId);
		}
		
		map.put("derivacion", derivacion);
		map.put("etapaId", etapaId);
		
		gestionAutorizacionTransactionManager.ingresarDerivacionMedica(map);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Derivacion traerDerivacionMedica(Integer etapaId) throws IOException {
		GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("ID etapa: " + etapaId);
		}
		
		return gestionAutorizacionTransactionManager.traerDerivacionMedica(etapaId);
	}
}
