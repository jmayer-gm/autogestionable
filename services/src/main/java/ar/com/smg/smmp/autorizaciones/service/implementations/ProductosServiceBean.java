package ar.com.smg.smmp.autorizaciones.service.implementations;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import ar.com.smg.smmp.autorizaciones.persistence.dao.ProductoTransactionManager;
import ar.com.smg.smmp.autorizaciones.service.interfaces.ProductosServiceLocal;
import ar.com.smg.smmp.autorizaciones.service.interfaces.ProductosServiceRemote;

@Stateless
public class ProductosServiceBean implements ProductosServiceLocal, ProductosServiceRemote {

	private ProductoTransactionManager productoTransactionManager;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> monodroga(String denoDroga) throws IOException {
		productoTransactionManager = new ProductoTransactionManager();
		return productoTransactionManager.monodroga(denoDroga);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> productosSearch(String productoDeno, String troquel) throws IOException {
		productoTransactionManager = new ProductoTransactionManager();
		return productoTransactionManager.productosSearch(productoDeno, troquel);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> productosPresmedSearch(String productoCodigo) throws IOException {
		productoTransactionManager = new ProductoTransactionManager();
		return productoTransactionManager.productosPresmedSearch(productoCodigo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Map<String, Object>> productosByDroga(String droga) throws IOException {
		productoTransactionManager = new ProductoTransactionManager();
		return productoTransactionManager.productosByDroga(droga);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Override
	public List<Map<String, Object>> obtenerProductosFarmaciaAutorizacionesSGI(Integer sucur, Integer auto) throws IOException {
		this.productoTransactionManager = new ProductoTransactionManager();
		return productoTransactionManager.obtenerProductosFarmaciaAutorizacionesSGI(sucur, auto);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Override
	public Map<String, Object> productoByCodigo(String produ) throws IOException {
		this.productoTransactionManager = new ProductoTransactionManager();
		return productoTransactionManager.productoByCodigo(produ);
	}
}
