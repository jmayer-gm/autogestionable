package ar.com.smg.smmp.autorizaciones.service.interfaces;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface AutogestionService {

	Map<String, Object> getAutogestionable(Map<String, Object> map);

	Map<String, Object> getTramite(HashMap<String, Object> body, String tramite);

	List<Map<String, Object>> getAutogestionables();

	List<Map<String, Object>> getDiagnosticos(Map<String, Object> map);

	List<Map<String, Object>> getCoberturaMadre(Map<String, Object> map);
}

