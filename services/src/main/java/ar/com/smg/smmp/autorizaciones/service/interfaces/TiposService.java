package ar.com.smg.smmp.autorizaciones.service.interfaces;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoExcepcion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoFinalizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoRechazo;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAgrupacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAutorizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoGestion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoPaciente;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoProvisionObraSocial;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoResolucion;

public interface TiposService {

	List<TipoAutorizacion> getTiposAutorizaciones() throws IOException;
	List<TipoGestion> getTiposGestion() throws IOException;
	List<TipoPaciente> getTiposPaciente() throws IOException;
	List<TipoProvisionObraSocial> getTiposProvisionObraSocial() throws IOException;
	List<TipoAgrupacion> getTiposAgrupacion() throws IOException;
	List<TipoResolucion> getTiposResolucion() throws IOException;
	List<MotivoExcepcion> getMotivosExcepcion() throws IOException;
	List<MotivoRechazo> getMotivosRechazo() throws IOException;
	List<MotivoFinalizacion> getMotivosFinalizacion(String tarea) throws IOException;
	List<TipoResolucion> getTiposResolucionST() throws IOException;
	List<Map<String, Object>> getTiposPracticaOdo() throws IOException;
	List<Map<String, Object>> getMotivosFinalizacionOdo(Integer codigo, Integer grupo) throws IOException;
	List<Map<String, Object>> getResolucionesOdo(Integer codigo) throws IOException;
	List<Map<String, Object>> traeMotivosNoReprogramacionCirugia() throws IOException;
	List<Map<String, Object>> traeEstudiosAdjuntosMdc(Long fecha) throws IOException;
	List<Map<String, Object>> traeObrasSocialesMdc() throws IOException;
	List<Map<String, Object>> estadoAutorizacionesSGI() throws IOException;
	List<Map<String, Object>> clasificacionAutorizacionesSGI() throws IOException;
	List<Map<String, Object>> obtenerMandatariasAutorizacionesSGI(String autorizaciones) throws IOException;
	List<Map<String, Object>> obtenerEstudiosPorTipoAutorizacionSGI(Long fecha, String tipoAutorizacion) throws IOException;;
	List<Map<String, Object>> getSubmotivosFinalizacion(String tarea) throws IOException;
}
