package ar.com.smg.smmp.autorizaciones.service.util;

import java.io.Serializable;
import java.util.List;

import ar.com.smg.smmp.model.autorizaciones.common.DatosEmail;

public final class ServiceHelper implements Serializable {

	private static final long serialVersionUID = 3813380283878464595L;

	private ServiceHelper() {
		//not called
		super();
	}
	
	public static DatosEmail buildDatosEmail(List<String> cadenas, char itemSeparator, 
			char keyValueSeparator) {
		DatosEmail datosEmail = new DatosEmail();
		StringBuffer cadenaBuffer = new StringBuffer();
		
		if (cadenas == null || cadenas.isEmpty()) {
			return datosEmail;
		}
		
		for (String cadena : cadenas) {
			cadenaBuffer.append(cadena);
		}
		
//		String[] items = StringUtils.split(cadenaBuffer.toString(), itemSeparator);
		String[] items = cadenaBuffer.toString().split(String.valueOf(itemSeparator));
		
		if (items == null || items.length == 0) {
			return datosEmail;
		}
		
		for (String item : items) {
//			String[] cadenaKeyValue = StringUtils.split(item, keyValueSeparator);
			String[] cadenaKeyValue = item.split(String.valueOf(keyValueSeparator));
			
			if (cadenaKeyValue != null && cadenaKeyValue.length > 1) {
				datosEmail.setDato(cadenaKeyValue[0], cadenaKeyValue[1] != null ? cadenaKeyValue[1] : "");					
			}
		}
		
		return datosEmail;
	}
}
