package ar.com.smg.smmp.autorizaciones.exception;

import ar.com.smg.smmp.core.SMMPRuntimeException;

public class InvalidParameterException extends SMMPRuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidParameterException() {
		super("Parametros Invalidos!");
	}

	
}
