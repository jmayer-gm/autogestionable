package ar.com.smg.smmp.autorizaciones.exception;

public class EntidadNegocioInexistenteException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private String message = "EntidadNegocio inexistente.";

	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
