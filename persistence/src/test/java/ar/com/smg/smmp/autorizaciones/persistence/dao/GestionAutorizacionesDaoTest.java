package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class GestionAutorizacionesDaoTest {

	protected static final String OBSERVACION_TEST = "Test JUnit - Prueba Sistemas modulo persistence";
	
	private static final String CONFIGURATION_FILE = "sql-map-config.xml";
	private static final String DS_PRESTACI_XA_JNDI_NAME = "prestaciDSXA";
	private static final String DS_PRESTACI_NO_TRANSACTION_JNDI_NAME = "prestaci_auto_false";
	private static final String DS_CRM_SMG_NO_TRANSACTION_JNDI_NAME = "crm_smg_no_tx";
	
	protected GestionAutorizacionTransactionManager gestionAutorizacionTransactionManager;
	protected TipoTransactionManager tipoTransactionManager;
	
	protected SqlSessionFactory session;
	protected Reader reader;
	
	public void initTestGestionAutorizaciones() {
		gestionAutorizacionTransactionManager = new GestionAutorizacionTransactionManager();
	}
	
	public void initTestTipo() {
		tipoTransactionManager = new TipoTransactionManager();
	}
		
	public SqlSession getPrestaciXASession() throws IOException {
		reader = Resources.getResourceAsReader(CONFIGURATION_FILE);
		session = new SqlSessionFactoryBuilder().build(reader, DS_PRESTACI_XA_JNDI_NAME);
		return session.openSession(true);
	}
	
	public SqlSession getPrestaciNoTransactionalSession() throws IOException {
		reader = Resources.getResourceAsReader(CONFIGURATION_FILE);
		session = new SqlSessionFactoryBuilder().build(reader, DS_PRESTACI_NO_TRANSACTION_JNDI_NAME);
		return session.openSession();
	}
	
	public SqlSession getCrmSmgNoTransactionalSession() throws IOException {
		reader = Resources.getResourceAsReader(CONFIGURATION_FILE);
		session = new SqlSessionFactoryBuilder().build(reader, DS_CRM_SMG_NO_TRANSACTION_JNDI_NAME);
		return session.openSession();
	}
	
}
