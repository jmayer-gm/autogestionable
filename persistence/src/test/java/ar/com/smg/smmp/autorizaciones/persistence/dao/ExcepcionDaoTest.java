package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.ExcepcionDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.ExcepcionDAO;
import ar.com.smg.smmp.model.autorizaciones.gestion.Excepcion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoExcepcion;

public class ExcepcionDaoTest extends GestionAutorizacionesDaoTest {

	private static final Logger LOGGER = Logger.getLogger(ExcepcionDaoTest.class);
	
	@Before
	public void init() {
		super.initTestGestionAutorizaciones();
	}
	
	@Test
	public void ingresarExcepcionPrecarga() throws Exception {
		/**
		Map<String, Object> map = new HashMap<String, Object>();
		Excepcion excepcion = new Excepcion();
		ExcepcionDAO excepcionDAO = new ExcepcionDAOImpl();
		
		excepcion.setSeExcepciona(Boolean.TRUE);
		excepcion.setObservacionExcepcion(OBSERVACION_TEST);
		
		map.put("etapaId", 34470092);
		map.put("excepcion", excepcion);
		
		excepcionDAO.setSession(this.getPrestaciXASession());
		excepcionDAO.insertaExcepcionAutorizacion(map);
		
		LOGGER.info("Id etapa: " + map.get("etapaId"));**/
	}
	
	@Test
	public void aprobarExcepcionPrecarga() throws Exception {/**
		Map<String, Object> map = new HashMap<String, Object>();
		Excepcion excepcion = new Excepcion();
		ExcepcionDAO excepcionDAO = new ExcepcionDAOImpl();
		
		excepcion.setAprobada(Boolean.TRUE);
		excepcion.setObservacionAprobacion(OBSERVACION_TEST);
		
		map.put("etapaId", 34470093);
		map.put("excepcion", excepcion);
		
		excepcionDAO.setSession(this.getPrestaciXASession());
		excepcionDAO.apruebaExcepcionAutorizacion(map);
		
		LOGGER.info("Id etapa: " + map.get("etapaId"));**/
	}
	
	@Test
	public void rechazarExcepcionPrecarga() throws Exception {/**
		Map<String, Object> map = new HashMap<String, Object>();
		Excepcion excepcion = new Excepcion();
		MotivoExcepcion motivoExcepcion = new MotivoExcepcion();
		ExcepcionDAO excepcionDAO = new ExcepcionDAOImpl();
		
		motivoExcepcion.setId(1);
		
		excepcion.setAprobada(Boolean.FALSE);
		excepcion.setMotivoExcepcion(motivoExcepcion);
		excepcion.setObservacionAprobacion(OBSERVACION_TEST);
		
		map.put("etapaId", 34470094);
		map.put("excepcion", excepcion);
		
		excepcionDAO.setSession(this.getPrestaciXASession());
		excepcionDAO.apruebaExcepcionAutorizacion(map);
		
		LOGGER.info("Id etapa: " + map.get("etapaId"));**/
	}
	
	// @Test
	// TODO: Revisar por que falla este test, y volver a activarlo
	public void traerDatosExcepcionPrecarga() throws Exception {/**
		Excepcion excepcion = new Excepcion();
		ExcepcionDAO excepcionDAO = new ExcepcionDAOImpl();
		Integer etapaId = 34475813;
		
		excepcionDAO.setSession(this.getCrmSmgNoTransactionalSession());
		excepcion = excepcionDAO.traeExcepcionAutorizacion(etapaId);
		
		Assert.assertTrue("El objeto excepcion es null", excepcion != null);
		Assert.assertTrue(excepcion.getSeExcepciona());
		Assert.assertEquals(OBSERVACION_TEST, excepcion.getObservacionExcepcion());
		
		LOGGER.info("Id etapa: " + etapaId);
		LOGGER.info("Se excepciona: " + excepcion.getSeExcepciona());
		LOGGER.info("Observacion: " + excepcion.getObservacionExcepcion());**/
	}
	
}
