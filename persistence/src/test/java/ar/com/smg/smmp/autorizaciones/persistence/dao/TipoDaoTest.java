package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.io.IOException;
import java.util.List;

import junit.framework.Assert;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.TipoDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.TipoDAO;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoExcepcion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoRechazo;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAgrupacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAutorizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoGestion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoPaciente;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoProvisionObraSocial;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoResolucion;

public class TipoDaoTest extends GestionAutorizacionesDaoTest {

	private static final Logger LOGGER = Logger.getLogger(TipoDaoTest.class);
	private SqlSession session;
	
	@Before
	public void init() throws IOException {
		session = this.getPrestaciNoTransactionalSession();
	}
	
	@After
	public void ended() {
		if (session != null) {
			session.close();
		}
	}
		
	@Test
	public void traeTiposAutorizacion() throws Exception {/**
		List<TipoAutorizacion>  tiposAutorizacion = null;
		TipoDAO tipoDao = new TipoDAOImpl();
		tipoDao.setSession(session);
		
		tiposAutorizacion = tipoDao.traeTiposAutorizacion();
		
		Assert.assertNotNull("La lista es NULL", tiposAutorizacion);
		Assert.assertFalse("La lista esta vacia", tiposAutorizacion.isEmpty());
		
		LOGGER.info("Tipos de Autorizacion:");
		for (TipoAutorizacion tipoAutorizacion : tiposAutorizacion) {
			LOGGER.info("Id: " + tipoAutorizacion.getId());
			LOGGER.info("Descripcion: " +  tipoAutorizacion.getDescripcion());
		}**/
	}
	
	@Test
	public void traeTiposGestion() throws Exception {/**
		List<TipoGestion> tiposGestion = null;
		TipoDAO tipoDao = new TipoDAOImpl();
		tipoDao.setSession(session);
		
		tiposGestion = tipoDao.traeTiposGestion();
		
		Assert.assertNotNull("La lista es NULL", tiposGestion);
		Assert.assertFalse("La lista esta vacia", tiposGestion.isEmpty());
		
		LOGGER.info("Tipos de Gestion:");
		for (TipoGestion tipoGestion : tiposGestion) {
			LOGGER.info("Id: " + tipoGestion.getId());
			LOGGER.info("Descripcion: " +  tipoGestion.getDescripcion());**/
		
	}
	
	@Test
	public void traeTiposPaciente() throws Exception {/**
		List<TipoPaciente> tiposPaciente = null;
		TipoDAO tipoDao = new TipoDAOImpl();
		tipoDao.setSession(session);
		
		tiposPaciente = tipoDao.traeTiposPaciente();
		
		Assert.assertNotNull("La lista es NULL", tiposPaciente);
		Assert.assertFalse("La lista esta vacia", tiposPaciente.isEmpty());
		
		LOGGER.info("Tipos de Paciente:");
		for (TipoPaciente tipoPaciente : tiposPaciente) {
			LOGGER.info("Id: " + tipoPaciente.getId());
			LOGGER.info("Descripcion: " +  tipoPaciente.getDescripcion());
		}**/
	}
	
	@Test
	public void traeTiposProvisionObraSocial() throws Exception {/**
		List<TipoProvisionObraSocial> tiposProvisionObraSocial = null;
		TipoDAO tipoDao = new TipoDAOImpl();
		tipoDao.setSession(session);
		
		tiposProvisionObraSocial = tipoDao.traeTiposProvisionObraSocial();
		
		Assert.assertNotNull("La lista es NULL", tiposProvisionObraSocial);
		Assert.assertFalse("La lista esta vacia", tiposProvisionObraSocial.isEmpty());
		
		LOGGER.info("Tipos de Provision por Obra Social:");
		for (TipoProvisionObraSocial tipoProvisonObraSocial : tiposProvisionObraSocial) {
			LOGGER.info("Id: " + tipoProvisonObraSocial.getId());
			LOGGER.info("Descripcion: " +  tipoProvisonObraSocial.getDescripcion());
		}**/
	}
	
	@Test
	public void traeTiposAgrupacion() throws Exception {/**
		List<TipoAgrupacion> tiposAgrupacion = null;
		TipoDAO tipoDao = new TipoDAOImpl();
		tipoDao.setSession(session);
		
		tiposAgrupacion = tipoDao.traeTiposAgrupacion();
		
		Assert.assertNotNull("La lista es NULL", tiposAgrupacion);
		Assert.assertFalse("La lista esta vacia", tiposAgrupacion.isEmpty());
		
		LOGGER.info("Tipos de Agrupacion:");
		for (TipoAgrupacion tipoAgrupacion : tiposAgrupacion) {
			LOGGER.info("Id: " + tipoAgrupacion.getId());
			LOGGER.info("Descripcion: " +  tipoAgrupacion.getDescripcion());
		}**/
	}
	
	@Test
	public void traeTiposResolucion() throws Exception {/**
		List<TipoResolucion> tiposResolucion = null;
		TipoDAO tipoDao = new TipoDAOImpl();
		tipoDao.setSession(session);
		
		tiposResolucion = tipoDao.traeTiposResolucion();
		
		Assert.assertNotNull("La lista es NULL", tiposResolucion);
		Assert.assertFalse("La lista esta vacia", tiposResolucion.isEmpty());
		
		LOGGER.info("Tipos de Agrupacion:");
		for (TipoResolucion tipoResolucion : tiposResolucion) {
			LOGGER.info("Id: " + tipoResolucion.getId());
			LOGGER.info("Descripcion: " +  tipoResolucion.getDescripcion());
		}**/
	}
	
	@Test
	public void traeMotivosExcepcion() throws Exception {/**
		List<MotivoExcepcion> motivosExcepcion = null;
		TipoDAO tipoDao = new TipoDAOImpl();
		tipoDao.setSession(session);
		
		motivosExcepcion = tipoDao.traeMotivosExcepcion();
		
		Assert.assertNotNull("La lista es NULL", motivosExcepcion);
		Assert.assertFalse("La lista esta vacia", motivosExcepcion.isEmpty());
		
		LOGGER.info("Tipos de Agrupacion:");
		for (MotivoExcepcion motivoExcepcion : motivosExcepcion) {
			LOGGER.info("Id: " + motivoExcepcion.getId());
			LOGGER.info("Descripcion: " +  motivoExcepcion.getDescripcion());
		}**/
	}
	
	@Test
	public void traeMotivosRechazo() throws Exception {/**
		List<MotivoRechazo> motivosRechazo = null;
		TipoDAO tipoDao = new TipoDAOImpl();
		tipoDao.setSession(session);
		
		motivosRechazo = tipoDao.traeMotivosRechazo();
		
		Assert.assertNotNull("La lista es NULL", motivosRechazo);
		Assert.assertFalse("La lista esta vacia", motivosRechazo.isEmpty());
		
		LOGGER.info("Tipos de Agrupacion:");
		for (MotivoRechazo motivoRechazo : motivosRechazo) {
			LOGGER.info("Id: " + motivoRechazo.getId());
			LOGGER.info("Descripcion: " +  motivoRechazo.getDescripcion());
		}**/
	}

}
