package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.ControlEntregaDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.ControlEntregaDAO;
import ar.com.smg.smmp.model.autorizaciones.gestion.ControlEntrega;

public class ControlEntregaDaoTest extends GestionAutorizacionesDaoTest {

private static final Logger LOGGER = Logger.getLogger(ControlEntregaDaoTest.class);
	
	@Before
	public void init() {
		super.initTestGestionAutorizaciones();
	}
	
	@Test
	public void ingresarControlEntrega() throws Exception {
		/**
		Map<String, Object> map = new HashMap<String, Object>();
		ControlEntrega controlEntrega = new ControlEntrega();
		ControlEntregaDAO controlEntregaDAO = new ControlEntregaDAOImpl();
		
		controlEntrega.setObservacion(OBSERVACION_TEST);
		controlEntrega.setEntregada(Boolean.TRUE);
		
		map.put("etapaId", 34470092);
		map.put("controlEntrega", controlEntrega);
		
		controlEntregaDAO.setSession(this.getPrestaciXASession());
		controlEntregaDAO.insertaControlEntrega(map);
		
		LOGGER.info("Id etapa: " + map.get("etapaId"));
		**/
	}
	
	// @Test
	// TODO: Revisar por que falla este test, y volver a activarlo
	public void traerDatosControlEntrega() throws Exception {
		/**
		ControlEntrega controlEntrega = new ControlEntrega();
		ControlEntregaDAO controlEntregaDAO = new ControlEntregaDAOImpl();
		
		Integer etapaId = 34475762;
		
		controlEntregaDAO.setSession(this.getCrmSmgNoTransactionalSession());
		controlEntrega = controlEntregaDAO.traeControlEntrega(etapaId);
		
		Assert.assertNotNull("El objeto controlEntrega es null", controlEntrega);
		Assert.assertNotNull("El objeto entregada es null", controlEntrega.getEntregada());
		Assert.assertTrue("El control de la entrega no esta entregada", controlEntrega.getEntregada());
		Assert.assertEquals(OBSERVACION_TEST, controlEntrega.getObservacion());
		
		LOGGER.info("Id etapa: " + etapaId);
		LOGGER.info("La entrega fue realizada? " + controlEntrega.getEntregada());
		LOGGER.info("Observacion: " + controlEntrega.getObservacion());
	**/
	}
	
}