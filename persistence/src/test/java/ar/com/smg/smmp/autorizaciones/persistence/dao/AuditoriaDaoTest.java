package ar.com.smg.smmp.autorizaciones.persistence.dao;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

public class AuditoriaDaoTest extends GestionAutorizacionesDaoTest {

	private static final Logger LOGGER = Logger.getLogger(AuditoriaDaoTest.class);
	
	@Before
	public void init() {
		super.initTestGestionAutorizaciones();
	}
	
	@Test
	public void ingresarAuditoriaMedica() throws Exception {
	/**
	Map<String, Object> map = new HashMap<String, Object>();
		TipoResolucion tipoResolucion = new TipoResolucion();
		Auditoria auditoria = new Auditoria();
		AuditoriaDAO auditoriaDAO = new AuditoriaDAOImpl();
		
		tipoResolucion.setId(1);
		
		auditoria.setTipoResolucion(tipoResolucion);
		auditoria.setObservacion(OBSERVACION_TEST);
		
		map.put("etapaId", 34470092);
		map.put("auditoria", auditoria);
		
		auditoriaDAO.setSession(this.getPrestaciXASession());
		auditoriaDAO.insertaAuditoriaMedica(map);
		
		LOGGER.info("Id etapa: " + map.get("etapaId"));**/
	}
	
	// @Test
	// TODO: Revisar por que falla este test, y volver a activarlo
	public void traerDatosAuditoriaMedica() throws Exception {
		/**
		Auditoria auditoria = new Auditoria();
		AuditoriaDAO auditoriaDAO = new AuditoriaDAOImpl();
		
		Integer etapaId = 34475832;
		
		auditoriaDAO.setSession(this.getCrmSmgNoTransactionalSession());
		auditoria = auditoriaDAO.traeAuditoriaMedica(etapaId);
		
		Assert.assertNotNull("El objeto auditoria es null", auditoria);
		Assert.assertNotNull("El objeto tipoResolucion es null", auditoria.getTipoResolucion());
		Assert.assertNotNull("El id del tipo de resolucion es null", auditoria.getTipoResolucion().getId());
		Assert.assertTrue("El id del tipo de resolucion no coincide", 
				auditoria.getTipoResolucion().getId().equals(1));
		Assert.assertEquals(OBSERVACION_TEST, auditoria.getObservacion());
		
		LOGGER.info("Id etapa: " + etapaId);
		LOGGER.info("Tipo de resolucion: " + auditoria.getTipoResolucion().getDescripcion());
		LOGGER.info("Observacion: " + auditoria.getObservacion());
		**/
	}
	
}
