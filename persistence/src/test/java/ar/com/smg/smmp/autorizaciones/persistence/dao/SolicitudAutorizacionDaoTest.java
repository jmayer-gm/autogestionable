package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.SolicitudAutorizacionDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.SolicitudAutorizacionDAO;
import ar.com.smg.smmp.model.autorizaciones.solicitud.SolicitudAutorizacion;
import ar.com.smg.smmp.model.autorizaciones.solicitud.Estado;

public class SolicitudAutorizacionDaoTest extends GestionAutorizacionesDaoTest {

	private static final Logger LOGGER = Logger.getLogger(SolicitudAutorizacionDaoTest.class);
	
	@Before
	public void init() {
		super.initTestGestionAutorizaciones();
	}
	
	@Test
	public void ingresarSolicitudAutorizacion() throws Exception {/**
		Map<String, Object> map = new HashMap<String, Object>();
		SolicitudAutorizacion solicitudAutorizacion = new SolicitudAutorizacion();
		SolicitudAutorizacionDAO solicitudAutorizacionDAO = new SolicitudAutorizacionDAOImpl();
		
		solicitudAutorizacion.setId(1000);
		solicitudAutorizacion.setObservacion(OBSERVACION_TEST);
		solicitudAutorizacion.getTipoAutorizacion().setId("C");
		solicitudAutorizacion.setFechaOrden(new Date());
		solicitudAutorizacion.setFechaProcedimiento(new Date());
		solicitudAutorizacion.setHoraProcedimiento("01:00");
		solicitudAutorizacion.getTipoGestion().setId(1);
		solicitudAutorizacion.getTipoPaciente().setId(2);
		solicitudAutorizacion.getEstado().setId(Estado.PENDIENTE_ID);
		
		map.put("etapaId", 38444294);
		map.put("workflowId", 31270923);
		map.put("solicitudAutorizacion", solicitudAutorizacion);
		
		solicitudAutorizacionDAO.setSession(this.getPrestaciXASession());
		solicitudAutorizacionDAO.insertaSolicitudAutorizacion(map);
		
		LOGGER.info("Id etapa: " + map.get("etapaId"));**/
	}
	
	@Test
	public void traerSolicitudAutorizacion() throws Exception {/**
		SolicitudAutorizacion solicitudAutorizacion = new SolicitudAutorizacion();
		SolicitudAutorizacionDAO solicitudAutorizacionDAO = new SolicitudAutorizacionDAOImpl();
		Integer etapaId = 38447268;
		
		solicitudAutorizacionDAO.setSession(this.getCrmSmgNoTransactionalSession());
		solicitudAutorizacion = solicitudAutorizacionDAO.traeSolicitudAutorizacion(etapaId);
		
		Assert.assertTrue("El objeto solicitud de autorizacion es null", solicitudAutorizacion != null);
		Assert.assertTrue("El objeto tipo de autorizacion es null", 
				solicitudAutorizacion.getTipoAutorizacion() != null);
		Assert.assertTrue("El id del tipo de autorizacion es null", 
				solicitudAutorizacion.getTipoAutorizacion().getId() != null);
		
		LOGGER.info("Id etapa: " + etapaId);
		LOGGER.info("Id Solicitud: " + solicitudAutorizacion.getId());
		LOGGER.info("Id Tipo de Autorizacion: " + solicitudAutorizacion.getTipoAutorizacion().getId());	**/
	}
	
	@Test
	public void traerPrestadorSolicitudAutorizacion() throws Exception {/**
		Map<String, Object> map = null;
		SolicitudAutorizacionDAO solicitudAutorizacionDAO = new SolicitudAutorizacionDAOImpl();
		Integer etapaId = 38444331;
		
		solicitudAutorizacionDAO.setSession(this.getCrmSmgNoTransactionalSession());
		map = solicitudAutorizacionDAO.traePrestadorSolicitudAutorizacion(etapaId);
		
		Assert.assertTrue("El map es null", map != null);
		Assert.assertTrue("El objeto prestador es null", map.get("prestador") != null);
		Assert.assertTrue("El id del tipo de autorizacion es null", map.get("lugarDeAtencion") != null);
		
		LOGGER.info("Id etapa: " + etapaId);
		LOGGER.info("Prestador: " + map.get("prestador"));
		LOGGER.info("Lugar de Atencion: " + map.get("lugarDeAtencion"));**/	
	}
	
}
