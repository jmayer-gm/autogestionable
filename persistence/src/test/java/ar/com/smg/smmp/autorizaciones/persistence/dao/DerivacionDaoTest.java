package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.DerivacionDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.DerivacionDAO;
import ar.com.smg.smmp.model.autorizaciones.gestion.Derivacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoRechazo;

public class DerivacionDaoTest extends GestionAutorizacionesDaoTest {

	private static final Logger LOGGER = Logger.getLogger(DerivacionDaoTest.class);
	
	@Before
	public void init() {
		super.initTestGestionAutorizaciones();
	}
	
	@Test
	public void aprobarDerivacionTramite() throws Exception {
		/**
		Map<String, Object> map = new HashMap<String, Object>();
		Derivacion derivacion = new Derivacion();
		MotivoRechazo motivoRechazo = new MotivoRechazo();
		DerivacionDAO derivacionDAO = new DerivacionDAOImpl();
		
		derivacion.setAprobada(Boolean.TRUE);
		derivacion.setMotivoRechazo(motivoRechazo);
		derivacion.setObservacion(OBSERVACION_TEST);
		
		map.put("etapaId", 34470092);
		map.put("derivacion", derivacion);
		
		derivacionDAO.setSession(this.getPrestaciXASession());
		derivacionDAO.insertaDerivacionMedica(map);
		
		LOGGER.info("Id etapa: " + map.get("etapaId"));**/
	}
	
	@Test
	public void rechazarDerivacionTramite() throws Exception {
		/**
		Map<String, Object> map = new HashMap<String, Object>();
		Derivacion derivacion = new Derivacion();
		MotivoRechazo motivoRechazo = new MotivoRechazo();
		DerivacionDAO derivacionDAO = new DerivacionDAOImpl();
		
		motivoRechazo.setId(1);
		
		derivacion.setAprobada(Boolean.FALSE);
		derivacion.setMotivoRechazo(motivoRechazo);
		derivacion.setObservacion(OBSERVACION_TEST);
		
		map.put("etapaId", 34475781);
		map.put("derivacion", derivacion);
		
		derivacionDAO.setSession(this.getPrestaciXASession());
		derivacionDAO.insertaDerivacionMedica(map);
		
		LOGGER.info("Id etapa: " + map.get("etapaId"));**/
	}
	
	// @Test
	// TODO: Revisar por que falla este test, y volver a activarlo
	public void traerDatosDerivacionTramite() throws Exception {
		/**
		Derivacion derivacion = new Derivacion();
		DerivacionDAO derivacionDAO = new DerivacionDAOImpl();
		
		Integer etapaId = 34470092;
		
		derivacionDAO.setSession(this.getCrmSmgNoTransactionalSession());
		derivacion = derivacionDAO.traeDerivacionMedica(etapaId);
		
		Assert.assertNotNull("El objeto derivacion es null", derivacion);
		Assert.assertNotNull("El objeto aprobada es null", derivacion.getAprobada());
		Assert.assertTrue("La derivacion no esta aprobada", derivacion.getAprobada());
		Assert.assertNotNull("El objeto motivoRechazo es null", derivacion.getMotivoRechazo());
		Assert.assertNotNull("El id del motivo de rechazo es null", derivacion.getMotivoRechazo().getId());
		Assert.assertTrue("El id del del motivo de rechazo no coincide", 
				derivacion.getMotivoRechazo().getId().equals(1));
		Assert.assertEquals(OBSERVACION_TEST, derivacion.getObservacion());
		
		LOGGER.info("Id etapa: " + etapaId);
		LOGGER.info("La derivacion fue aprobada? " + derivacion.getAprobada());
		LOGGER.info("Motivo de recazo: " + derivacion.getMotivoRechazo().getDescripcion());
		LOGGER.info("Observacion: " + derivacion.getObservacion());**/
	}
	
}