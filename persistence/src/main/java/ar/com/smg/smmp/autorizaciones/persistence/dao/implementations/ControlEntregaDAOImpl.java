package ar.com.smg.smmp.autorizaciones.persistence.dao.implementations;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.ControlEntregaDAO;
import ar.com.smg.smmp.model.autorizaciones.gestion.ControlEntrega;

public class ControlEntregaDAOImpl implements ControlEntregaDAO {

	private SqlSession session;
	
	public ControlEntregaDAOImpl() {
		session = null;
	}
	
	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void setSession(SqlSession session) {
		this.session = session;
	}
	
	@Override
	public void insertaControlEntrega(Map<String, Object> map) {
		session.insert(INSERTA_CONTROL_ENTREGA, map);
	}

	@Override
	public ControlEntrega traeControlEntrega(Integer etapaId) {
		return session.selectOne(TRAE_CONTROL_ENTREGA, etapaId);
	}

}
