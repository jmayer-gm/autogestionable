package ar.com.smg.smmp.autorizaciones.persistence.dao.implementations;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.AuditoriaDAO;
import ar.com.smg.smmp.model.autorizaciones.gestion.Auditoria;

public class AuditoriaDAOImpl implements AuditoriaDAO {

	private SqlSession session;
	
	public AuditoriaDAOImpl() {
		session = null;
	}
	
	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void setSession(SqlSession session) {
		this.session = session;
	}

	@Override
	public void insertaAuditoriaMedica(Map<String, Object> map) {
		session.insert(INSERTA_AUDITORIA_MEDICA, map);
	}

	@Override
	public Auditoria traeAuditoriaMedica(Integer etapaId) {
		return session.selectOne(TRAE_AUDITORIA_MEDICA, etapaId);
	}

}
