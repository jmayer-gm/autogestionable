package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.SupportDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.SupportDAO;

public class SupportTransactionManager {

	private static final Logger LOGGER = Logger.getLogger(SupportTransactionManager.class);
	
	private static final String TRAE_DATOS_EMAIL_ERROR_MESSAGE = "Ha ocurrido un error al traer los datos del email";
	
	SupportDAO supportDAO;
	
        
        
	// ************************** DATOS EMAIL **************************
	public List<String> traerDatosEmailAutorizacion(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		supportDAO = new SupportDAOImpl();
		List<String> lista;
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			supportDAO.setSession(session);
			lista = supportDAO.traeDatosEmailAutorizacion(map);
		} catch (IOException e) {
			LOGGER.error(TRAE_DATOS_EMAIL_ERROR_MESSAGE, e);
			throw new IOException(TRAE_DATOS_EMAIL_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
                
	}

	public List<String> getMailDataPrestador(HashMap<String, Object> params) throws IOException {
		SqlSession session = null;
		supportDAO = new SupportDAOImpl();
		List<String> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			supportDAO.setSession(session);
			lista = supportDAO.getMailDataPrestador(params);
		} 
		catch (IOException e) {
			LOGGER.error(TRAE_DATOS_EMAIL_ERROR_MESSAGE, e);
			throw new IOException(TRAE_DATOS_EMAIL_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
}
