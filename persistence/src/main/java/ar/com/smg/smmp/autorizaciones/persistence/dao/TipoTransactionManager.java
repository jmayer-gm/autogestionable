package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.TipoDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.TipoDAO;
import ar.com.smg.smmp.core.SMMPRuntimeException;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoExcepcion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoFinalizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoRechazo;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAgrupacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAutorizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoGestion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoPaciente;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoProvisionObraSocial;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoResolucion;

public class TipoTransactionManager {

	private static final Logger LOGGER = Logger.getLogger(TipoTransactionManager.class);
	private static final String TIPO_ERROR_MESSAGE = "Ha ocurrido un error al obtener los tipos de {0}";
	private static final String MOTIVO_ERROR_MESSAGE = "Ha ocurrido un error al obtener los motivos de {0}";

	private TipoDAO tipoDao;
	private SqlSession session;

	private SqlSession getSqlSession() throws IOException {
		session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
		return session;
	}
        
        private void closeSqlSession() throws IOException  {
            if (session != null) {
                    session.close();
            }            
        }

	public List<TipoAutorizacion> traeTiposAutorizacion() throws IOException {
		List<TipoAutorizacion> tiposAutorizacion = null;
		tipoDao = new TipoDAOImpl();

		try {
			tipoDao.setSession(this.getSqlSession());
			tiposAutorizacion = tipoDao.traeTiposAutorizacion();
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "autorizacion"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "autorizacion") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return tiposAutorizacion;
	}

	public List<TipoGestion> traeTiposGestion() throws IOException {
		List<TipoGestion> tiposGestion = null;
		tipoDao = new TipoDAOImpl();

		try {
			tipoDao.setSession(this.getSqlSession());
			tiposGestion = tipoDao.traeTiposGestion();
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "gestion"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "gestion") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return tiposGestion;
	}

	public List<TipoPaciente> traeTiposPaciente() throws IOException {
		List<TipoPaciente> tiposPaciente = null;
		tipoDao = new TipoDAOImpl();

		try {
			tipoDao.setSession(this.getSqlSession());
			tiposPaciente = tipoDao.traeTiposPaciente();
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "paciente"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "paciente") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return tiposPaciente;
	}

	public List<TipoProvisionObraSocial> traeTiposProvisionObraSocial() throws IOException {
		List<TipoProvisionObraSocial> tiposProvisionObraSocial = null;
		tipoDao = new TipoDAOImpl();

		try {
			tipoDao.setSession(this.getSqlSession());
			tiposProvisionObraSocial = tipoDao.traeTiposProvisionObraSocial();
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "provision por obra social"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "provision por obra social") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return tiposProvisionObraSocial;
	}

	public List<TipoAgrupacion> traeTiposAgrupacion() throws IOException {
		List<TipoAgrupacion> tiposAgrupacion = null;
		tipoDao = new TipoDAOImpl();

		try {
			tipoDao.setSession(this.getSqlSession());
			tiposAgrupacion = tipoDao.traeTiposAgrupacion();
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "agrupacion"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "agrupacion") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return tiposAgrupacion;
	}

	public List<TipoResolucion> traeTiposResolucion() throws IOException {
		List<TipoResolucion> tiposResolucion = null;
		tipoDao = new TipoDAOImpl();

		try {
			tipoDao.setSession(this.getSqlSession());
			tiposResolucion = tipoDao.traeTiposResolucion();
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "resolucion"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "resolucion") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return tiposResolucion;
	}
	
	public List<TipoResolucion> traeTiposResolucionST() throws IOException {
		List<TipoResolucion> tiposResolucion = null;
		tipoDao = new TipoDAOImpl();

		try {
			tipoDao.setSession(this.getSqlSession());
			tiposResolucion = tipoDao.traeTiposResolucionST();
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "resolucion"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "resolucion") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return tiposResolucion;
	}

	public List<MotivoExcepcion> traeMotivosExcepcion() throws IOException {
		List<MotivoExcepcion> motivosExcepcion = null;
		tipoDao = new TipoDAOImpl();

		try {
			tipoDao.setSession(this.getSqlSession());
			motivosExcepcion = tipoDao.traeMotivosExcepcion();
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(MOTIVO_ERROR_MESSAGE, "excepcion"), e);
			throw new IOException(MessageFormat.format(MOTIVO_ERROR_MESSAGE, "excepcion") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return motivosExcepcion;
	}

	public List<MotivoRechazo> traeMotivosRechazo() throws IOException {
		List<MotivoRechazo> motivosRechazo = null;
		tipoDao = new TipoDAOImpl();

		try {
			tipoDao.setSession(this.getSqlSession());
			motivosRechazo = tipoDao.traeMotivosRechazo();
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(MOTIVO_ERROR_MESSAGE, "rechazo"), e);
			throw new IOException(MessageFormat.format(MOTIVO_ERROR_MESSAGE, "rechazo") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return motivosRechazo;
	}

	public List<MotivoFinalizacion> traeMotivosFinalizacion(String tarea) throws IOException {
		List<MotivoFinalizacion> motivosFinalizacion = null;
		tipoDao = new TipoDAOImpl();

		try {
			tipoDao.setSession(this.getSqlSession());
			motivosFinalizacion = tipoDao.traeMotivosFinalizacion(tarea);
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(MOTIVO_ERROR_MESSAGE, "finalizacion"), e);
			throw new IOException(MessageFormat.format(MOTIVO_ERROR_MESSAGE, "finalizacion") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return motivosFinalizacion;
	}
	
	public List<Map<String, Object>> traeSubmotivosFinalizacion(Map<String, Object> map) throws IOException {
                List<Map<String, Object>> listaSubMotivosFinalizacion = null;
		tipoDao = new TipoDAOImpl();

		try {
			tipoDao.setSession(this.getSqlSession());
			listaSubMotivosFinalizacion = tipoDao.traeSubmotivosFinalizacion(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
                return listaSubMotivosFinalizacion;
	}
	
	public List<Map<String, Object>> traeTiposPracticaOdo() throws IOException {
		
		List<Map<String, Object>> tiposPractica = null;
		tipoDao = new TipoDAOImpl();
		try {
			tipoDao.setSession(this.getSqlSession());
			tiposPractica = tipoDao.traeTiposPracticaOdo();
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "practica"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "practica") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return tiposPractica;
	}
	
	public List<Map<String, Object>> traeMotivosFinalizacionOdo(Integer codigo, Integer grupo) throws IOException {
		
		List<Map<String, Object>> motivos = null;
		tipoDao = new TipoDAOImpl();
		try {
			tipoDao.setSession(this.getSqlSession());
			motivos = tipoDao.traeMotivosFinalizacionOdo(codigo, grupo);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "motivo"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "motivo") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return motivos;
	}
	
	public List<Map<String, Object>> traeResolucionesOdo(Integer codigo) throws IOException {
		
		List<Map<String, Object>> resoluciones = null;
		tipoDao = new TipoDAOImpl();
		try {
			tipoDao.setSession(this.getSqlSession());
			resoluciones = tipoDao.traeResolucionesOdo(codigo);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "resolucion"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "resolucion") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return resoluciones;
	}

	public List<Map<String, Object>> traeMotivosNoReprogramacionCirugia() throws IOException {
		List<Map<String, Object>> motivosReprogramacionMdc = null;
		tipoDao = new TipoDAOImpl();
		try {
			tipoDao.setSession(this.getSqlSession());
			motivosReprogramacionMdc = tipoDao.traeMotivosNoReprogramacionCirugia();
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "motivos reprogramacion mdc"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "motivos reprogramacion mdc") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return motivosReprogramacionMdc;
	}

	public List<Map<String, Object>> traeEstudiosAdjuntosMdc(Long fecha) throws IOException {
		List<Map<String, Object>> estudiosAdjuntosMdc = null;
		tipoDao = new TipoDAOImpl();
		try {
			tipoDao.setSession(this.getSqlSession());
			estudiosAdjuntosMdc = tipoDao.traeEstudiosAdjuntosMdc(fecha);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "estudios adjuntos mdc"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "estudios adjuntos mdc") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return estudiosAdjuntosMdc;
	}

	public List<Map<String, Object>> traeObrasSocialesMdc() throws IOException {
		List<Map<String, Object>> obrasSocialesMdc = null;
		tipoDao = new TipoDAOImpl();
		try {
			tipoDao.setSession(this.getSqlSession());
			obrasSocialesMdc = tipoDao.traeObrasSocialesMdc();
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "obras sociales mdc"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "obras sociales mdc") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return obrasSocialesMdc;
	}

	public List<Map<String, Object>> estadoAutorizacionesSGI() throws IOException {
		List<Map<String, Object>> estadoAutorizacionesSgi = null;
		tipoDao = new TipoDAOImpl();
		try {
			tipoDao.setSession(this.getSqlSession());
			estadoAutorizacionesSgi = tipoDao.estadoAutorizacionesSGI();
		} 
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "estados autorizacion SGI"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "estados autorizacion SGI") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return estadoAutorizacionesSgi;
	}
	
	public List<Map<String, Object>> clasificacionAutorizacionesSGI() throws IOException {
		List<Map<String, Object>> clasificacionAutorizacionesSgi = null;
		tipoDao = new TipoDAOImpl();
		try {
			tipoDao.setSession(this.getSqlSession());
			clasificacionAutorizacionesSgi = tipoDao.clasificacionAutorizacionesSGI();
		} 
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "clasificacion autorizacion SGI"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "clasificacion autorizacion SGI") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return clasificacionAutorizacionesSgi;
	}
	
	public List<Map<String, Object>> obtenerMandatariasAutorizacionesSGI(String autorizaciones) throws IOException {
		List<Map<String, Object>> mandatarias = null;
		tipoDao = new TipoDAOImpl();
		try {
			tipoDao.setSession(this.getSqlSession());
			mandatarias = tipoDao.obtenerMandatariasAutorizacionesSGI(autorizaciones);
		} 
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "mandatarias SGI"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "mandatarias SGI") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return mandatarias;
	}
	
	//mcastillo
	public List<Map<String, Object>> obtenerEstudiosPorTipoAutorizacionSGI(Long fecha, String tipoAutorizacion) throws IOException {
		List<Map<String, Object>> estudiosAdjuntos = null;
		tipoDao = new TipoDAOImpl();
		try {
			tipoDao.setSession(this.getSqlSession());
			estudiosAdjuntos = tipoDao.obtenerEstudiosPorTipoAutorizacionSGI(fecha, tipoAutorizacion);
		} 
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(TIPO_ERROR_MESSAGE, "estudios adjuntos por tipo de autorizacion"), e);
			throw new IOException(MessageFormat.format(TIPO_ERROR_MESSAGE, "estudios adjuntos por tipo de autorizacion") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return estudiosAdjuntos;
	}
	
}
