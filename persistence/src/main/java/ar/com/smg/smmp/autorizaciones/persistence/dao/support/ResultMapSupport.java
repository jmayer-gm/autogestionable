package ar.com.smg.smmp.autorizaciones.persistence.dao.support;

import java.util.HashMap;
import java.util.Map;

import ar.com.smg.smmp.model.global.domicilio.Barrio;
import ar.com.smg.smmp.model.global.domicilio.Domicilio;
import ar.com.smg.smmp.model.global.domicilio.Localidad;
import ar.com.smg.smmp.model.global.domicilio.Partido;
import ar.com.smg.smmp.model.global.domicilio.Provincia;
import ar.com.smg.smmp.model.global.domicilio.Region;
import ar.com.smg.smmp.model.global.persona.PersonaFisica;
import ar.com.smg.smmp.model.prestadores.LugarDeAtencion;
import ar.com.smg.smmp.model.prestadores.medicoasistencial.PrestadorMedicoAsistencial;
import ar.com.smg.smmp.model.prestadores.medicoasistencial.PrestadorMedicoAsistencialProfesional;

public final class ResultMapSupport {

	private static final String UNCHECKED = "unchecked";
	private static final String KEY_ID = "id";
	private static final String KEY_DESCRIPCION = "descripcion";
	
	private ResultMapSupport(){
		//not called
		super();
	}
	
	@SuppressWarnings(UNCHECKED)
	public static Map<String, Object> prestadorSolicitudAdapterMap(Map<String, Object> map) {
		if (map == null) {
			return null;
		}
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> mapPrestador = (Map<String, Object>) map.get("prestador");
		Map<String, Object> mapLugarAtencion = (Map<String, Object>) map.get("lugarDeAtencion");
		
		PrestadorMedicoAsistencial prestador = new PrestadorMedicoAsistencial();
		
		if (mapPrestador != null) {
			prestador.setId((Integer) mapPrestador.get(KEY_ID));
			prestador.setTelefono((String) mapPrestador.get("telefono"));
		}
		
		resultMap.put("prestador", prestador);
		resultMap.put("lugarDeAtencion", buildLugarDeAtencion(mapLugarAtencion));
		
		return resultMap;
	}
	
	@SuppressWarnings(UNCHECKED)
	public static Map<String, Object> profesionalesSolicitudAdapterMap(Map<String, Object> map) {
		if (map == null) {
			return null;
		}
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
				
		Map<String, Object> mapProfesionalEfector = (Map<String, Object>) map.get("profesionalEfector");
		Map<String, Object> mapProfesionalPrescriptor = (Map<String, Object>) map.get("profesionalPrescriptor");
		Map<String, Object> mapLugarDeAtencionProfesionalEfector = (Map<String, Object>) map.get("lugarDeAtencionProfesionalEfector");
		Map<String, Object> mapLugarDeAtencionProfesionalPrescriptor =(Map<String, Object>) map.get("lugarDeAtencionProfesionalPrescriptor");
		
		PrestadorMedicoAsistencialProfesional profesionalEfector = buildPrestadorProfesional(mapProfesionalEfector);
		PrestadorMedicoAsistencialProfesional profesionalPrescriptor = buildPrestadorProfesional(mapProfesionalPrescriptor);
		LugarDeAtencion lugarDeAtencionProfesionalEfector = buildLugarDeAtencion(mapLugarDeAtencionProfesionalEfector);
		LugarDeAtencion lugarDeAtencionProfesionalPrescriptor = buildLugarDeAtencion(mapLugarDeAtencionProfesionalPrescriptor);
		
		resultMap.put("profesionalEfector", profesionalEfector);
		resultMap.put("profesionalPrescriptor", profesionalPrescriptor);
		resultMap.put("lugarDeAtencionProfesionalEfector", lugarDeAtencionProfesionalEfector);
		resultMap.put("lugarDeAtencionProfesionalPrescriptor", lugarDeAtencionProfesionalPrescriptor);		
		
		return resultMap;
	}	
	
	@SuppressWarnings(UNCHECKED)
	private static LugarDeAtencion buildLugarDeAtencion(Map<String, Object> mapLugarDeAtencion) {
		LugarDeAtencion lugarDeAtencion = new LugarDeAtencion();
		Map<String, Object> mapDomicilio = null;
		if (mapLugarDeAtencion != null) {
			mapDomicilio = (Map<String, Object>) mapLugarDeAtencion.get("domicilio");
			if (mapDomicilio != null) {
				lugarDeAtencion.setDomicilio(buildDomicilio(mapDomicilio));
			}
			lugarDeAtencion.setId((Integer) mapLugarDeAtencion.get(KEY_ID));	
			lugarDeAtencion.setTelefono((String) mapLugarDeAtencion.get("telefono"));				
		}
		return lugarDeAtencion;
	}

	private static PrestadorMedicoAsistencialProfesional buildPrestadorProfesional(Map<String, Object> mapPrestadorProfesional) {
		PrestadorMedicoAsistencialProfesional prestadorProfesional = new PrestadorMedicoAsistencialProfesional();
		PersonaFisica tipoPersona = new PersonaFisica();
		if (mapPrestadorProfesional != null) {
			tipoPersona.setApellido((String) mapPrestadorProfesional.get("apellido"));
			prestadorProfesional.setId((Integer) mapPrestadorProfesional.get(KEY_ID));
			prestadorProfesional.setNumeroMatriculaNacional((String) mapPrestadorProfesional.get("numeroMatriculaNacional"));
			prestadorProfesional.setTipoPersona(tipoPersona);
			prestadorProfesional.setTelefono((String) mapPrestadorProfesional.get("telefono"));
			prestadorProfesional.setDireccionEmail((String) mapPrestadorProfesional.get("direccionEmail"));
		}		
		return prestadorProfesional;
	}

	@SuppressWarnings(UNCHECKED)
	private static Domicilio buildDomicilio(Map<String, Object> mapDomicilio) {
		Domicilio domicilio = new Domicilio();
		
		Map<String, Object> mapBarrio = (Map<String, Object>) mapDomicilio.get("barrio");
		Map<String, Object> mapLocalidad = (Map<String, Object>) mapDomicilio.get("localidad");
		Map<String, Object> mapPartido = (Map<String, Object>) mapDomicilio.get("partido");
		Map<String, Object> mapProvincia = (Map<String, Object>) mapDomicilio.get("provincia");
		Map<String, Object> mapRegion = (Map<String, Object>) mapDomicilio.get("region");
		
		domicilio.setCalle((String) mapDomicilio.get("calle"));
		domicilio.setNumero((Integer) mapDomicilio.get("numero"));
		domicilio.setPiso((String) mapDomicilio.get("piso"));
		domicilio.setDepartamento((String) mapDomicilio.get("depto"));
		domicilio.setCodigoPostalAsString((String) mapDomicilio.get("codigoPostal"));
		
		domicilio.setBarrio(buildBarrio(mapBarrio));
		domicilio.setLocalidad(buildLocalidad(mapLocalidad));
		domicilio.setPartido(buildPartido(mapPartido));
		domicilio.setProvincia(buildProvincia(mapProvincia));
		domicilio.setRegion(buildRegion(mapRegion));
		
		return domicilio;
	}
	
	private static Barrio buildBarrio(Map<String, Object> mapBarrio) {
		Barrio barrio = new Barrio();
		if (mapBarrio != null) {
			barrio.setId((Integer) mapBarrio.get(KEY_ID));
			barrio.setDescripcion((String) mapBarrio.get(KEY_DESCRIPCION));			
		}
		return barrio;
	}
	private static Localidad buildLocalidad(Map<String, Object> mapLocalidad) {
		Localidad localidad = new Localidad();
		if (mapLocalidad != null) {
			localidad.setId((Integer) mapLocalidad.get(KEY_ID));
			localidad.setDescripcion((String) mapLocalidad.get(KEY_DESCRIPCION));			
		}
		return localidad;
	}
	
	private static Partido buildPartido(Map<String, Object> mapPartido) {
		Partido partido = new Partido();
		if (mapPartido != null) {
			partido.setId((Integer) mapPartido.get(KEY_ID));
			partido.setDescripcion((String) mapPartido.get(KEY_DESCRIPCION));			
		}
		return partido;
	}
	
	private static Provincia buildProvincia(Map<String, Object> mapProvincia) {
		Provincia provincia = new Provincia();
		if (mapProvincia != null) {
			provincia.setId((Integer) mapProvincia.get(KEY_ID));
			provincia.setDescripcion((String) mapProvincia.get(KEY_DESCRIPCION));			
		}
		return provincia;
	}
	
	private static Region buildRegion(Map<String, Object> mapRegion) {
		Region region = new Region();
		if (mapRegion != null) {
			region.setId((Integer) mapRegion.get(KEY_ID));
			region.setDescripcion((String) mapRegion.get(KEY_DESCRIPCION));	
		}
		return region;
	}
	
}
