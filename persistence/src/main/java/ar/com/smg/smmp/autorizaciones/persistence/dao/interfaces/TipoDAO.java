package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import java.util.List;
import java.util.Map;

import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoExcepcion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoFinalizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoRechazo;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAgrupacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAutorizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoGestion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoPaciente;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoProvisionObraSocial;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoResolucion;

public interface TipoDAO extends DAO {

	String TRAE_TIPOS_AUTORIZACION = "trae_tipos_autorizacion";
	String TRAE_TIPOS_GESTION = "trae_tipos_gestion";
	String TRAE_TIPOS_PACIENTE = "trae_tipos_paciente";
	String TRAE_TIPOS_PROVISION_OBRA_SOCIAL = "trae_tipos_provision_obra_social";
	String TRAE_TIPOS_AGRUPACION = "trae_tipos_agrupacion";
	String TRAE_TIPOS_RESOLUCION = "trae_tipos_resolucion";
	String TRAE_TIPOS_RESOLUCION_ST = "trae_tipos_resolucion_st";
	String TRAE_MOTIVOS_EXCEPCION = "trae_motivos_excepcion";
	String TRAE_MOTIVOS_RECHAZO = "trae_motivos_rechazo";
	String TRAE_MOTIVOS_FINALIZACION = "trae_motivos_finalizacion";
	String TRAE_TIPOS_PRACTICA_ODO = "trae_tipos_practica_odo";
	String TRAE_MOTIVOS_FINALIZACION_ODO = "trae_motivos_finalizacion_odo";
	String TRAE_RESOLUCIONES_ODO = "trae_resoluciones_odo";
	String TRAE_MOTIVOS_NO_REPROGRAMACION_CIRUGIA = "trae_motivos_no_reprogramacion_cirugia";
	String TRAE_ESTUDIOS_ADJUNTOS_MDC = "trae_estudios_adjuntos_mdc";
	String TRAE_OBRAS_SOCIALES_MDC = "trae_obras_sociales_mdc";
	String ESTADO_AUTORIZACIONES_SGI = "estado_autorizaciones_sgi";
	String CLASIFICACION_AUTORIZACIONES_SGI = "clasificacion_autorizaciones_sgi";
	String OBTENER_MANDATARIAS_AUTORIZACIONES_SGI = "obtener_mandatarias_autorizaciones_sgi";
	//mcastillo
	String OBTENER_ESTUDIOS_TIPO_AUTORIZACIONES_SGI = "obtener_estudios_por_tipo_autorizacion";
	
	String TRAE_SUBMOTIVOS_FINALIZACION = "trae_submotivos_finalizacion";
	
	List<TipoAutorizacion> traeTiposAutorizacion();

	List<TipoGestion> traeTiposGestion();

	List<TipoPaciente> traeTiposPaciente();

	List<TipoProvisionObraSocial> traeTiposProvisionObraSocial();

	List<TipoAgrupacion> traeTiposAgrupacion();

	List<TipoResolucion> traeTiposResolucion();

	List<MotivoExcepcion> traeMotivosExcepcion();

	List<MotivoRechazo> traeMotivosRechazo();

	List<MotivoFinalizacion> traeMotivosFinalizacion(String tarea);

	List<TipoResolucion> traeTiposResolucionST();
	
	List<Map<String, Object>> traeTiposPracticaOdo();
	
	List<Map<String, Object>> traeMotivosFinalizacionOdo(Integer codigo, Integer grupo);
	
	List<Map<String, Object>> traeResolucionesOdo(Integer codigo);

	List<Map<String, Object>> traeMotivosNoReprogramacionCirugia();

	List<Map<String, Object>> traeEstudiosAdjuntosMdc(Long fecha);

	List<Map<String, Object>> traeObrasSocialesMdc();

	List<Map<String, Object>> estadoAutorizacionesSGI();
	
	List<Map<String, Object>> clasificacionAutorizacionesSGI();
	
	List<Map<String, Object>> obtenerMandatariasAutorizacionesSGI(String autorizaciones);

	//mcastillo
	List<Map<String, Object>> obtenerEstudiosPorTipoAutorizacionSGI(Long fecha, String tipoAutorizacion);
	
	List<Map<String, Object>> traeSubmotivosFinalizacion(Map<String, Object> map);
	
	
}
