package ar.com.smg.smmp.autorizaciones.persistence.dao.implementations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.SupportDAO;

public class SupportDAOImpl implements SupportDAO {

	private SqlSession session;
	
	public SupportDAOImpl() {
		session = null;
	}
	
	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void setSession(SqlSession session) {
		this.session = session;
	}

	@Override
	public List<String> traeDatosEmailAutorizacion(Map<String, Object> map) {
		return session.selectList(TRAE_DATOS_EMAIL_AUTORIZACION, map);
	}

	@Override
	public List<String> getMailDataPrestador(HashMap<String, Object> params) {
		return session.selectList(TRAE_DATOS_EMAIL_AUTORIZACION_PRESTADOR, params);
	}

}
