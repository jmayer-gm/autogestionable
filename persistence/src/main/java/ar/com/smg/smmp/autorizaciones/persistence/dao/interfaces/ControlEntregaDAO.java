package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import java.util.Map;

import ar.com.smg.smmp.model.autorizaciones.gestion.ControlEntrega;

public interface ControlEntregaDAO extends DAO {

	String INSERTA_CONTROL_ENTREGA = "inserta_control_entrega";
	String TRAE_CONTROL_ENTREGA = "trae_datos_control_entrega";

	void insertaControlEntrega(Map<String, Object> map);
	ControlEntrega traeControlEntrega(Integer etapaId);
}
