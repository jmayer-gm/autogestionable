package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.AutogestionableDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.AutorizacionDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.AutorizacionDAO;
import ar.com.smg.smmp.core.SMMPRuntimeException;
import ar.com.smg.smmp.model.autorizaciones.gestion.AutorizacionNormalizado;

public class AutorizacionTransactionManager {

	private static final Logger LOGGER = Logger.getLogger(AutorizacionTransactionManager.class);

	private static final String TRAER_AUTORIZACION_ERROR_MESSAGE = "Ha ocurrido un error al traer la autorizacion de la sucursal {0} con el id {1}";
	private static final String OBTENER_LOGS_AUDITORIA_ERROR_MESSAGE = "Ha ocurrido un error al traer los logs de la autorizacion de la sucursal {0} con el id {1}";
	public static String OBSERVADO = "Observado";

	private AutorizacionDAO autorizacionDAO;

	// ************************** AUTORIZACION (NO
	// LLAMADO)**************************
	public AutorizacionNormalizado traerAutorizacionXSucursalYId(Integer sucursalId, Integer autorizacionId)
			throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		AutorizacionNormalizado autoNorm;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			autoNorm = autorizacionDAO.traerAutorizacionXSucursalYId(sucursalId, autorizacionId);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TRAER_AUTORIZACION_ERROR_MESSAGE, sucursalId, autorizacionId), e);
			throw new IOException(MessageFormat.format(TRAER_AUTORIZACION_ERROR_MESSAGE, sucursalId, autorizacionId),
					e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return autoNorm;
	}

	/**
	 * Traer autorizaciones de tipo cuidado domiciliarios que tenga equipamiento.
	 * 
	 * @return
	 * @throws IOException
	 */
	public List<Map<String, Object>> traeAutorizacionConEquipos() throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerAutorizacionConEquipos();
		} catch (IOException e) {
			// LOGGER.error(MessageFormat.format(TRAER_AUTORIZACION_ERROR_MESSAGE,
			// sucursalId, autorizacionId), e);
			throw new IOException();
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return lista;
	}

	public List<Map<String, Object>> buscarEquiposXAutorizacion(Integer sucursal, Integer autorizacionId)
			throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerEquiposXAutorizacion(sucursal, autorizacionId);
		} catch (IOException e) {
			// LOGGER.error(MessageFormat.format(TRAER_AUTORIZACION_ERROR_MESSAGE,
			// sucursalId, autorizacionId), e);
			throw new IOException();
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return lista;
	}

	public List<Map<String, Object>> buscarModulosXAutorizacion(Integer sucursal, Integer autorizacionId)
			throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerModulosXAutorizacion(sucursal, autorizacionId);
		} catch (IOException e) {
			// LOGGER.error(MessageFormat.format(TRAER_AUTORIZACION_ERROR_MESSAGE,
			// sucursalId, autorizacionId), e);
			throw new IOException();
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return lista;
	}

//	public Map<String, Object> traerAutorizacionesPorPrestador(Map<String, Object> map) throws IOException {
//		SqlSession session = null;
//		autorizacionDAO = new AutorizacionDAOImpl();
//		
//		try {
//			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
//			autorizacionDAO.setSession(session);
//			return autorizacionDAO.traerAutorizacionesPorPrestador(map);
//		} 
//		catch (IOException e) {
//			LOGGER.error(TRAER_SALAS_ERROR_MESSAGE, e);
//			throw new SMMPRuntimeException(TRAER_SALAS_ERROR_MESSAGE, e);
//		}
//		finally {
//			if(session != null) {
//				session.close();
//			}
//		}
//	}

	public List<Map<String, Object>> obtenerLogsAutorizacionXSucursalYId(Integer sucursalId, Integer autorizacionId)
			throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.obtenerLogsAutorizacionXSucursalYId(sucursalId, autorizacionId);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(OBTENER_LOGS_AUDITORIA_ERROR_MESSAGE, sucursalId, autorizacionId), e);
			throw new IOException(
					MessageFormat.format(OBTENER_LOGS_AUDITORIA_ERROR_MESSAGE, sucursalId, autorizacionId), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return lista;
	}

	public Map<String, Object> recoverAutoByWorkflowId(Integer workflowId) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		Map<String, Object> lista;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.recoverAutoByWorkflowId(workflowId);
		} catch (IOException e) {
			LOGGER.error("Error obteniendo autorizacion by workflowId", e);
			throw new IOException("Error obteniendo autorizacion by workflowId", e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return lista;
	}

	public void expedientesAutorizacion(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			autorizacionDAO.expedientesAutorizacion(map);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public Map<String, Object> validaTopesPrestacion(HashMap<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		Map<String, Object> result = null;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autorizacionDAO.setSession(session);
			result = autorizacionDAO.validaTopesPrestacion(params);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return result;
	}

	public Map<String, Object> mandatariaByRudiId(Integer rudiId) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			return autorizacionDAO.mandatariaByRudiId(rudiId);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public Map<String, Object> traerAutogestionable(Map<String, Object> map) {
		SqlSession session = null;
		AutogestionableDAOImpl autogestionableDAO = new AutogestionableDAOImpl();
		Map<String, Object> result = null;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autogestionableDAO.setSession(session);
			result = autogestionableDAO.traeAutogestionable(map);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return result;
	}

	public List<Map<String, Object>> traerAutogestionables() {
		SqlSession session = null;
		AutogestionableDAOImpl autogestionableDAO = new AutogestionableDAOImpl();
		List<Map<String, Object>> result = null;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autogestionableDAO.setSession(session);
			result = autogestionableDAO.traeAutogestionables();
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return result;
	}
	public List<Map<String, Object>> traerDiagnosticos(Map<String, Object> map) {
		SqlSession session = null;
		AutogestionableDAOImpl autogestionableDAO = new AutogestionableDAOImpl();
		List<Map<String, Object>> result = null;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autogestionableDAO.setSession(session);
			result = autogestionableDAO.traeDiagnosticos(map);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return result;
	}
	public Map<String, Object> traerTramite(HashMap<String, Object> body, String tramite) {
		SqlSession session = null;
		AutogestionableDAOImpl autogestionableDAO = new AutogestionableDAOImpl();
		Map<String, Object> result = null;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autogestionableDAO.setSession(session);
			result = autogestionableDAO.traeTramite(body);
			if (result == null)
				return result;
			Boolean faltaDocumentacion = OBSERVADO.equals(result.get("estado"));
			result.put("nro_tramite", tramite);
			if (faltaDocumentacion) {
				List<Map<String, Object>> docu = documentacion(body);
				result.put("documentacion", docu);
			}
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return result;
	}

	public List<Map<String, Object>> traerDocumentacion(Integer workflowId) {
		SqlSession session = null;
		AutogestionableDAOImpl autogestionableDAO = new AutogestionableDAOImpl();
		List<Map<String, Object>> result = null;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autogestionableDAO.setSession(session);
			result = autogestionableDAO.traeDocumentacionFaltante(workflowId);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return result;
	}

	private List<Map<String, Object>> documentacion(HashMap<String, Object> body) {
		List<Map<String, Object>> docu = traerDocumentacion((Integer) body.get("workflow_id"));
		List<Map<String, Object>> docuFilter = new ArrayList<>();
		for (Map<String, Object> m : docu) {
			Map<String, Object> map = new HashMap<>();
			map.put("descripcion", m.get("description"));
			docuFilter.add(map);
		}
		return docuFilter;
	}

	public List<Map<String, Object>> traerCoberturaMadre(Map<String, Object> map) {
		SqlSession session = null;
		AutogestionableDAOImpl autogestionableDAO = new AutogestionableDAOImpl();
		List<Map<String, Object>> result = null;

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autogestionableDAO.setSession(session);
			result = autogestionableDAO.traeCoberturas(map);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return result;
	}
}
