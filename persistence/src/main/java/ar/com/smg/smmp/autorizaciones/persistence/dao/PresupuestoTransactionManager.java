package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.PresupuestoDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.PresupuestoDAO;

public class PresupuestoTransactionManager {

	private static final Logger LOGGER = Logger.getLogger(PresupuestoTransactionManager.class);
	private static final String PRESUPUESTO_ERROR_MESSAGE = "Ha ocurrido un error. {0}";

	private PresupuestoDAO presupuestoDao;
	private SqlSession session;

	private SqlSession getSqlSession() throws IOException {
		session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
		return session;
	}
	
	private SqlSession getTxSqlSession() throws IOException {
		session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
		return session;
	}

	private void closeSqlSession() throws IOException {
		if (session != null) {
			session.close();
		}
	}

	public List<Map<String, Object>> motivosPresupuesto(Map<String, Object> tipoTrm) throws IOException {
		List<Map<String, Object>> motivosPresupuesto = null;
		presupuestoDao = new PresupuestoDAOImpl();
		try {
			presupuestoDao.setSession(this.getSqlSession());
			motivosPresupuesto = presupuestoDao.motivosPresupuesto(tipoTrm);
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(PRESUPUESTO_ERROR_MESSAGE, "motivos presupuesto"), e);
			throw new IOException(MessageFormat.format(PRESUPUESTO_ERROR_MESSAGE, "motivos presupuesto") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return motivosPresupuesto;
	}

	@SuppressWarnings("unchecked")
	public void saveReqTrmPresupuesto(HashMap<String, Object> dataReqPresupuesto) throws IOException {
		presupuestoDao = new PresupuestoDAOImpl();
		try {
			presupuestoDao.setSession(this.getTxSqlSession());
			
			presupuestoDao.saveDataPresupuesto(dataReqPresupuesto);
			
			presupuestoDao.deleteDataPresupuestoMotivos(Long.valueOf(dataReqPresupuesto.get("processId").toString()));
			
			List<Map<String, Object>> motivos = (List<Map<String, Object>>) dataReqPresupuesto.get("motivos");
			if (motivos != null) {
				for (Map<String, Object> motivo : motivos) {
					presupuestoDao.saveDataPresupuestoMotivo(motivo);
				}
			}
			
			presupuestoDao.getSession().commit();
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(PRESUPUESTO_ERROR_MESSAGE, "grabando requiere presupuesto data"), e);
			throw new IOException(MessageFormat.format(PRESUPUESTO_ERROR_MESSAGE, "grabando requiere presupuesto data") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		
	}

	public Map<String, Object> reqTrmPresupuesto(Long processId) throws IOException {
		Map<String, Object> reqTrmPresupuesto = null;
		presupuestoDao = new PresupuestoDAOImpl();
		try {
			presupuestoDao.setSession(this.getSqlSession());
			
			reqTrmPresupuesto = presupuestoDao.readReqTrmPresupuesto(processId);
			if (reqTrmPresupuesto != null) {
				List<Map<String, Object>> motivos = presupuestoDao.readReqTrmPresupuestoMotivos(processId);
				reqTrmPresupuesto.put("motivos", motivos);
			}
			
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(PRESUPUESTO_ERROR_MESSAGE, "recuperando requiere presupuesto data"), e);
			throw new IOException(MessageFormat.format(PRESUPUESTO_ERROR_MESSAGE, "recuperando requiere presupuesto data") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}

		return reqTrmPresupuesto;
	}

	public List<Map<String, Object>> tramitesPresupuestoAsociado(Long processId) throws IOException {
		List<Map<String, Object>> tramites = null;
		presupuestoDao = new PresupuestoDAOImpl();
		try {
			presupuestoDao.setSession(this.getSqlSession());
			
			tramites = presupuestoDao.tramitesPresupuestoAsociado(processId);
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(PRESUPUESTO_ERROR_MESSAGE, "recuperando requiere presupuesto data"), e);
			throw new IOException(MessageFormat.format(PRESUPUESTO_ERROR_MESSAGE, "recuperando requiere presupuesto data") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}

		return tramites;
	}

	public void updateReqTrmPresupuesto(HashMap<String, Object> dataReqPresupuesto) throws IOException {
		presupuestoDao = new PresupuestoDAOImpl();
		try {
			presupuestoDao.setSession(this.getTxSqlSession());
			
			presupuestoDao.updateDataPresupuesto(dataReqPresupuesto);
			
			presupuestoDao.getSession().commit();
		}
		catch (IOException e) {
			LOGGER.error(MessageFormat.format(PRESUPUESTO_ERROR_MESSAGE, "update requiere presupuesto data"), e);
			throw new IOException(MessageFormat.format(PRESUPUESTO_ERROR_MESSAGE, "update requiere presupuesto data") + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
	}
	

}
