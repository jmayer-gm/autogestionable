package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface PresupuestoDAO extends DAO {

	
	String MOTIVOS_PRESUPUESTO = "motivos_presupuesto";
	String SAVE_DATA_PRESUPUESTO = "save_data_presupuesto";
	String UPDATE_DATA_PRESUPUESTO = "update_data_presupuesto";
	String DELETE_DATA_PRESUPUESTO_MOTIVO = "delete_data_presupuesto_motivo";
	String SAVE_DATA_PRESUPUESTO_MOTIVO = "save_data_presupuesto_motivo";
	
	String READ_REQ_TRM_PRESUPUESTO = "read_req_trm_presupuesto";
	String READ_REQ_TRM_PRESUPUESTO_MOTIVOS = "read_req_trm_presupuesto_motivos";

	String READ_TRAMITES_PRESUPUESTO_ASOCIADOS = "read_tramites_presupuesto_asociados";
	
	List<Map<String, Object>> motivosPresupuesto(Map<String, Object> tipoTrm);

	void saveDataPresupuesto(HashMap<String, Object> dataReqPresupuesto);

	void deleteDataPresupuestoMotivos(Long processId);

	void saveDataPresupuestoMotivo(Map<String, Object> motivo);
	
	Map<String, Object> readReqTrmPresupuesto(Long processId);

	List<Map<String, Object>> readReqTrmPresupuestoMotivos(Long processId);

	List<Map<String, Object>> tramitesPresupuestoAsociado(Long processId);

	void updateDataPresupuesto(HashMap<String, Object> dataReqPresupuesto);
	
}
