package ar.com.smg.smmp.autorizaciones.persistence.dao.implementations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.ProductoDAO;

public class ProductoDAOImpl implements ProductoDAO {

	private SqlSession session;

	public ProductoDAOImpl() {
		session = null;
	}

	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void setSession(SqlSession session) {
		this.session = session;
	}

	
	@Override
	public List<Map<String, Object>> monodroga(String denoDroga) {
		Map<String, Object> mapParamsSP = new HashMap<String, Object>();
		// Paramatros para el Store Procedure
		mapParamsSP.put("denoDroga", denoDroga);
		
		return session.selectList(MONODROGA, mapParamsSP);
	}

	@Override
	public List<Map<String, Object>> productosSearch(String productoDeno, String troquel) {
		Map<String, Object> mapParamsSP = new HashMap<String, Object>();
		// Paramatros para el Store Procedure
		mapParamsSP.put("productoDeno", productoDeno);
		mapParamsSP.put("troquel", troquel);

		return session.selectList(PRODUCTOS_SEARCH, mapParamsSP);
	}
	
	@Override
	public List<Map<String, Object>> productosPresmedSearch(String productoCodigo) {
		Map<String, Object> mapParamsSP = new HashMap<String, Object>();
		// Paramatros para el Store Procedure
		mapParamsSP.put("productoCodigo", productoCodigo);
		mapParamsSP.put("prepaga", null);
		mapParamsSP.put("vigencia", null);

		mapParamsSP.put("codInterno", 'C');
		//returns
		/*mapParamsSP.put("labo", null);
		mapParamsSP.put("barra", null);
		mapParamsSP.put("troquel", null);
		mapParamsSP.put("deno", null);
		mapParamsSP.put("inye", null);
		mapParamsSP.put("rubro", null);
		mapParamsSP.put("flia", null);
		mapParamsSP.put("ori", null);		
		mapParamsSP.put("tama", null);
		mapParamsSP.put("eti", null);		
		mapParamsSP.put("dosisInye", null);		
		mapParamsSP.put("actuFecha", null);		
		mapParamsSP.put("altaFecha", null);
		mapParamsSP.put("bajaFecha", null);
		mapParamsSP.put("err", null);
		mapParamsSP.put("gestionSolped", null);*/

		return session.selectList(PRODUCTOS_PRESMED_SEARCH, mapParamsSP);
	}


	@Override
	public List<Map<String, Object>> productosByDroga(String droga) {
		Map<String, Object> mapParamsSP = new HashMap<String, Object>();
		// Paramatros para el Store Procedure
		mapParamsSP.put("droga", droga);
		
		return session.selectList(PRODUCTOS_BY_DROGA, mapParamsSP);
	}
	
	@Override
	public List<Map<String, Object>> obtenerProductosFarmaciaAutorizacionesSGI(Integer sucur, Integer auto) {
		Map<String, Object> mapParamsSP = new HashMap<String, Object>();
		// Paramatros para el Store Procedure
		mapParamsSP.put("sucur", sucur);
		mapParamsSP.put("auto", auto);
		
		return session.selectList(OBTENER_PRODUCTOS_FARMACIA_AUTORIZACIONES_SGI, mapParamsSP);
	}
	
	@Override
	public Map<String, Object> productoByCodigo(String produ) {
		Map<String, Object> mapParamsSP = new HashMap<String, Object>();
		// Paramatros para el Store Procedure
		mapParamsSP.put("produ", produ);
		
		return session.selectOne(OBTENER_PRODUCTO_POR_CODIGO_SGI, mapParamsSP);
	}
}	
