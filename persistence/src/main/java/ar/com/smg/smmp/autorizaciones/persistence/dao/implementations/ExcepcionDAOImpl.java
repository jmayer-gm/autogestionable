package ar.com.smg.smmp.autorizaciones.persistence.dao.implementations;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.ExcepcionDAO;
import ar.com.smg.smmp.model.autorizaciones.gestion.Excepcion;

public class ExcepcionDAOImpl implements ExcepcionDAO {

	private SqlSession session;
	
	public ExcepcionDAOImpl() {
		session = null;
	}
	
	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void setSession(SqlSession session) {
		this.session = session;
	}

	@Override
	public void insertaExcepcionAutorizacion(Map<String, Object> map) {
		session.insert(INSERTA_EXCEPCION_AUTORIZACION, map);		
	}

	@Override
	public void apruebaExcepcionAutorizacion(Map<String, Object> map) {
		session.insert(APRUEBA_EXCEPCION_AUTORIZACION, map);
	}
	
	@Override
	public Excepcion traeExcepcionAutorizacion(Integer etapaId) {
		return (Excepcion) session.selectOne(TRAE_EXCEPCION_AUTORIZACION, etapaId);
	}
	
}
