package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import java.util.Map;

import ar.com.smg.smmp.model.autorizaciones.gestion.Excepcion;

public interface ExcepcionDAO extends DAO {

	String INSERTA_EXCEPCION_AUTORIZACION = "inserta_excepcion_autorizacion";
	String TRAE_EXCEPCION_AUTORIZACION = "trae_datos_excepcion_autorizacion";
	String APRUEBA_EXCEPCION_AUTORIZACION = "aprobar_excepcion_autorizacion";
	
	void insertaExcepcionAutorizacion(Map<String, Object> map);
	void apruebaExcepcionAutorizacion(Map<String, Object> map);
	Excepcion traeExcepcionAutorizacion(Integer etapaId);
	
}
