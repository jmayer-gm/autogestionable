package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.com.smg.smmp.model.autorizaciones.autorizacion.Internacion;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Prorroga;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Sala;
import ar.com.smg.smmp.model.autorizaciones.gestion.Autorizacion;
import ar.com.smg.smmp.model.autorizaciones.gestion.AutorizacionNormalizado;
import ar.com.smg.smmp.model.global.Documentacion;
import ar.com.smg.smmp.model.prestadores.farmacia.Farmacia;


public interface AutorizacionDAO extends DAO {

	String TRAE_AUTORIZACION = "trae_datos_autorizacion";
	String TRAE_INTERNACIONES_AUTORIZACION = "trae_internaciones_autorizacion";
	String TRAE_AUTORIZACIONES_ASOCIADAS_SALA = "trae_autorizaciones_asociadas_sala";
	String TRAE_AUTORIZACIONES_ASOCIADAS = "trae_autorizaciones_asociadas";
	String INSERTA_PRORROGA_AUTORIZACION = "insertar_prorroga_autorizacion";
	String INSERTA_SALA_AUTORIZACION = "insertar_sala_autorizacion";
	String VALIDA_PRORROGA_AUTORIZACION = "valida_prorroga_autorizacion";
	String TRAE_SALAS = "trae_salas";
	String TRAE_DOCUMENTACION_SITUACION_TERAPEUTICA = "trae_documentacion_situacion_terapeutica";
	String TRAE_AUTORIZACION_POR_PRESTADOR = "trae_autorizacion_por_prestador";
	static final String TRAE_AUTORIZACION_POR_AFILIADO_EFECTOR	= "trae_autorizacion_por_afiliado_efector";
	static final String TIENE_AUTORIZACION_VINCULABLE	= "tiene_autorizacion_vinculable";
	String TRAE_AUTORIZACION_POR_SUCUR_Y_ID = "prestaci_trae_autorizacion";
	static final String TRAE_FARMACIAS = "trae_farmacias";
	static final String TRAE_FARMACIAS_BY_UBICACION = "trae_farmacias_ubicacion";
	String TRAE_AUTORIZACION_CON_EQUPIPOS = "prestaci_trae_autorizacion_con_equipos";
	String TRAE_EQUIPOS_X_AUTORIZACION = "prestaci_trae_equipos_autorizacion";
	String TRAE_MODULOS_X_AUTORIZACION = "prestaci_trae_modulos_autorizacion";
	
	String TRAE_AUTORIZACIONES_ODONTO_AFILIADO = "trae_autorizaciones_odonto_afiliado";
	String TRAE_DATOS_CAB_AUTORIZACION_ODO = "trae_datos_cab_autorizacion_odo";
	String TRAE_PRESTACIONES_AUTORIZACIONES_ODO = "trae_prestaciones_autorizaciones_odo";
	String TRAE_AUTORIZACIONES_ODONTO_PRESTADOR = "trae_autorizaciones_odonto_prestador";
	String TRAE_HISTORIAL_VALOR_LEY = "trae_historial_valor_ley";
	
	String SOLAPAMIENTO_AUTO = "solapamiento_auto";
	String VALIDAR_CONVENIO = "validar_convenio";
	String VALIDAR_CONVENIO_INTER = "validar_convenio_inter";
	String VALIDAR_CONVENIO_CIRU = "validar_convenio_ciru";
	String VALIDAR_COBERTURA = "validar_cobertura";
	String READ_TEMP_MODULOS = "read_temp_modulos";
	String VALIDAR_GENERO_Y_EDAD = "validar_genero_y_edad";
	String VALIDAR_CARENCIA = "validar_carencia";
	String TRAE_COPAGO_PRESTACION = "trae_copago_prestacion";
	String PLNS_CONFIG_MODULOS = "plns_config_modulos";
	
	String OBTENER_LOGS_AUDITORIA_AUTORIZACION = "prestaci_logs_auditoria_autorizacion";
	
	String TRAE_AUTORIZACION_BY_WORKFLOW_ID = "recover_auto_by_workflow_id";
	
	String TRAE_CLASIFICACIONES = "trae_clasificaciones";
	String TRAE_PRESTACIONES_AUTORIZACION = "trae_prestaciones_autorizacion";
	String EXPEDIENTES_AUTORIZACION = "expedientes_autorizacion";
	String VALIDA_TOPES_PRESTACION = "valida_topes_prestacion";
	
	String TRAE_DESCRIPCION_RECHA = "trae_descripcion_recha";
	
	String INSERTA_AUDITORIA = "inserta_auditoria";
	
	String OBTENER_PORCE_DTO_PRODUCTO = "obtener_porce_dto_producto";
	String VALIDAR_PRODUCTO = "validar_producto";
	String MANDATARIA_BY_RUDI_ID = "mandataria_by_rudi_id";
	String REGISTRA_ENVIO_A_FARMALINK = "registra_envio_a_farmalink";
	
	String TRAE_MOTIVOS_ODO = "trae_motivos_odo";
	String TRAE_RAZONES_ODO = "trae_razones_odo";
	String OBTENER_CONVERSION_SAP = "obtener_conversion_sap";
	
	Autorizacion traeAutorizacion(Integer etapaId);
	List<Internacion> traeInternacionesAutorizacion(Map<String, Object> map);
	List<Map<String, Object>> traeAutorizacionesAsociadasConSala(Map<String, Object> map);
	List<Prorroga> traeAutorizacionesAsociadas(Map<String, Object> map);
	Prorroga validaProrrogaAutorizacion(Map<String, Object> map);
	Integer insertaProrrogaAutorizacion(Prorroga prorroga);
	void insertaSalaAutorizacion(Map<String, Object> map);
	List<Sala> traeSalas(Map<String, Object> map);
	List<Documentacion> traerDocumentacionSituacionTerapeutica(Map<String, Object> map);
	Map<String, Object>  traerAutorizacionesPorPrestador(Map<String, Object> map);
	List<Map<String, Object>> traerAutorizacionesPorAfiliadoEfector(Map<String, Object> map); 
	Map<String, Object> tieneAutorizacionVinculable(Map<String, Object> map); 
	AutorizacionNormalizado traerAutorizacionXSucursalYId(Integer sucursal,	Integer autorizacionId);
	List<Farmacia> traerFarmacias(Map<String, Object> map);
	List<Farmacia> traerFarmaciasByUbicacion(Map<String, Object> map);
	List<Map<String, Object>>  traerAutorizacionConEquipos();
	List<Map<String, Object>>  traerEquiposXAutorizacion(Integer sucursal,Integer autorizacionId);
	List<Map<String, Object>>  traerModulosXAutorizacion(Integer sucursal,Integer autorizacionId);
	List<Map<String, Object>> traerAutorizacionesOdontoAfiliado(Map<String, Object> map);
	Map<String, Object> traerDatosCabAutorizacionesOdo(Map<String, Object> map);
	List<Map<String, Object>> traerPrestacionesAutorizacionesOdo(Map<String, Object> map);
	List<Map<String, Object>> traerAutorizacionesOdontoPrestador(Map<String, Object> map);
	List<Map<String, Object>> validarAutorizacionesSolapadas(HashMap<String, Object> params);
	List<Map<String, Object>> validarConvenio(HashMap<String, Object> params);
	List<Map<String, Object>> validarConvenioInter(HashMap<String, Object> params);
	List<Map<String, Object>> validarConvenioCiru(HashMap<String, Object> params);
	void validarCobertura(HashMap<String, Object> params);
	List<Map<String, Object>> validarGeneroYEdad(HashMap<String, Object> params);
	void validarCarencia(HashMap<String, Object> params);
	void traeCopagoPrestacion(Map<String, Object> params);
	List<Map<String, Object>> traerHistorialValorLey(Map<String, Object> map);
	List<Map<String, Object>> obtenerLogsAutorizacionXSucursalYId(Integer sucursal, Integer autorizacion);
	Map<String, Object> recoverAutoByWorkflowId(Integer workflowId);
	List<Map<String, Object>> traerClasificaciones(Map<String, Object> map);
	List<Map<String, Object>> traePrestacionesAutorizacion(Map<String, Object> map);
	List<Map<String, Object>> traerDescripcionRecha(Map<String, Object> map);
	void auditarAutorizacion(Map<String, Object> map);
	void expedientesAutorizacion(Map<String, Object> map);
	Map<String, Object> validaTopesPrestacion(Map<String, Object> map);
	List<Map<String, Object>> plnsConfigModulos(Map<String, Object> map);
	Map<String, Object> obtenerProceDtoProducto(HashMap<String, Object> params);
	Map<String, Object> validarProducto(HashMap<String, Object> params);
	Map<String, Object> mandatariaByRudiId(Integer rudiId);
	void registraEnvioAFarmalink(Map<String, Object> params);
	List<Map<String, Object>> traerRazonesOdo();
	List<Map<String, Object>> traerMotivosOdo(Map<String, Object> params);
	Map<String, Object> obtenerConversionProductoSap(HashMap<String, Object> params);
}
