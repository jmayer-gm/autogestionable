package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.ProductoDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.ProductoDAO;

public class ProductoTransactionManager {

	private static final Logger LOGGER = Logger.getLogger(ProductoTransactionManager.class);

	private static final String PRODUCTO_ERROR_MESSAGE = "Ha ocurrido un error al obtener los productos";
	private static final String DROGA_ERROR_MESSAGE = "Ha ocurrido un error al obtener las drogas";
	
	private ProductoDAO productoDao;
	private SqlSession session;

	private SqlSession getSqlSession() throws IOException {
		session = null;
		session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
		return session;
	}

        private void closeSqlSession() throws IOException  {
            if (session != null) {
                    session.close();
            }            
        }        
        
	public List<Map<String, Object>> monodroga(String denoDroga) throws IOException {
		List<Map<String, Object>> drogas = null;
		productoDao = new ProductoDAOImpl();
		try {
			productoDao.setSession(this.getSqlSession());
			drogas = productoDao.monodroga(denoDroga);
		}
		catch (IOException e) {
			LOGGER.error(DROGA_ERROR_MESSAGE, e);
			throw new IOException(DROGA_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return drogas;
	}

	public List<Map<String, Object>> productosSearch(String productoDeno, String troquel)
			throws IOException {
		List<Map<String, Object>> productos = null;
		productoDao = new ProductoDAOImpl();
		try {
			productoDao.setSession(this.getSqlSession());
			productos = productoDao.productosSearch(productoDeno, troquel);
		}
		catch (IOException e) {
			LOGGER.error(PRODUCTO_ERROR_MESSAGE, e);
			throw new IOException(PRODUCTO_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return productos;
	}
	
	
	public List<Map<String, Object>> productosPresmedSearch(String productoCodigo)
			throws IOException {
		List<Map<String, Object>> productos = null;
		productoDao = new ProductoDAOImpl();
		try {
			productoDao.setSession(this.getSqlSession());
			productos = productoDao.productosPresmedSearch(productoCodigo);
		}
		catch (IOException e) {
			LOGGER.error(PRODUCTO_ERROR_MESSAGE, e);
			throw new IOException(PRODUCTO_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return productos;
	}
	
	public List<Map<String, Object>> productosByDroga(String droga)
			throws IOException {
		List<Map<String, Object>> productos = null;
		productoDao = new ProductoDAOImpl();
		try {
			productoDao.setSession(this.getSqlSession());
			productos = productoDao.productosByDroga(droga);
		}
		catch (IOException e) {
			LOGGER.error(PRODUCTO_ERROR_MESSAGE, e);
			throw new IOException(PRODUCTO_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			closeSqlSession();
		}
		return productos;
	}

	public List<Map<String, Object>> obtenerProductosFarmaciaAutorizacionesSGI(Integer sucur, Integer auto) throws IOException{
		List<Map<String,Object>> productosFarmacia = null;
		productoDao = new ProductoDAOImpl(); 
		try{
			productoDao.setSession(this.getSqlSession());
			productosFarmacia = this.productoDao.obtenerProductosFarmaciaAutorizacionesSGI(sucur, auto);
		}
		catch(IOException e){
			LOGGER.error(PRODUCTO_ERROR_MESSAGE, e);
			throw new IOException(PRODUCTO_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally{
			closeSqlSession();
		}
		return productosFarmacia;
	}
	
	public Map<String, Object> productoByCodigo(String produ) throws IOException{
		Map<String,Object> producto = null;
		productoDao = new ProductoDAOImpl(); 
		try{
			productoDao.setSession(this.getSqlSession());
			producto = this.productoDao.productoByCodigo(produ);
		}
		catch(IOException e){
			LOGGER.error(PRODUCTO_ERROR_MESSAGE, e);
			throw new IOException(PRODUCTO_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally{
			closeSqlSession();
		}
		return producto;
	}

}
