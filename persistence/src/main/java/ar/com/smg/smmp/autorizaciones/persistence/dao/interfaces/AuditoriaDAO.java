package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import java.util.Map;

import ar.com.smg.smmp.model.autorizaciones.gestion.Auditoria;

public interface AuditoriaDAO extends DAO {

	String INSERTA_AUDITORIA_MEDICA = "inserta_auditoria_medica";
	String TRAE_AUDITORIA_MEDICA = "trae_datos_auditoria_medica";

	void insertaAuditoriaMedica(Map<String, Object> map);
	Auditoria traeAuditoriaMedica(Integer etapaId);
}
