package ar.com.smg.smmp.autorizaciones.persistence.dao.implementations;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.DerivacionDAO;
import ar.com.smg.smmp.model.autorizaciones.gestion.Derivacion;

public class DerivacionDAOImpl implements DerivacionDAO {

	private SqlSession session;
	
	public DerivacionDAOImpl() {
		session = null;
	}
	
	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void setSession(SqlSession session) {
		this.session = session;
	}

	@Override
	public void insertaDerivacionMedica(Map<String, Object> map) {
		session.insert(INSERTA_DERIVACION_MEDICA, map);		
	}

	@Override
	public Derivacion traeDerivacionMedica(Integer etapaId) {
		return session.selectOne(TRAE_DERIVACION_MEDICA, etapaId);
	}

}
