package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import java.util.List;
import java.util.Map;

public interface ProductoDAO extends DAO {

	String MONODROGA = "monodroga";
	String PRODUCTOS_SEARCH = "productosSearch";
	String PRODUCTOS_PRESMED_SEARCH = "productosPresmedSearch";
	String PRODUCTOS_BY_DROGA = "productosByDroga";
	String OBTENER_PRODUCTOS_FARMACIA_AUTORIZACIONES_SGI = "obtener_productos_farmacia_autorizaciones_sgi";
	String OBTENER_PRODUCTO_POR_CODIGO_SGI = "obtener_producto_por_codigo_sgi";
	

	List<Map<String, Object>> monodroga(String denoDroga);
	List<Map<String, Object>> productosSearch(String productoDeno, String troquel);
	List<Map<String, Object>> productosPresmedSearch(String productoCodigo);
	List<Map<String, Object>> productosByDroga(String droga);
	Map<String, Object> productoByCodigo(String produ);
	
	List<Map<String, Object>> obtenerProductosFarmaciaAutorizacionesSGI(Integer sucur, Integer auto);
	
	
}
