package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface SupportDAO extends DAO {

	String TRAE_DATOS_EMAIL_AUTORIZACION = "trae_datos_email_autorizacion";
	String TRAE_DATOS_EMAIL_AUTORIZACION_PRESTADOR = "trae_datos_email_autorizacion_prestador";

	List<String> traeDatosEmailAutorizacion(Map<String, Object> map);

	List<String> getMailDataPrestador(HashMap<String, Object> params);
	
}
