package ar.com.smg.smmp.autorizaciones.persistence.dao.implementations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.AutogestionableDao;

public class AutogestionableDAOImpl implements AutogestionableDao {

	private SqlSession session;

	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void setSession(SqlSession session) {
		this.session = session;
	}

	@Override
	public Map<String, Object> traeAutogestionable(Map<String, Object> map) {
		return session.selectOne(GET_AUTOGESTIONABLE, map);
	}
	
	@Override
	public List<Map<String, Object>> traeAutogestionables() {
		return session.selectList(GET_AUTOGESTIONABLES);
	}

	public Map<String, Object> traeTramite(HashMap<String, Object> body) {
		return session.selectOne(GET_TRAMITE, body);

	}

	@Override
	public List<Map<String, Object>> traeDocumentacionFaltante(Integer workflowId) {
		return session.selectList(GET_DOCUMENTACION, workflowId);

	}
	@Override
	public List<Map<String, Object>> traeDiagnosticos(Map<String, Object> map) {
		return session.selectList(GET_DIAGNOSTICOS);
	}

	public List<Map<String, Object>> traeCoberturas(Map<String, Object> map) {
		return session.selectList(GET_COBERTURAS_MADRES, map);

	}

}
