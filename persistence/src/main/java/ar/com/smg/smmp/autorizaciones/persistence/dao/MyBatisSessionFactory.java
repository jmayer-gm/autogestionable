package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public final class MyBatisSessionFactory {

	private static final String DS_PRESTACI_JNDI_NAME = "prestaci";
	private static final String DS_PRESTACI_XA_JNDI_NAME = "prestaciDSXA";
	private static final String DS_PRESTACI_AUTO_TRUE_JNDI_NAME = "prestaci_auto_true";
	private static final String DS_PRESTACI_NO_TRANSACTION_JNDI_NAME = "prestaci_auto_false";
	private static final String DS_CRM_SMG_NO_TRANSACTION_JNDI_NAME = "crm_smg_no_tx";
	private static final String DS_CRM_SMG_XA_JNDI_NAME = "crm_smg_XA";
	private static final String CONFIGURATION_FILE = "sql-map-config.xml";
	
	private static MyBatisSessionFactory instance;
	private static Object dbSessionFactoryLock = new Object();
	
	private Object sqlPrestaciSessionFactoryLock = new Object();
	private SqlSessionFactory prestaciSessionFactory;
	
	private Object sqlPrestaciAutoTrueSessionFactoryLock = new Object();
	private SqlSessionFactory prestaciAutoTrueSessionFactory;
	
	private Object sqlPrestaciXASessionFactoryLock = new Object();
	private SqlSessionFactory prestaciXASessionFactory;
	
	private Object sqlPrestaciNoTransactionalSessionFactoryLock = new Object();
	private SqlSessionFactory prestaciNoTransactionalSessionFactory;

	private Object sqlCrmSmgNoTransactionalSessionFactoryLock = new Object();
	private SqlSessionFactory crmSmgNoTransactionalSessionFactory;
	
	private Object sqlCrmSmgXATransactionalSessionFactoryLock = new Object();
	private SqlSessionFactory crmSmgXATransactionalSessionFactory;
	
	private MyBatisSessionFactory(){
	}	
	
	/**
	 * Singleton
	 * @return
	 * @throws IOException
	 */
	public static MyBatisSessionFactory getInstance() throws IOException {
		synchronized (dbSessionFactoryLock) {
			if(instance == null) {
				instance = new MyBatisSessionFactory();
			}
		}
		return instance;
	}
	
	/**
	 * Abre una session de mybatis con autocommit en false en Prestaci.
	 * @return
	 * @throws IOException
	 */
	public SqlSession getPrestaciNoTransactionalSession() throws IOException {
		return getPrestaciNoTransactionalSqlSessionFactory(DS_PRESTACI_NO_TRANSACTION_JNDI_NAME).openSession();
	}
	
	private SqlSessionFactory getPrestaciNoTransactionalSqlSessionFactory(String enviroment) throws IOException {
		synchronized (this.sqlPrestaciNoTransactionalSessionFactoryLock) {
			if(this.prestaciNoTransactionalSessionFactory == null ) {
				Reader reader = Resources.getResourceAsReader(CONFIGURATION_FILE);
				this.prestaciNoTransactionalSessionFactory = new SqlSessionFactoryBuilder().build(reader, enviroment);
			}
			return prestaciNoTransactionalSessionFactory;
		}
	}
	
	/**
	 * Abre una session de mybatis con autocommit en false en CRM.
	 * @return
	 * @throws IOException
	 */
	public SqlSession getCrmSmgNoTransactionalSession() throws IOException {
		return getCrmSmgNoTransactionalSqlSessionFactory(DS_CRM_SMG_NO_TRANSACTION_JNDI_NAME).openSession();
	}
	
	private SqlSessionFactory getCrmSmgNoTransactionalSqlSessionFactory(String enviroment) throws IOException {
		synchronized (this.sqlCrmSmgNoTransactionalSessionFactoryLock) {
			if(this.crmSmgNoTransactionalSessionFactory == null ) {
				Reader reader = Resources.getResourceAsReader(CONFIGURATION_FILE);
				this.crmSmgNoTransactionalSessionFactory = new SqlSessionFactoryBuilder().build(reader, enviroment);
			}
			return crmSmgNoTransactionalSessionFactory;
		}
	}
	
	/**
	 * Abre una session de mybatis para transacciones XA en CRM.
	 * @return
	 * @throws IOException
	 */
	public SqlSession getCrmSmgXASession() throws IOException {
		return getCrmSmgXASqlSessionFactory(DS_CRM_SMG_XA_JNDI_NAME).openSession();
	}
	
	private SqlSessionFactory getCrmSmgXASqlSessionFactory(String enviroment) throws IOException {
		synchronized (this.sqlCrmSmgXATransactionalSessionFactoryLock) {
			if(this.crmSmgXATransactionalSessionFactory == null ) {
				Reader reader = Resources.getResourceAsReader(CONFIGURATION_FILE);
				this.crmSmgXATransactionalSessionFactory = new SqlSessionFactoryBuilder().build(reader, enviroment);
			}
			return crmSmgXATransactionalSessionFactory;
		}
	}
	
	/**
	 * Abre una session de mybatis.
	 * @return
	 * @throws IOException
	 */
	public SqlSession getPrestaciSession() throws IOException {
		return getPrestaciSqlSessionFactory(DS_PRESTACI_JNDI_NAME).openSession();
	}
	
	private SqlSessionFactory getPrestaciSqlSessionFactory(String enviroment) throws IOException {
		synchronized (this.sqlPrestaciSessionFactoryLock) {
			if(this.prestaciSessionFactory == null ) {
				Reader reader = Resources.getResourceAsReader(CONFIGURATION_FILE);
				this.prestaciSessionFactory = new SqlSessionFactoryBuilder().build(reader, enviroment);
			}
			return prestaciSessionFactory;
		}
	}
		
	public SqlSession getPrestaciAutoTrueSession() throws IOException {
		return getPrestaciSqlSessionFactory(DS_PRESTACI_AUTO_TRUE_JNDI_NAME).openSession();
	}
	
	/**
	 * Abre una session de mybatis para transacciones XA en Prestaci.
	 * @return
	 * @throws IOException
	 */
	public SqlSession getPrestaciXASession() throws IOException {
		return getPrestaciXASqlSessionFactory(DS_PRESTACI_XA_JNDI_NAME).openSession();
	}
	
	private SqlSessionFactory getPrestaciXASqlSessionFactory(String enviroment) throws IOException {
		synchronized (this.sqlPrestaciXASessionFactoryLock) {
			if(this.prestaciXASessionFactory == null ) {
				Reader reader = Resources.getResourceAsReader(CONFIGURATION_FILE);
				this.prestaciXASessionFactory = new SqlSessionFactoryBuilder().build(reader, enviroment);
			}
			return prestaciXASessionFactory;
		}
	}
	
}
