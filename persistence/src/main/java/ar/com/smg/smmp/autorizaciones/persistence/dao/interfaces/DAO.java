package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import org.apache.ibatis.session.SqlSession;

public interface DAO {

	SqlSession getSession();
	void setSession(SqlSession session);
	
}
