package ar.com.smg.smmp.autorizaciones.persistence.dao.implementations;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.SolicitudAutorizacionDAO;
import ar.com.smg.smmp.model.autorizaciones.solicitud.SolicitudAutorizacion;

public class SolicitudAutorizacionDAOImpl implements SolicitudAutorizacionDAO {

	private SqlSession session;
	
	public SolicitudAutorizacionDAOImpl() {
		session = null;
	}
	
	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void setSession(SqlSession session) {
		this.session = session;
	}

	@Override
	public Integer insertaSolicitudAutorizacion(Map<String, Object> map) {
		return session.selectOne(INSERTA_SOLICITUD_AUTORIZACION, map);		
	}
	
	@Override
	public void actualizaSolicitudAutorizacion(Map<String, Object> map) {
		session.insert(ACTUALIZA_SOLICITUD_AUTORIZACION, map);
	}
	
	@Override
	public void insertaPrestadorSolicitudAutorizacion(Map<String, Object> map) {
		session.selectOne(INSERTA_PRESTADOR_SOLICITUD_AUTORIZACION, map);
	}
	
	@Override
	public void insertaProfesionalSolicitudAutorizacion(Map<String, Object> map) {
		session.selectOne(INSERTA_PROFESIONAL_SOLICITUD_AUTORIZACION, map);
	}

	@Override
	public SolicitudAutorizacion traeSolicitudAutorizacion(Integer etapaId) {
		return (SolicitudAutorizacion) session.selectOne(TRAE_SOLICITUD_AUTORIZACION, etapaId);
	}

	@Override
	// Solamente para lecturas dentro de una transaccion (cuando aun no se han persistido los datos)
	public SolicitudAutorizacion traeSolicitudAutorizacionTx(Integer etapaId) {
		return (SolicitudAutorizacion) session.selectOne(TRAE_SOLICITUD_AUTORIZACION_TX, etapaId);
	}
	
	@Override
	public Map<String, Object> traePrestadorSolicitudAutorizacion(Integer etapaId) {
		return session.selectOne(TRAE_PRESTADOR_SOLICITUD_AUTORIZACION, etapaId);
	}

	@Override
	// Solamente para lecturas dentro de una transaccion (cuando aun no se han persistido los datos)
	public Map<String, Object> traePrestadorSolicitudAutorizacionTx(Integer etapaId) {
		return session.selectOne(TRAE_PRESTADOR_SOLICITUD_AUTORIZACION_TX, etapaId);
	}
	
	@Override
	public Map<String, Object> traeProfesionalesSolicitudAutorizacion(Integer etapaId) {
		return session.selectOne(TRAE_PROFESIONALES_SOLICITUD_AUTORIZACION, etapaId);
	}

	@Override
	public List<SolicitudAutorizacion> traerSolicitudesPendientes(Map<String, Object> map) {
		return session.selectList(TRAE_SOLICITUDES_PENDIENTES, map);
	}

	@Override
	public List<Map<String, Object>> traerEstudiosTramite(Map<String, Object> map) {
		return session.selectList(TRAER_ESTUDIOS_TRAMITE, map);
	}

}
