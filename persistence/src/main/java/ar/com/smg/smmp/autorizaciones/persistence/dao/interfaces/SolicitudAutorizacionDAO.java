package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import java.util.List;
import java.util.Map;

import ar.com.smg.smmp.model.autorizaciones.solicitud.SolicitudAutorizacion;

public interface SolicitudAutorizacionDAO extends DAO {

	String INSERTA_SOLICITUD_AUTORIZACION = "inserta_solicitud_autorizacion";
	String ACTUALIZA_SOLICITUD_AUTORIZACION = "actualiza_solicitud_autorizacion";
	String INSERTA_PRESTADOR_SOLICITUD_AUTORIZACION = "inserta_prestador_solicitud_autorizacion";
	String INSERTA_PROFESIONAL_SOLICITUD_AUTORIZACION = "inserta_profesional_solicitud_autorizacion";
	String TRAE_SOLICITUD_AUTORIZACION = "trae_solicitud_autorizacion";
	String TRAE_SOLICITUD_AUTORIZACION_TX = "trae_solicitud_autorizacion_tx";
	String TRAE_PRESTADOR_SOLICITUD_AUTORIZACION = "trae_prestador_solicitud_autorizacion";
	String TRAE_PRESTADOR_SOLICITUD_AUTORIZACION_TX = "trae_prestador_solicitud_autorizacion_tx";
	String TRAE_PROFESIONALES_SOLICITUD_AUTORIZACION = "trae_profesionales_solicitud_autorizacion";
	String TRAE_SOLICITUDES_PENDIENTES = "trae_solicitudes_pendientes";
	
	String TRAER_ESTUDIOS_TRAMITE = "traer_estudios_tramite";
	String PRUEBA_PARAMETROS_OUT = "prueba_parametros_out";
	
	Integer insertaSolicitudAutorizacion(Map<String, Object> map);
	void actualizaSolicitudAutorizacion(Map<String, Object> map);
	void insertaPrestadorSolicitudAutorizacion(Map<String, Object> map);
	void insertaProfesionalSolicitudAutorizacion(Map<String, Object> map);
	SolicitudAutorizacion traeSolicitudAutorizacion(Integer etapaId);
	Map<String, Object> traePrestadorSolicitudAutorizacion(Integer etapaId);
	Map<String, Object> traeProfesionalesSolicitudAutorizacion(Integer etapaId);
	List<SolicitudAutorizacion> traerSolicitudesPendientes(Map<String, Object> map);
	
	/**
	 * Este metodo es solamente para lecturas dentro de una transaccion
	 * 
	 * @param etapaId
	 * @return
	 */
	SolicitudAutorizacion traeSolicitudAutorizacionTx(Integer etapaId);
	
	/**
	 * Este metodo es solamente para lecturas dentro de una transaccion
	 * 
	 * @param etapaId
	 * @return
	 */
	Map<String, Object> traePrestadorSolicitudAutorizacionTx(Integer etapaId);
	
	List<Map<String, Object>> traerEstudiosTramite(Map<String, Object> map);

	
}
