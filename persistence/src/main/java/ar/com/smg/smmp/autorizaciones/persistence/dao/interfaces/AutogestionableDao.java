package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface AutogestionableDao extends DAO {

	String GET_AUTOGESTIONABLE = "trae_autogestionables";
	String GET_TRAMITE = "trae_tramite";
	String GET_DOCUMENTACION = "trae_documentacion";
	String GET_AUTOGESTIONABLES = "trae_autogestionables_todas";
	String GET_DIAGNOSTICOS = "trae_diagnosticos";
	String GET_COBERTURAS_MADRES = "trae_coberturas_madres";




	Map<String, Object> traeAutogestionable(Map<String, Object> body);
	Map<String, Object> traeTramite(HashMap<String, Object> body);
	List<Map<String, Object>> traeDocumentacionFaltante(Integer workflowId);
	List<Map<String, Object>> traeAutogestionables();
	List<Map<String, Object>> traeDiagnosticos(Map<String, Object> map);
	List<Map<String, Object>> traeCoberturas(Map<String, Object> map);



}
