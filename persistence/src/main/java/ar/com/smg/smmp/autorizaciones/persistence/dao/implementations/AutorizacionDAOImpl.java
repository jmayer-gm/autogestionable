package ar.com.smg.smmp.autorizaciones.persistence.dao.implementations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;

import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.AutorizacionDAO;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Internacion;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Prorroga;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Sala;
import ar.com.smg.smmp.model.autorizaciones.gestion.Autorizacion;
import ar.com.smg.smmp.model.autorizaciones.gestion.AutorizacionPrestador;
import ar.com.smg.smmp.model.autorizaciones.gestion.AutorizacionNormalizado;
import ar.com.smg.smmp.model.global.Documentacion;
import ar.com.smg.smmp.model.prestadores.farmacia.Farmacia;
import ar.com.smg.smmp.core.SMMPRuntimeException;

public class AutorizacionDAOImpl implements AutorizacionDAO {

	private SqlSession session;
	
	public AutorizacionDAOImpl() {
		session = null;
	}
	
	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void setSession(SqlSession session) {
		this.session = session;
	}

	@Override
	public Autorizacion traeAutorizacion(Integer etapaId) {
		return (Autorizacion) session.selectOne(TRAE_AUTORIZACION, etapaId);
	}
	
	@Override
	public List<Internacion> traeInternacionesAutorizacion(Map<String, Object> map) {
		return session.selectList(TRAE_INTERNACIONES_AUTORIZACION, map);
	}
	
	@Override
	public Prorroga validaProrrogaAutorizacion(Map<String, Object> map) {
		return session.selectOne(VALIDA_PRORROGA_AUTORIZACION, map);
	}

	@Override
	public Integer insertaProrrogaAutorizacion(Prorroga prorroga) {
		return session.selectOne(INSERTA_PRORROGA_AUTORIZACION, prorroga);
	}

	@Override
	public void insertaSalaAutorizacion(Map<String, Object> map) {
		session.selectOne(INSERTA_SALA_AUTORIZACION, map);
	}

	@Override
	public List<Map<String, Object>> traeAutorizacionesAsociadasConSala(Map<String, Object> map) {
		return session.selectList(TRAE_AUTORIZACIONES_ASOCIADAS_SALA, map);
	}

	@Override
	public List<Prorroga> traeAutorizacionesAsociadas(Map<String, Object> map) {
		return session.selectList(TRAE_AUTORIZACIONES_ASOCIADAS, map);
	}

	@Override
	public List<Sala> traeSalas(Map<String, Object> map) {
		return session.selectList(TRAE_SALAS, map);
	}
	
	@Override
	public List<Documentacion> traerDocumentacionSituacionTerapeutica(Map<String, Object> map) {
		return session.selectList(TRAE_DOCUMENTACION_SITUACION_TERAPEUTICA, map);
	}

	@Override
	public Map<String, Object> traerAutorizacionesPorPrestador(Map<String, Object> map) {
		try{
			Map<String, Object> mapListaCantTotal = new HashMap<String, Object>();
			List<AutorizacionPrestador> autorizacionPrestador = session.selectList(TRAE_AUTORIZACION_POR_PRESTADOR, map);
			mapListaCantTotal.put("listaAutorizacionesPrestador",autorizacionPrestador);
			mapListaCantTotal.put("cantTotalPag",map.get("cantTotalPag"));
	
			if(map.get("cantTotalPag") != null){
				System.out.println(map.get("cantTotalPag"));
			}
	
			return mapListaCantTotal;
		}catch(PersistenceException pe){
			wrapPersistenceException(pe);
		}
		return null;
	}

	@Override
	public List<Map<String, Object>> traerAutorizacionesPorAfiliadoEfector(Map<String, Object> map) {
		return session.selectList(TRAE_AUTORIZACION_POR_AFILIADO_EFECTOR, map);
	}
	
	@Override
	public Map<String, Object>  tieneAutorizacionVinculable(Map<String, Object> map) {
		
		Map<String, Object> maux = session.selectOne(TIENE_AUTORIZACION_VINCULABLE, map);
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("tieneAutorizacionVinculable",maux.get(""));
		return m;
	}	
	
	private void wrapPersistenceException(PersistenceException e){
		if(e.getCause() != null){
			throw new SMMPRuntimeException(e.getCause().getMessage(), e);
		}
		throw e;
	}

	@Override
	public AutorizacionNormalizado traerAutorizacionXSucursalYId(Integer sucursalId, Integer autorizacionId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ksucur", sucursalId);
		map.put("kauto", autorizacionId);
		
		return session.selectOne(TRAE_AUTORIZACION_POR_SUCUR_Y_ID, map);
	}
	
	@Override
	public List<Map<String, Object>> traerAutorizacionConEquipos() {
	
		return session.selectList(TRAE_AUTORIZACION_CON_EQUPIPOS);
	}

	@Override
	public List<Farmacia> traerFarmacias(Map<String, Object> map) {
		return session.selectList(TRAE_FARMACIAS, map);
	}
	
	@Override
	public List<Farmacia> traerFarmaciasByUbicacion(Map<String, Object> map) {
		return session.selectList(TRAE_FARMACIAS_BY_UBICACION, map);
	}

	@Override
	public List<Map<String, Object>> traerEquiposXAutorizacion(Integer sucursal,
			Integer autorizacionId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sucursal", sucursal);
		map.put("autorizacion", autorizacionId);
		
		return session.selectList(TRAE_EQUIPOS_X_AUTORIZACION, map);
	}
	
	@Override
	public List<Map<String, Object>> traerModulosXAutorizacion(Integer sucursal,
			Integer autorizacionId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sucursal", sucursal);
		map.put("autorizacion", autorizacionId);
		
		return session.selectList(TRAE_MODULOS_X_AUTORIZACION, map);
	}

	@Override
	public List<Map<String, Object>> traerAutorizacionesOdontoAfiliado(Map<String, Object> map) {
		return session.selectList(TRAE_AUTORIZACIONES_ODONTO_AFILIADO, map);
	}

	@Override
	public Map<String, Object> traerDatosCabAutorizacionesOdo(Map<String, Object> map) {
		return session.selectOne(TRAE_DATOS_CAB_AUTORIZACION_ODO, map);
	}

	@Override
	public List<Map<String, Object>> traerPrestacionesAutorizacionesOdo(Map<String, Object> map) {
		return session.selectList(TRAE_PRESTACIONES_AUTORIZACIONES_ODO, map);
	}
	
	@Override
	public List<Map<String, Object>> traerAutorizacionesOdontoPrestador(Map<String, Object> map) {
		return session.selectList(TRAE_AUTORIZACIONES_ODONTO_PRESTADOR, map);
	}

	@Override
	public List<Map<String, Object>> validarAutorizacionesSolapadas(HashMap<String, Object> params) {
		return session.selectList(SOLAPAMIENTO_AUTO, params);
	}

	@Override
	public List<Map<String, Object>> validarConvenio(HashMap<String, Object> params) {
		session.selectOne(VALIDAR_CONVENIO, params);
		return null;
	}

	@Override
	public List<Map<String, Object>> validarConvenioInter(HashMap<String, Object> params) {
		session.selectOne(VALIDAR_CONVENIO_INTER, params);
		return null;
	}

	@Override
	public List<Map<String, Object>> validarConvenioCiru(HashMap<String, Object> params) {
		session.selectOne(VALIDAR_CONVENIO_CIRU, params);
		return null;
	}
	
	@Override
	public void validarCobertura(HashMap<String, Object> params) {
		try {
			session.update("dropTableModulos");
		}
		catch (Throwable e) {
			// TODO: Se captura por si falla el drop, porque no existe la tabla.
		}
		session.update("createTableModulosIfNotExist");
		session.selectList(VALIDAR_COBERTURA, params);
		
		List<HashMap<String, Object>> modulos = session.selectList(READ_TEMP_MODULOS);
		params.put("modulos", modulos);
		
		session.update("dropTableModulos");
	}
	
	@Override
	public List<Map<String, Object>> plnsConfigModulos(Map<String, Object> map) {
		try {
			session.update("dropTableModulos");
		}
		catch (Throwable e) {
			// TODO: Se captura por si falla el drop, porque no existe la tabla.
		}
		session.update("createTableModulosIfNotExist");
		
		List<Map<String, Object>> result = session.selectList(PLNS_CONFIG_MODULOS, map);
		
		session.update("dropTableModulos");
		
		return result;
	}
	
	@Override
	public List<Map<String, Object>> validarGeneroYEdad(HashMap<String, Object> params) {
		session.selectList(VALIDAR_GENERO_Y_EDAD, params);
		return null;
	}

	@Override
	public void validarCarencia(HashMap<String, Object> params) {
		session.selectList(VALIDAR_CARENCIA, params);
	}

	@Override
	public void traeCopagoPrestacion(Map<String, Object> params) {
		session.update(TRAE_COPAGO_PRESTACION, params);
	}
	
	@Override
	public List<Map<String, Object>> traerHistorialValorLey(Map<String, Object> map) {
		return session.selectList(TRAE_HISTORIAL_VALOR_LEY, map);
	}
	
	@Override
	public List<Map<String,Object>> obtenerLogsAutorizacionXSucursalYId(Integer sucursalId, Integer autorizacionId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sucursal", sucursalId);
		map.put("autorizacion", autorizacionId);
		
		return session.selectList(OBTENER_LOGS_AUDITORIA_AUTORIZACION, map);
	}

	@Override
	public Map<String, Object> recoverAutoByWorkflowId(Integer workflowId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("workflowId", workflowId);
		
		return session.selectOne(TRAE_AUTORIZACION_BY_WORKFLOW_ID, map);
	}

	@Override
	public List<Map<String, Object>> traerClasificaciones(Map<String, Object> map) {
		return session.selectList(TRAE_CLASIFICACIONES, map);
	}

	@Override
	public List<Map<String, Object>> traePrestacionesAutorizacion(Map<String, Object> map) {
		return session.selectList(TRAE_PRESTACIONES_AUTORIZACION, map);
	}
	
	@Override
	public List<Map<String, Object>> traerDescripcionRecha(Map<String, Object> map) {
		return session.selectList(TRAE_DESCRIPCION_RECHA, map);
	}
	
	@Override
	public void auditarAutorizacion(Map<String, Object> map){		
		try{
			session.selectOne(INSERTA_AUDITORIA, map);
		}
		catch(PersistenceException pe){
			wrapPersistenceException(pe);
		}
	}

	@Override
	public void expedientesAutorizacion(Map<String, Object> map) {
		session.selectList(EXPEDIENTES_AUTORIZACION, map);
	}

	@Override
	public Map<String, Object> validaTopesPrestacion(Map<String, Object> map) {
		try {
			session.update("dropTableModulos");
		}
		catch (Throwable e) {
			// TODO: Se captura por si falla el drop, porque no existe la tabla.
		}
		session.update("createTableModulosIfNotExist");
		
		Map<String, Object> result = session.selectOne(VALIDA_TOPES_PRESTACION, map);
		
		session.update("dropTableModulos");
		
		return result;
	}

	@Override
	public Map<String, Object> obtenerProceDtoProducto(HashMap<String, Object> params) {
		return session.selectOne(OBTENER_PORCE_DTO_PRODUCTO, params);
	}

	@Override
	public Map<String, Object> validarProducto(HashMap<String, Object> params) {
		return session.selectOne(VALIDAR_PRODUCTO, params);
	}

	@Override
	public Map<String, Object> mandatariaByRudiId(Integer rudiId) {
		return session.selectOne(MANDATARIA_BY_RUDI_ID, rudiId);
		
	}
	
	@Override
	public void registraEnvioAFarmalink(Map<String, Object> params) {
		session.update(REGISTRA_ENVIO_A_FARMALINK, params);
	}

	@Override
	public List<Map<String, Object>> traerRazonesOdo() {
		return session.selectList(TRAE_RAZONES_ODO);
	}

	@Override
	public List<Map<String, Object>> traerMotivosOdo(Map<String, Object> params) {
		return session.selectList(TRAE_MOTIVOS_ODO, params);
	}
	
	@Override
	public Map<String, Object> obtenerConversionProductoSap(HashMap<String, Object> params) {
		String result = session.selectOne(OBTENER_CONVERSION_SAP, params);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("codigoProducto", result);
		return map;
	}
}
