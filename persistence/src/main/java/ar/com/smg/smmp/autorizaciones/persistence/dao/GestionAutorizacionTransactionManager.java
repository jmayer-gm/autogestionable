package ar.com.smg.smmp.autorizaciones.persistence.dao;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.AuditoriaDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.AutorizacionDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.ControlEntregaDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.DerivacionDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.ExcepcionDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.implementations.SolicitudAutorizacionDAOImpl;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.AuditoriaDAO;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.AutorizacionDAO;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.ControlEntregaDAO;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.DerivacionDAO;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.ExcepcionDAO;
import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.SolicitudAutorizacionDAO;
import ar.com.smg.smmp.autorizaciones.persistence.dao.support.ResultMapSupport;
import ar.com.smg.smmp.core.SMMPRuntimeException;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Internacion;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Prorroga;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Sala;
import ar.com.smg.smmp.model.autorizaciones.gestion.Auditoria;
import ar.com.smg.smmp.model.autorizaciones.gestion.Autorizacion;
import ar.com.smg.smmp.model.autorizaciones.gestion.ControlEntrega;
import ar.com.smg.smmp.model.autorizaciones.gestion.Derivacion;
import ar.com.smg.smmp.model.autorizaciones.gestion.Excepcion;
import ar.com.smg.smmp.model.autorizaciones.solicitud.SolicitudAutorizacion;
import ar.com.smg.smmp.model.global.Documentacion;
import ar.com.smg.smmp.model.prestadores.farmacia.Farmacia;

public class GestionAutorizacionTransactionManager {

	private static final Logger LOGGER = Logger.getLogger(GestionAutorizacionTransactionManager.class);
	
	private static final String INGRESAR_SOLICITUD_ERROR_MESSAGE = "Ha ocurrido un error al ingresar la solicitud";
	private static final Object ACTUALIZAR_SOLICITUD_ERROR_MESSAGE = "Ha ocurrido un error al actualizar la solicitud";
	private static final String INGRESAR_PRESTADOR_SOLICITUD_ERROR_MESSAGE = "Ha ocurrido un error al ingresar el prestador de la solicitud";
	private static final String INGRESAR_PROFESIONAL_SOLICITUD_ERROR_MESSAGE = "Ha ocurrido un error al ingresar el profesional la solicitud";
	private static final String INGRESAR_EXCEPCION_ERROR_MESSAGE = "Ha ocurrido un error al ingresar la excepcion de la precarga";
	private static final String INGRESAR_DERIVACION_ERROR_MESSAGE = "Ha ocurrido un error al ingresar la derivacion medica";
	private static final String INGRESAR_AUDITORIA_ERROR_MESSAGE = "Ha ocurrido un error al ingresar la auditoria medica";
	private static final String INGRESAR_CONTROLAR_ENTREGA_ERROR_MESSAGE = "Ha ocurrido un error al ingresar los datos del control de la entrega";
	private static final String INGRESAR_PRORROGA_AUTORIZACION_ERROR_MESSAGE = "Ha ocurrido un error al ingresar la prorroga de la autorizacion {0}";
	private static final String INGRESAR_SALA_AUTORIZACION_ERROR_MESSAGE = "Ha ocurrido un error al ingresar la sala de la autorizacion";
	private static final String APROBAR_EXCEPCION_ERROR_MESSAGE = "Ha ocurrido un error al aprobar la excepcion";
	private static final String VALIDAR_PRORROGA_AUTORIZACION_ERROR_MESSAGE = "Ha ocurrido un error al validar la prorroga";
	
	private static final String TRAER_EXCEPCION_ERROR_MESSAGE = "Ha ocurrido un error al traer la excepcion de la autorizacion con etapa id {0}";
	private static final String TRAER_AUDITORIA_ERROR_MESSAGE = "Ha ocurrido un error al traer la auditoria medica con etapa id {0}";
	private static final String TRAER_DERIVACION_ERROR_MESSAGE = "Ha ocurrido un error al traer la derivacion medica con etapa id {0}";
	private static final String TRAER_CONTROL_ENTREGA_ERROR_MESSAGE = "Ha ocurrido un error al traer los datos del control de la entrega con etapa id {0}";
	private static final String TRAER_AUTORIZACION_ERROR_MESSAGE = "Ha ocurrido un error al traer la autorizacion con etapa id {0}";
	private static final String TRAER_INTERNACIONES_AUTORIZACION_ERROR_MESSAGE = "Ha ocurrido un error al traer las internaciones de la autorizacion";
	private static final String TRAER_AUTORIZACIONES_ASOCIADAS_ERROR_MESSAGE = "Ha ocurrido un error al traer las autorizaciones asociadas";
	private static final String TRAER_SOLICITUD_ERROR_MESSAGE = "Ha ocurrido un error al traer la solicitud de autorizacion con etapa id {0}";
	private static final String TRAER_PRESTADOR_SOLICITUD_ERROR_MESSAGE = "Ha ocurrido un error al traer el prestador de la solicitud de autorizacion con etapa id {0}";
	private static final String TRAER_PROFESIONALES_SOLICITUD_ERROR_MESSAGE = "Ha ocurrido un error al traer los profesionales de la solicitud de autorizacion con etapa id {0}";
	private static final String TRAER_SOLICITUDES_PENDIENTES_ERROR_MESSAGE = "Ha ocurrido un error al traer las solicitudes pendientes";
	private static final String TRAER_SALAS_ERROR_MESSAGE = "Ha ocurrido un error al traer las salas";
	
	private SolicitudAutorizacionDAO solicitudAutorizacionDAO;
	private ExcepcionDAO excepcionDAO;
	private DerivacionDAO derivacionDAO;
	private AuditoriaDAO auditoriaDAO;
	private AutorizacionDAO autorizacionDAO;
	private ControlEntregaDAO controlEntregaDAO;
	

	private SolicitudAutorizacionDAO getSolicitudAutorizacionDAO() {
		if (solicitudAutorizacionDAO == null) {
			solicitudAutorizacionDAO = new SolicitudAutorizacionDAOImpl(); 
		}
		return solicitudAutorizacionDAO;
	}
	
	// ************************** SOLICITUD **************************
	public Integer ingresarSolicitudAutorizacion(Map<String, Object> map) throws IOException {
		
		SqlSession session = null;
		
		Integer solicitudId = null;
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			this.getSolicitudAutorizacionDAO().setSession(session);
			solicitudId = solicitudAutorizacionDAO.insertaSolicitudAutorizacion(map);
		} catch (IOException e) {
			LOGGER.error(INGRESAR_SOLICITUD_ERROR_MESSAGE, e);
			throw new IOException(INGRESAR_SOLICITUD_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}

		return solicitudId;
		
	}
	
	public void actualizarSolicitudAutorizacion(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			this.getSolicitudAutorizacionDAO().setSession(session);
			solicitudAutorizacionDAO.actualizaSolicitudAutorizacion(map);
		} catch (IOException e) {
			LOGGER.error(ACTUALIZAR_SOLICITUD_ERROR_MESSAGE, e);
			throw new IOException(ACTUALIZAR_SOLICITUD_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	public void ingresarPrestadorSolicitudAutorizacion(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			this.getSolicitudAutorizacionDAO().setSession(session);
			this.getSolicitudAutorizacionDAO().insertaPrestadorSolicitudAutorizacion(map);
		} catch (IOException e) {
			LOGGER.error(INGRESAR_PRESTADOR_SOLICITUD_ERROR_MESSAGE, e);
			throw new IOException(INGRESAR_PRESTADOR_SOLICITUD_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	public void ingresarProfesionalSolicitud(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			this.getSolicitudAutorizacionDAO().setSession(session);
			this.getSolicitudAutorizacionDAO().insertaProfesionalSolicitudAutorizacion(map);
		} catch (IOException e) {
			LOGGER.error(INGRESAR_PROFESIONAL_SOLICITUD_ERROR_MESSAGE, e);
			throw new IOException(INGRESAR_PROFESIONAL_SOLICITUD_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	public SolicitudAutorizacion traerSolicitudAutorizacion(Integer etapaId) throws IOException {
		SqlSession session = null;
		solicitudAutorizacionDAO = new SolicitudAutorizacionDAOImpl();
                SolicitudAutorizacion solicitud;
		
		try {
			session = MyBatisSessionFactory.getInstance().getCrmSmgNoTransactionalSession();
			solicitudAutorizacionDAO.setSession(session);
			solicitud = solicitudAutorizacionDAO.traeSolicitudAutorizacion(etapaId);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TRAER_SOLICITUD_ERROR_MESSAGE, etapaId), e);
			throw new IOException(MessageFormat.format(TRAER_SOLICITUD_ERROR_MESSAGE, etapaId), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return solicitud;
	}
	
	// Para consultar datos de la solicitud que no se han persistido dureante la transaccion
	public SolicitudAutorizacion traerSolicitudAutorizacionTx(Integer etapaId) throws IOException {
		SqlSession session = null;
		solicitudAutorizacionDAO = new SolicitudAutorizacionDAOImpl();
                SolicitudAutorizacion solicitud;
		
		try {
//			session = MyBatisSessionFactory.getInstance().getCrmSmgXASession();
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			solicitudAutorizacionDAO.setSession(session);
			solicitud = solicitudAutorizacionDAO.traeSolicitudAutorizacionTx(etapaId);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TRAER_SOLICITUD_ERROR_MESSAGE, etapaId), e);
			throw new IOException(MessageFormat.format(TRAER_SOLICITUD_ERROR_MESSAGE, etapaId), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return solicitud;
	}
	
	// ************************** PRESTADOR y PROFESIONALES DE LA SOLICITUD **************************
	public Map<String, Object> traerPrestadorSolicitudAutorizacion(Integer etapaId) throws IOException {
		SqlSession session = null;
		Map<String, Object> resultMap = null;
		solicitudAutorizacionDAO = new SolicitudAutorizacionDAOImpl();
		Map<String, Object> lista;
                        
		try {
			session = MyBatisSessionFactory.getInstance().getCrmSmgNoTransactionalSession();
			solicitudAutorizacionDAO.setSession(session);
			resultMap = solicitudAutorizacionDAO.traePrestadorSolicitudAutorizacion(etapaId);
			lista = ResultMapSupport.prestadorSolicitudAdapterMap(resultMap);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TRAER_PRESTADOR_SOLICITUD_ERROR_MESSAGE, etapaId), e);
			throw new IOException(MessageFormat.format(TRAER_PRESTADOR_SOLICITUD_ERROR_MESSAGE, etapaId), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
	
	// Para consultar datos del prestador de la solicitud que no se han persistido dureante la transaccion
	public Map<String, Object> traerPrestadorSolicitudAutorizacionTx(Integer etapaId) throws IOException {
		SqlSession session = null;
		Map<String, Object> resultMap = null;
		solicitudAutorizacionDAO = new SolicitudAutorizacionDAOImpl();
                Map<String, Object> lista;
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			solicitudAutorizacionDAO.setSession(session);
			resultMap = solicitudAutorizacionDAO.traePrestadorSolicitudAutorizacionTx(etapaId);
			lista = ResultMapSupport.prestadorSolicitudAdapterMap(resultMap);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TRAER_PRESTADOR_SOLICITUD_ERROR_MESSAGE, etapaId), e);
			throw new IOException(MessageFormat.format(TRAER_PRESTADOR_SOLICITUD_ERROR_MESSAGE, etapaId), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
	
	public Map<String, Object> traerProfesionalesSolicitudAutorizacion(Integer etapaId) throws IOException {
		SqlSession session = null;
		Map<String, Object> resultMap = null;
		solicitudAutorizacionDAO = new SolicitudAutorizacionDAOImpl();
		Map<String, Object> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getCrmSmgNoTransactionalSession();
			solicitudAutorizacionDAO.setSession(session);
			resultMap = solicitudAutorizacionDAO.traeProfesionalesSolicitudAutorizacion(etapaId);
			lista = ResultMapSupport.profesionalesSolicitudAdapterMap(resultMap);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TRAER_PROFESIONALES_SOLICITUD_ERROR_MESSAGE, etapaId), e);
			throw new IOException(MessageFormat.format(TRAER_PROFESIONALES_SOLICITUD_ERROR_MESSAGE, etapaId), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
	
	public List<SolicitudAutorizacion> traerSolicitudesPendientes(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		solicitudAutorizacionDAO = new SolicitudAutorizacionDAOImpl();
		List<SolicitudAutorizacion> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getCrmSmgNoTransactionalSession();
			solicitudAutorizacionDAO.setSession(session);
			lista = solicitudAutorizacionDAO.traerSolicitudesPendientes(map);
		} catch (IOException e) {
			LOGGER.error(TRAER_SOLICITUDES_PENDIENTES_ERROR_MESSAGE, e);
			throw new IOException(TRAER_SOLICITUDES_PENDIENTES_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
	
	// ************************** EXCEPCION **************************
	public void ingresarExcepcionAutorizacion(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		excepcionDAO = new ExcepcionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			excepcionDAO.setSession(session);
			excepcionDAO.insertaExcepcionAutorizacion(map);
		} catch (IOException e) {
			LOGGER.error(INGRESAR_EXCEPCION_ERROR_MESSAGE, e);
			throw new IOException(INGRESAR_EXCEPCION_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	public void aprobarExcepcionAutorizacion(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		excepcionDAO = new ExcepcionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			excepcionDAO.setSession(session);
			excepcionDAO.apruebaExcepcionAutorizacion(map);
		} catch (IOException e) {
			LOGGER.error(APROBAR_EXCEPCION_ERROR_MESSAGE + e.getMessage(), e);
			throw new IOException(APROBAR_EXCEPCION_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	public Excepcion traerExcepcionAutorizacion(Integer etapaId) throws IOException {
		SqlSession session = null;
		excepcionDAO = new ExcepcionDAOImpl();
                Excepcion excepcionAuto;
		
		try {
			session = MyBatisSessionFactory.getInstance().getCrmSmgNoTransactionalSession();
			excepcionDAO.setSession(session);
			excepcionAuto = excepcionDAO.traeExcepcionAutorizacion(etapaId);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TRAER_EXCEPCION_ERROR_MESSAGE, etapaId), e);
			throw new IOException(MessageFormat.format(TRAER_EXCEPCION_ERROR_MESSAGE, etapaId), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return excepcionAuto;
	}
	
	// ************************** DERIVACION **************************
	public void ingresarDerivacionMedica(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		derivacionDAO = new DerivacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			derivacionDAO.setSession(session);
			derivacionDAO.insertaDerivacionMedica(map);
		} catch (IOException e) {
			LOGGER.error(INGRESAR_DERIVACION_ERROR_MESSAGE, e);
			throw new IOException(INGRESAR_DERIVACION_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	public Derivacion traerDerivacionMedica(Integer etapaId) throws IOException {
		SqlSession session = null;
		derivacionDAO = new DerivacionDAOImpl();
                Derivacion derivacion;
		
		try {
			session = MyBatisSessionFactory.getInstance().getCrmSmgNoTransactionalSession();
			derivacionDAO.setSession(session);
			derivacion= derivacionDAO.traeDerivacionMedica(etapaId);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TRAER_DERIVACION_ERROR_MESSAGE, etapaId), e);
			throw new IOException(MessageFormat.format(TRAER_DERIVACION_ERROR_MESSAGE, etapaId), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return derivacion;
	}
	
	// ************************** AUDITORIA **************************
	public void ingresarAuditoriaMedica(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		auditoriaDAO = new AuditoriaDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			auditoriaDAO.setSession(session);
			auditoriaDAO.insertaAuditoriaMedica(map);
		} catch (IOException e) {
			LOGGER.error(INGRESAR_AUDITORIA_ERROR_MESSAGE, e);
			throw new IOException(INGRESAR_AUDITORIA_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	public Auditoria traerAuditoriaMedica(Integer etapaId) throws IOException {
		SqlSession session = null;
		auditoriaDAO = new AuditoriaDAOImpl();
                Auditoria auditoria;
		try {
			session = MyBatisSessionFactory.getInstance().getCrmSmgNoTransactionalSession();
			auditoriaDAO.setSession(session);
			auditoria = auditoriaDAO.traeAuditoriaMedica(etapaId);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TRAER_AUDITORIA_ERROR_MESSAGE, etapaId), e);
			throw new IOException(MessageFormat.format(TRAER_AUDITORIA_ERROR_MESSAGE, etapaId), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return auditoria;
	}
	
	// ************************** AUTORIZACION **************************
	public Autorizacion traerAutorizacion(Integer etapaId) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		Autorizacion auto;
                
		try {
			session = MyBatisSessionFactory.getInstance().getCrmSmgNoTransactionalSession();
			autorizacionDAO.setSession(session);
			auto = autorizacionDAO.traeAutorizacion(etapaId);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TRAER_AUTORIZACION_ERROR_MESSAGE, etapaId), e);
			throw new IOException(MessageFormat.format(TRAER_AUTORIZACION_ERROR_MESSAGE, etapaId), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return auto;
	}
	
	public List<Internacion> traerInternacionesAutorizacion(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
                List<Internacion> lista;
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traeInternacionesAutorizacion(map);
		} catch (IOException e) {
			LOGGER.error(TRAER_INTERNACIONES_AUTORIZACION_ERROR_MESSAGE, e);
			throw new IOException(TRAER_INTERNACIONES_AUTORIZACION_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
	
	public List<Map<String, Object>> traerAutorizacionesAsociadasConSala(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista;
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traeAutorizacionesAsociadasConSala(map);
		} catch (IOException e) {
			LOGGER.error(TRAER_AUTORIZACIONES_ASOCIADAS_ERROR_MESSAGE, e);
			throw new IOException(TRAER_AUTORIZACIONES_ASOCIADAS_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
	
	public List<Prorroga> traerAutorizacionesAsociadas(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Prorroga> lista;
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traeAutorizacionesAsociadas(map);
		} catch (IOException e) {
			LOGGER.error(TRAER_AUTORIZACIONES_ASOCIADAS_ERROR_MESSAGE, e);
			throw new IOException(TRAER_AUTORIZACIONES_ASOCIADAS_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
	
	public Prorroga validarProrrogaAutorizacion(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		Prorroga prorroga;
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			prorroga = autorizacionDAO.validaProrrogaAutorizacion(map);
		} 
		catch (IOException e) {
			LOGGER.error(VALIDAR_PRORROGA_AUTORIZACION_ERROR_MESSAGE, e);
			throw new IOException(VALIDAR_PRORROGA_AUTORIZACION_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return prorroga;
	}
	
	public void ingresarProrrogaAutorizacion(Prorroga prorroga) throws IOException {
		SqlSession session = null;
		Integer numeroProrroga = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			autorizacionDAO.setSession(session);
			numeroProrroga = autorizacionDAO.insertaProrrogaAutorizacion(prorroga);
			prorroga.setNumeroAutorizacion(numeroProrroga);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(INGRESAR_PRORROGA_AUTORIZACION_ERROR_MESSAGE, 
					prorroga.getNumero()), e);
			throw new IOException(MessageFormat.format(INGRESAR_PRORROGA_AUTORIZACION_ERROR_MESSAGE, 
					prorroga.getNumero()), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	public void ingresarSalaAutorizacion(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			autorizacionDAO.setSession(session);
			autorizacionDAO.insertaSalaAutorizacion(map);
		} catch (IOException e) {
			LOGGER.error(INGRESAR_SALA_AUTORIZACION_ERROR_MESSAGE, e);
			throw new IOException(INGRESAR_SALA_AUTORIZACION_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	// ************************** CONTROL ENTREGA **************************
	public void ingresarControlEntrega(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		controlEntregaDAO = new ControlEntregaDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciXASession();
			controlEntregaDAO.setSession(session);
			controlEntregaDAO.insertaControlEntrega(map);
		} catch (IOException e) {
			LOGGER.error(INGRESAR_CONTROLAR_ENTREGA_ERROR_MESSAGE, e);
			throw new IOException(INGRESAR_CONTROLAR_ENTREGA_ERROR_MESSAGE + e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	public ControlEntrega traerControlEntrega(Integer etapaId) throws IOException {
		SqlSession session = null;
		controlEntregaDAO = new ControlEntregaDAOImpl();
		ControlEntrega control;
		try {
			session = MyBatisSessionFactory.getInstance().getCrmSmgNoTransactionalSession();
			controlEntregaDAO.setSession(session);
			control = controlEntregaDAO.traeControlEntrega(etapaId);
		} catch (IOException e) {
			LOGGER.error(MessageFormat.format(TRAER_CONTROL_ENTREGA_ERROR_MESSAGE, etapaId), e);
			throw new IOException(MessageFormat.format(TRAER_CONTROL_ENTREGA_ERROR_MESSAGE, etapaId), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return control;
	}
	
	public List<Sala> traerSalas(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Sala> lista;
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traeSalas(map);
		} 
		catch (IOException e) {
			LOGGER.error(TRAER_SALAS_ERROR_MESSAGE, e);
			throw new IOException(TRAER_SALAS_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}

	public List<Internacion> traerInternaciones(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Internacion> lista;
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traeInternacionesAutorizacion(map);
		} 
		catch (IOException e) {
			LOGGER.error(TRAER_AUTORIZACIONES_ASOCIADAS_ERROR_MESSAGE, e);
			throw new IOException(TRAER_AUTORIZACIONES_ASOCIADAS_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
	
	public List<Documentacion> traerDocumentacionSituacionTerapeutica(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Documentacion> lista;
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerDocumentacionSituacionTerapeutica(map);
		} 
		catch (IOException e) {
			LOGGER.error(TRAER_SALAS_ERROR_MESSAGE, e);
			throw new IOException(TRAER_SALAS_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
	
	
	public Map<String, Object> traerAutorizacionesPorPrestador(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		Map<String, Object> lista;
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerAutorizacionesPorPrestador(map);
		} 
		catch (IOException e) {
			LOGGER.error(TRAER_SALAS_ERROR_MESSAGE, e);
			throw new SMMPRuntimeException(TRAER_SALAS_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
	
	public List<Map<String, Object>> traerAutorizacionesPorAfiliadoEfector(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerAutorizacionesPorAfiliadoEfector(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}	

	public Map<String, Object>  tieneAutorizacionVinculable(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		Map<String, Object> lista; 
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.tieneAutorizacionVinculable(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}	
	

	public List<Farmacia> traerFarmacias(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
                List<Farmacia> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerFarmacias(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}

	public List<Farmacia> traerFarmaciasByUbicacion(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Farmacia> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerFarmaciasByUbicacion(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}

	public List<Map<String, Object>> traerAutorizacionesOdontoAfiliado(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerAutorizacionesOdontoAfiliado(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}

	public Map<String, Object> traerDatosCabAutorizacionesOdo(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		Map<String, Object> lista;
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerDatosCabAutorizacionesOdo(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}

	public List<Map<String, Object>> traerPrestacionesAutorizacionesOdo(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista; 
                        
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista= autorizacionDAO.traerPrestacionesAutorizacionesOdo(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}

	public List<Map<String, Object>> traerAutorizacionesOdontoPrestador(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerAutorizacionesOdontoPrestador(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}

	public List<Map<String, Object>> validarAutorizacionesSolapadas(HashMap<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.validarAutorizacionesSolapadas(params);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}
	
	public List<Map<String, Object>> validarConvenio(HashMap<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autorizacionDAO.setSession(session);
			autorizacionDAO.validarConvenio(params);
		}
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	public List<Map<String, Object>> validarConvenioInter(HashMap<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autorizacionDAO.setSession(session);
			autorizacionDAO.validarConvenioInter(params);
		}
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	public List<Map<String, Object>> validarConvenioCiru(HashMap<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autorizacionDAO.setSession(session);
			autorizacionDAO.validarConvenioCiru(params);
		}
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	public List<Map<String, Object>> traerHistorialValorLey(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerHistorialValorLey(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}

	public List<Map<String, Object>> traerEstudiosTramite(Map<String, Object> map) {
		SqlSession session = null;
		solicitudAutorizacionDAO = new SolicitudAutorizacionDAOImpl();
		List<Map<String, Object>> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			solicitudAutorizacionDAO.setSession(session);
			lista =  solicitudAutorizacionDAO.traerEstudiosTramite(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista; 
	}

	public List<Map<String, Object>> plnsConfigModulos(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista;
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.plnsConfigModulos(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
		return lista;
	}

	public List<Map<String, Object>> validarCobertura(HashMap<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();

		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autorizacionDAO.setSession(session);
			autorizacionDAO.validarCobertura(params);
		}
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if (session != null) {
				session.close();
			}
		}
		return null;
	}
	
	public List<Map<String, Object>> validarGeneroYEdad(HashMap<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autorizacionDAO.setSession(session);
			autorizacionDAO.validarGeneroYEdad(params);
		}
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if (session != null) {
				session.close();
			}
		}
		return null;
	}

	public void validarCarencia(HashMap<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autorizacionDAO.setSession(session);
			autorizacionDAO.validarCarencia(params);
		}
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void traeCopagoPrestacion(Map<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autorizacionDAO.setSession(session);
			autorizacionDAO.traeCopagoPrestacion(params);
		}
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public List<Map<String, Object>> traerClasificaciones(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerClasificaciones(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                return lista;
	}

	public List<Map<String, Object>> traePrestacionesAutorizacion(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista; 
                
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traePrestacionesAutorizacion(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                
                return lista;
	}
	
	public List<Map<String, Object>> traerDescripcionRecha(Map<String, Object> map) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
                List<Map<String, Object>> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerDescripcionRecha(map);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
                
                return lista;
	}
	
	public void auditarAutorizacion(Map<String, Object> map) throws IOException {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autorizacionDAO.setSession(session);
			autorizacionDAO.auditarAutorizacion(map);
		}
		catch (IOException e) {
			LOGGER.error(INGRESAR_SALA_AUTORIZACION_ERROR_MESSAGE, e);
			throw new IOException(INGRESAR_SALA_AUTORIZACION_ERROR_MESSAGE, e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}

	public Map<String, Object> obtenerProceDtoProducto(HashMap<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			return autorizacionDAO.obtenerProceDtoProducto(params);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}

	public Map<String, Object> validarProducto(HashMap<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			Map<String, Object> response = null;
			params.put("idCoberMccMensaje", null);
			params.put("descuentoTipo", null);
			params.put("descuentoPorce", null);
			
			autorizacionDAO.validarProducto(params);
			
			response = params;
			
			return response;
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	public void registraEnvioAFarmalink(Map<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciAutoTrueSession();
			autorizacionDAO.setSession(session);
			autorizacionDAO.registraEnvioAFarmalink(params);
		}
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public List<Map<String, Object>> traerRazonesOdo() {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerRazonesOdo();
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
        return lista;
	}

	public List<Map<String, Object>> traerMotivosOdo(Map<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		List<Map<String, Object>> lista; 
                
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);
			lista = autorizacionDAO.traerMotivosOdo(params);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
        return lista;
	}
	
	public Map<String, Object> obtenerConversionProductoSap(HashMap<String, Object> params) {
		SqlSession session = null;
		autorizacionDAO = new AutorizacionDAOImpl();
		Map<String, Object> result;
		try {
			session = MyBatisSessionFactory.getInstance().getPrestaciNoTransactionalSession();
			autorizacionDAO.setSession(session);			
			result =  autorizacionDAO.obtenerConversionProductoSap(params);
		} 
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new SMMPRuntimeException(e.getMessage(), e);
		}
		finally {
			if(session != null) {
				session.close();
			}
		}
		
		return result;
	}

}
