package ar.com.smg.smmp.autorizaciones.persistence.dao.implementations;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.TipoDAO;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoExcepcion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoFinalizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoRechazo;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAgrupacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAutorizacion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoGestion;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoPaciente;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoProvisionObraSocial;
import ar.com.smg.smmp.model.autorizaciones.tipo.TipoResolucion;

public class TipoDAOImpl implements TipoDAO {

	private SqlSession session;

	public TipoDAOImpl() {
		session = null;
	}

	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void setSession(SqlSession session) {
		this.session = session;
	}

	@Override
	public List<TipoAutorizacion> traeTiposAutorizacion() {
		return session.selectList(TRAE_TIPOS_AUTORIZACION);
	}

	@Override
	public List<TipoGestion> traeTiposGestion() {
		return session.selectList(TRAE_TIPOS_GESTION);
	}

	@Override
	public List<TipoPaciente> traeTiposPaciente() {
		return session.selectList(TRAE_TIPOS_PACIENTE);
	}

	@Override
	public List<TipoProvisionObraSocial> traeTiposProvisionObraSocial() {
		return session.selectList(TRAE_TIPOS_PROVISION_OBRA_SOCIAL);
	}

	@Override
	public List<TipoAgrupacion> traeTiposAgrupacion() {
		return session.selectList(TRAE_TIPOS_AGRUPACION);
	}

	@Override
	public List<TipoResolucion> traeTiposResolucion() {
		return session.selectList(TRAE_TIPOS_RESOLUCION);
	}
	
	@Override
	public List<TipoResolucion> traeTiposResolucionST() {
		return session.selectList(TRAE_TIPOS_RESOLUCION_ST);
	}

	@Override
	public List<MotivoExcepcion> traeMotivosExcepcion() {
		return session.selectList(TRAE_MOTIVOS_EXCEPCION);
	}

	@Override
	public List<MotivoRechazo> traeMotivosRechazo() {
		return session.selectList(TRAE_MOTIVOS_RECHAZO);
	}

	@Override
	public List<MotivoFinalizacion> traeMotivosFinalizacion(String tarea) {
		return session.selectList(TRAE_MOTIVOS_FINALIZACION, tarea);
	}
	
	@Override
	public List<Map<String, Object>> traeTiposPracticaOdo() {
		return session.selectList(TRAE_TIPOS_PRACTICA_ODO);
	}
	
	@Override
	public List<Map<String, Object>> traeMotivosFinalizacionOdo(Integer codigo, Integer grupo) {
		
		Map<String, Object> parms = new HashMap<String, Object>();
		parms.put("id_motivo", codigo);
		parms.put("id_grupo", grupo);
		
		return session.selectList(TRAE_MOTIVOS_FINALIZACION_ODO, parms);
	}
	
	@Override
	public List<Map<String, Object>> traeResolucionesOdo(Integer codigo) {
		
		Map<String, Object> parms = new HashMap<String, Object>();
		parms.put("id_resolucion", codigo);
		
		return session.selectList(TRAE_RESOLUCIONES_ODO, parms);
	}

	@Override
	public List<Map<String, Object>> traeMotivosNoReprogramacionCirugia() {
		return session.selectList(TRAE_MOTIVOS_NO_REPROGRAMACION_CIRUGIA);
	}

	@Override
	public List<Map<String, Object>> traeEstudiosAdjuntosMdc(Long fecha) {
		Map<String, Object> parms = new HashMap<String, Object>();
		parms.put("fecha", fecha != null ? new Date(fecha) : null);
		
		return session.selectList(TRAE_ESTUDIOS_ADJUNTOS_MDC, parms);
	}

	@Override
	public List<Map<String, Object>> traeObrasSocialesMdc() {
		return session.selectList(TRAE_OBRAS_SOCIALES_MDC);
	}

	@Override
	public List<Map<String, Object>> estadoAutorizacionesSGI() {
		return session.selectList(ESTADO_AUTORIZACIONES_SGI);
	}
	
	@Override
	public List<Map<String, Object>> clasificacionAutorizacionesSGI() {
		return session.selectList(CLASIFICACION_AUTORIZACIONES_SGI);
	}
	
	@Override
	public List<Map<String, Object>> obtenerMandatariasAutorizacionesSGI(String autorizaciones) {
		Map<String, Object> mapParamsSP = new HashMap<String, Object>();
		// Paramatros Store Procedure
		mapParamsSP.put("autorizaciones", autorizaciones);
		
		return session.selectList(OBTENER_MANDATARIAS_AUTORIZACIONES_SGI, mapParamsSP);
	}
	
	//mcastillo
	@Override
	public List<Map<String, Object>> obtenerEstudiosPorTipoAutorizacionSGI(Long fecha, String tipoAutorizacion) {
		Map<String, Object> mapParamsSP = new HashMap<String, Object>();
		mapParamsSP.put("fecha", fecha != null ? new Date(fecha) : null);
		mapParamsSP.put("tipoAutorizacion", tipoAutorizacion != null ? tipoAutorizacion : null);
		return session.selectList(OBTENER_ESTUDIOS_TIPO_AUTORIZACIONES_SGI, mapParamsSP);
	}

	@Override
	public List<Map<String, Object>> traeSubmotivosFinalizacion(Map<String, Object> map) {
		return session.selectList(TRAE_SUBMOTIVOS_FINALIZACION, map);
	}
	
}	
