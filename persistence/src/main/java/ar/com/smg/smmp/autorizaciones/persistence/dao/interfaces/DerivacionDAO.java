package ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces;

import java.util.Map;

import ar.com.smg.smmp.model.autorizaciones.gestion.Derivacion;

public interface DerivacionDAO extends DAO {

	String INSERTA_DERIVACION_MEDICA = "inserta_derivacion_medica";
	String TRAE_DERIVACION_MEDICA = "trae_datos_derivacion_medica";

	void insertaDerivacionMedica(Map<String, Object> map);
	Derivacion traeDerivacionMedica(Integer etapaId);
	
}
