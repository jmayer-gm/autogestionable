package ar.com.smg.smmp.autorizaciones.persistence.dao.implementations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import ar.com.smg.smmp.autorizaciones.persistence.dao.interfaces.PresupuestoDAO;

public class PresupuestoDAOImpl implements PresupuestoDAO {

	private SqlSession session;

	public PresupuestoDAOImpl() {
		session = null;
	}

	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void setSession(SqlSession session) {
		this.session = session;
	}

	
	@Override
	public List<Map<String, Object>> motivosPresupuesto(Map<String, Object> tipoTrm) {
		return session.selectList(MOTIVOS_PRESUPUESTO, tipoTrm);
	}

	@Override
	public void saveDataPresupuesto(HashMap<String, Object> dataReqPresupuesto) {
		session.insert(SAVE_DATA_PRESUPUESTO, dataReqPresupuesto);
	}

	@Override
	public void deleteDataPresupuestoMotivos(Long processId) {
		session.delete(DELETE_DATA_PRESUPUESTO_MOTIVO, processId);
	}

	@Override
	public void saveDataPresupuestoMotivo(Map<String, Object> motivo) {
		session.insert(SAVE_DATA_PRESUPUESTO_MOTIVO, motivo);
	}

	@Override
	public Map<String, Object> readReqTrmPresupuesto(Long processId) {
		return session.selectOne(READ_REQ_TRM_PRESUPUESTO, processId);
	}

	@Override
	public List<Map<String, Object>> readReqTrmPresupuestoMotivos(Long processId) {
		return session.selectList(READ_REQ_TRM_PRESUPUESTO_MOTIVOS, processId);
	}

	@Override
	public List<Map<String, Object>> tramitesPresupuestoAsociado(Long processId) {
		return session.selectList(READ_TRAMITES_PRESUPUESTO_ASOCIADOS, processId);
	}

	@Override
	public void updateDataPresupuesto(HashMap<String, Object> dataReqPresupuesto) {
		session.update(UPDATE_DATA_PRESUPUESTO, dataReqPresupuesto);
	}
}	
