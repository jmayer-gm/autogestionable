package ar.com.smg.smmp.autorizaciones.service.restapi;

import java.io.Serializable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

public interface Tipos<TResponse> extends Serializable {

	String APPLICATION_JSON = "application/json";
	String APPLICATION_XML = "application/xml";

	static final int STATUS_CODE_OK = 200;
	static final int STATUS_CODE_BAD_REQUEST = 400;
	static final int STATUS_CODE_NOT_FOUND = 404;
	static final int STATUS_CODE_INTERNAL_SERVER_ERROR = 500;
	static final int STATUS_CODE_SERVICE_UNAVAILABLE = 503;

	static final String STATUS_MSG_OK = "OK";
	static final String STATUS_MSG_BAD_REQUEST = "Bad Request";
	static final String STATUS_MSG_NOT_FOUND = "Not Found";
	static final String STATUS_MSG_INTERNAL_SERVER_ERROR = "Internal Server Error";
	static final String STATUS_MSG_SERVICE_UNAVAILABLE = "Service Unavailable";

	static final String GET_METHOD = "GET";
	static final String PUT_METHOD = "PUT";
	static final String PATCH_METHOD = "PATCH";
	static final String POST_METHOD = "POST";
	static final String DELETE_METHOD = "DELETE";

	/**
	 * Trae los tipos de autorizacion
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetTiposAutorizacion", notes = "Trae los tipos de autorizacion", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/tipos-autorizacion")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse getTiposAutorizacion();

	/**
	 * Trae los tipos de gestion
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetTiposGestion", notes = "Trae los tipos de gestion", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/tipos-gestion")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse getTiposGestion();

	/**
	 * Trae los tipos de paciente
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetTiposPaciente", notes = "Trae los tipos de paciente", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/tipos-paciente")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse getTiposPaciente();

	/**
	 * Trae los tipos de provision por obra social
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetTiposProvisionObraSocial", notes = "Trae los tipos de provision por obra social", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/tipos-provision-obra-social")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse getTiposProvisionObraSocial();

	/**
	 * Trae los tipos de agrupacion
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetTiposAgrupacion", notes = "Trae los tipos de agrupacion", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/tipos-agrupacion")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse getTiposAgrupacion();

	/**
	 * Trae los motivos por los que se excepciona
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetMotivosExcepcion", notes = "Trae los motivos por los que se excepciona", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/motivos-excepcion")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse getMotivosExcepcion();

	/**
	 * Trae los tipos de resolucion
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetTiposResolucion", notes = "Trae los tipos de resolucion", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/tipos-resolucion")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse getTiposResolucion();

	/**
	 * Trae los motivos por los que se rechaza
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetMotivosRechazo", notes = "Trae los motivos por los que se rechaza", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/motivos-rechazo")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse getMotivosRechazo();

	/**
	 * Trae los motivos por los que finaliza un tramite
	 * 
	 * @param tarea
	 *            Nombre de la tarea
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetMotivosFinalizacion", notes = "Trae los motivos por los que finaliza un tramite", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/motivos-finalizacion/{tarea}")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse getMotivosFinalizacion(@ApiParam(value = "Tarea") @PathParam("tarea") String tarea);

	/**
	 * Trae los tipos de resolucion ST
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetTiposResolucionST", notes = "Trae los tipos de resolucion ST", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/tipos-resolucion-st")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response getTiposResolucionST();

	/**
	 * Trae los tipos de practica odo
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetTiposPracticaOdo", notes = "Trae los tipos de practica odo", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/tipos-practica-odo")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response getTiposPracticaOdo();

	/**
	 * Trae los motivos odo
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetMotivosFinalizacionOdo", notes = "Trae los motivos odo", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/motivos-odo")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response getMotivosFinalizacionOdo(@ApiParam(value = "Codigo") @QueryParam("codigo") Integer codigo,
			@ApiParam(value = "Grupo") @QueryParam(value = "grupo") Integer grupo);

	/**
	 * Trae los resoluciones odo
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetResolucionesOdo", notes = "Trae los resoluciones odo", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/resoluciones-odo")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response getResolucionesOdo(@ApiParam(value = "Codigo") @QueryParam("codigo") Integer codigo);

	/**
	 * Trae los motivos para la no reprogramacion de un tramite de
	 * autorizaciones de cirugia
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "TraeMotivosNoReprogramacionCirugia", notes = "Trae los motivos para la no reprogramacion de un tramite de autorizaciones de cirugia", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/motivos-no-reprogramacion-cirugia")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response traeMotivosNoReprogramacionCirugia();

	/**
	 * Trae los tipos de estudio adjuntos para un tramite de autorizaciones que
	 * requiere materiales de cirugia
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "TraeEstudiosAdjuntosMdc", notes = "Trae los tipos de estudio adjuntos para un tramite de autorizaciones que requiere materiales de cirugia", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/estudios-adjuntos-mdc")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response traeEstudiosAdjuntosMdc(@ApiParam(value = "Fecha") @QueryParam("fecha") Long fecha);

	/**
	 * Trae la lista de obras sociales que se muestran en la tarea Controlar
	 * Obra Social del proceso Gestion Autorizaciones (MDC)
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "TraeObrasSocialesMdc", notes = "Trae la lista de obras sociales que se muestran en la tarea Controlar Obra Social del proceso Gestion Autorizaciones (MDC) ", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/obras-sociales-mdc")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response traeObrasSocialesMdc();

	/**
	 * Trae los estados de autorizacion
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "EstadoAutorizacionesSGI", notes = "Trae los estados de autorizacion", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/tipos/estados")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response estadoAutorizacionesSGI();

	/**
	 * Trae clasificacion de autorizaciones
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "ClasificacionAutorizacionesSGI", notes = "Trae clasificacion de autorizaciones", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/tipos/clasificaciones")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response clasificacionAutorizacionesSGI();

	/**
	 * Trae mandatarias de autorizaciones
	 * 
	 * @param autorizaciones
	 */

	@GET
	@ApiOperation(tags = "Tipos", value = "ObtenerMandatariasAutorizacionesSGI", notes = "Trae mandatarias de autorizaciones", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/mandatarias")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response obtenerMandatariasAutorizacionesSGI(
			@ApiParam(value = "Autorizaciones") @QueryParam("autorizaciones") String autorizaciones);

	/**
	 * Trae los tipos de estudio adjuntos segun el tipo de autorizacion para la
	 * tares auditar Autorizacion
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "ObtenerEstudiosPorTipoAutorizacion", notes = "Trae los tipos de estudio adjuntos segun el tipo de autorizacion para la tares auditar Autorizacion", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/estudios-adjuntos-tipo-autorizacion")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response obtenerEstudiosPorTipoAutorizacion(
			@ApiParam(value = "Fecha") @QueryParam("fecha") Long fecha,
			@ApiParam(value = "Tipo de Autorizacion") @QueryParam("tipoAutorizacion") String tipoAutorizacion);

	/**
	 * Trae los submotivos para determinada tarea
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Tipos", value = "GetSubmotivosFinalizacion", notes = "Trae los submotivos para determinada tarea", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/submotivos-finalizacion")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse getSubmotivosFinalizacion(@ApiParam(value = "Tarea") @QueryParam("tarea") String tarea);

}
