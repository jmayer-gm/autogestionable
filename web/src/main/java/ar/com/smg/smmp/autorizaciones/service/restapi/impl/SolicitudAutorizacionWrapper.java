package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.solicitud.SolicitudAutorizacion;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SolicitudesAutorizacion")
public class SolicitudAutorizacionWrapper {
	
	@XmlElement(required = true, name = "solicitudAutorizacion")	
	private SolicitudAutorizacion solicitudAutorizacion;

	public SolicitudAutorizacion getSolicitudAutorizacion() {
		return solicitudAutorizacion;
	}

	public void setSolicitudAutorizacion(SolicitudAutorizacion solicitudAutorizacion) {
		this.solicitudAutorizacion = solicitudAutorizacion;
	}

}
