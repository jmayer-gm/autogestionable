package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AutorizacionesPorAfiliadoEfectorReporte")
public class ListaAutorizacionPorAfiliadoEfectorWrapper {

	@XmlElement(required = true, name = "autorizacionesPorAfiliadoEfectorReporte")
	private  List autorizacionesPorAfiliadoEfectorReporte;



	public List getAutorizacionPorAfiliadoEfector() {
		return autorizacionesPorAfiliadoEfectorReporte;
	}

	public void setAutorizacionPorAfiliadoEfector(
			List autorizacionesPorAfiliadoEfectorReporte) {
		this.autorizacionesPorAfiliadoEfectorReporte = autorizacionesPorAfiliadoEfectorReporte;
	}
	


}

