package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.tipo.TipoGestion;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "TiposGestion")
public class ListaTiposGestionWrapper {

	@XmlElement(required = true, name = "tipoGestion")
	private  List<TipoGestion> tiposGestion = new ArrayList<TipoGestion>();

	public List<TipoGestion> getTiposGestion() {
		return tiposGestion;
	}

	public void setTiposGestion(List<TipoGestion> lista) {
		this.tiposGestion = lista;
	}

}
