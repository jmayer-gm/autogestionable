package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.gestion.AutorizacionPrestador;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AutorizacionesPorPrestador")
public class ListaAutorizacionPorPrestadorWrapper {

	@XmlElement(required = true, name = "autorizacionesPorPrestador")
	private  List<AutorizacionPrestador> autorizacionPorPrestador;
	@XmlElement(required = true, name = "cantidadTotalDePaginas")
	private  String cantTotalPag;

	@SuppressWarnings("unchecked")
	public void setAutorizacionPorPrestador(Map<String, Object> mapAutoYCantPaginas) {
		this.autorizacionPorPrestador = (List<AutorizacionPrestador>) mapAutoYCantPaginas.get("listaAutorizacionesPrestador");
		this.cantTotalPag = ((BigDecimal) mapAutoYCantPaginas.get("cantTotalPag"))+"";
	}

	public List<AutorizacionPrestador> getAutorizacionPorPrestador() {
		return autorizacionPorPrestador;
	}

	public void setAutorizacionPorPrestador(
			List<AutorizacionPrestador> autorizacionPorPrestador) {
		this.autorizacionPorPrestador = autorizacionPorPrestador;
	}
	
	public String getCantTotalPag() {
		return cantTotalPag;
	}

	public void setCantTotalPag(String cantTotalPag) {
		this.cantTotalPag = cantTotalPag;
	}

}

