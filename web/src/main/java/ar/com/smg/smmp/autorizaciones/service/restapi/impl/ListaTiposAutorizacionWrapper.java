package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAutorizacion;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "TiposAutorizacion")
public class ListaTiposAutorizacionWrapper {

	@XmlElement(required = true, name = "tipoAutorizacion")
	private  List<TipoAutorizacion> tiposAutorizacion = new ArrayList<TipoAutorizacion>();

	public List<TipoAutorizacion> getTiposAutorizacion() {
		return tiposAutorizacion;
	}

	public void setTiposAutorizacion(List<TipoAutorizacion> lista) {
		this.tiposAutorizacion = lista;
	}

}
