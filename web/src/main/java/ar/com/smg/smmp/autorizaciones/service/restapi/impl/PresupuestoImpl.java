package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import ar.com.smg.smmp.autorizaciones.service.interfaces.PresupuestoService;
import ar.com.smg.smmp.autorizaciones.service.restapi.PresupuestoRest;

@Path("/rest-api")
public class PresupuestoImpl implements PresupuestoRest<Response> {

	private static final long serialVersionUID = -5809497604046005875L;

	private static final Logger LOGGER = Logger.getLogger(PresupuestoImpl.class);

	private transient PresupuestoService presupuestoEJBService;

	public PresupuestoImpl(@Context ServletContext servletContext) {
		super();
		this.loadResources(servletContext);
	}

	private void loadResources(ServletContext servletContext) {
		// Spring application context.
		ApplicationContext appCtx = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

		// Get the EJB Service.
		presupuestoEJBService = (PresupuestoService) appCtx.getBean("presupuestoEJBService");
	}

	@Override
	public Response motivosPresupuesto(String tipoTrm) {
		try {
			Map<String, Object> tipoTrmMap = new HashMap<String, Object>();
			tipoTrmMap.put("tipoTrm", tipoTrm);
			
			List<Map<String, Object>> lista = this.presupuestoEJBService.motivosPresupuesto(tipoTrmMap);
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response saveReqTrmPresupuesto(HashMap<String, Object> dataReqPresupuesto) {
		try {
			this.presupuestoEJBService.saveReqTrmPresupuesto(dataReqPresupuesto);
			return Response.status(Response.Status.OK).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response updateReqTrmPresupuesto(HashMap<String, Object> dataReqPresupuesto) {
		try {
			this.presupuestoEJBService.updateReqTrmPresupuesto(dataReqPresupuesto);
			return Response.status(Response.Status.OK).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response reqTrmPresupuesto(Long processId) {
		try {
			Map<String, Object> data = this.presupuestoEJBService.reqTrmPresupuesto(processId);
			
			if (data != null && Boolean.valueOf(data.get("reqPresupuesto").toString())) {
				buildTipoPresupuesto(data);
				buildMotivoPresupuesto(data);
				data.put("observacionesPresupuesto", data.get("observacion"));
			}
			
			return Response.status(Response.Status.OK).entity(data).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response tramitesPresupuestoAsociado(Long processId) {
		try {
			List<Map<String, Object>> tramites = this.presupuestoEJBService.tramitesPresupuestoAsociado(processId);
			
			return Response.status(Response.Status.OK).entity(tramites).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	private void buildTipoPresupuesto(Map<String, Object> data) {
		//"tipoPresupuesto"	:	"Practica / Medicacion / Material",
		boolean practica = Boolean.valueOf(data.get("tipoPractica").toString());
		boolean medicacion = Boolean.valueOf(data.get("tipoMedicacion").toString());
		boolean material = Boolean.valueOf(data.get("tipoMaterial").toString());
		
		StringBuffer tipoPresupuesto = new StringBuffer("");
		String finalString = "";
		if (practica) {
			tipoPresupuesto.append("Practica / ");
		}
		if (medicacion) {
			tipoPresupuesto.append("Medicacion / ");
		}
		if (material) {
			tipoPresupuesto.append("Material / ");
		}
		if (tipoPresupuesto.length() > 0) {
			finalString = tipoPresupuesto.substring(0, tipoPresupuesto.length() -3);
		}
		data.put("tipoPresupuesto", finalString);
	}
	
	@SuppressWarnings("unchecked")
	private void buildMotivoPresupuesto(Map<String, Object> data) {
		//"motivoPresupuesto"		: "Prestador no convenido (Continuidad de tratamiento) / Sin prestador en zona / Sin disponibilidad de stock / Prestador no acepta provision / Valor presupuestado inferior al de compras",
		List<Map<String, Object>> motivos = (List<Map<String, Object>>) data.get("motivos");
		String finalString = "";
		StringBuffer motivoPresupuesto = new StringBuffer("");
		if (motivos != null) {
			for (Map<String, Object> motivo : motivos) {
				motivoPresupuesto.append(motivo.get("motivoDescripcion") + " / ");
			}
		}
		
		if (motivoPresupuesto.length() > 0) {
			finalString = motivoPresupuesto.substring(0, motivoPresupuesto.length() -3);
		}
		data.put("motivoPresupuesto", finalString);
		
	}


}