package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.tipo.TipoPaciente;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "TiposPaciente")
public class ListaTiposPacienteWrapper {

	@XmlElement(required = true, name = "tipoPaciente")
	private  List<TipoPaciente> tiposPaciente = new ArrayList<TipoPaciente>();

	public List<TipoPaciente> getTiposPaciente() {
		return tiposPaciente;
	}

	public void setTiposPaciente(List<TipoPaciente> lista) {
		this.tiposPaciente = lista;
	}

}
