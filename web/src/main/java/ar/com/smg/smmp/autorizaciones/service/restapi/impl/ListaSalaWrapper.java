package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.autorizacion.Sala;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Salas")
public class ListaSalaWrapper {

	@XmlElement(required = true, name = "salas")
	private List<Sala> salas = new ArrayList<Sala>();

	public List<Sala> getSalas() {
		return salas;
	}

	public void setSalas(List<Sala> Salas) {
		this.salas = Salas;
	}

}
