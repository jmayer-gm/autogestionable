package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.autorizacion.Prorroga;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Prorrogas")
public class ListaProrrogaWrapper {

	@XmlElement(required = true, name = "prorrogas")
	private List<Prorroga> prorrogas = new ArrayList<Prorroga>();

	public List<Prorroga> getProrrogas() {
		return prorrogas;
	}

	public void setProrrogas(List<Prorroga> prorrogas) {
		this.prorrogas = prorrogas;
	}

}
