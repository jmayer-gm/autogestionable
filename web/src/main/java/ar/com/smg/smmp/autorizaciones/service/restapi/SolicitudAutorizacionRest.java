package ar.com.smg.smmp.autorizaciones.service.restapi;

import java.io.Serializable;
import java.util.HashMap;
import javax.ws.rs.core.Response;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

public interface SolicitudAutorizacionRest<TResponse> extends Serializable {

	String APPLICATION_JSON = "application/json";
	String APPLICATION_XML = "application/xml";
	
	String AFI_PREPAGA_PARAM = "prepaga";
	String AFI_CONTRA_PARAM = "contra";
	String AFI_INTE_PARAM = "inte";
	String SOL_WORKFLOW_ID = "businessKey";
	String SOL_ETAPA_ID = "etapaId";
	String TIPO_AUTORIZACION = "tipoAutorizacion";
	
	static final String SUCURSAL_ID = "sucursalId";
	static final String NUMERO_AUTORIZACION = "numeroAutorizacion";
	String BAJA_FECHA = "bajaFecha";
	String PRECARGA_ID="precargaId";
	
	static final String FECHA_DESDE = "fechaDesde";
	static final String FECHA_HASTA = "fechaHasta";
	static final String PRESTADORID = "prestadorId";
	static final String CODIGO_PRESTADOR = "codigoPrestador";
	static final String TIPO = "tipo";
	static final String ESTADO = "estado";
	static final String DIAS = "dias";
	static final String PAGINA = "pagina";
	static final String CANT_REG_POR_PAG = "cantRegPorPag";
	
	static final String RECHA = "recha";
	static final String MANUAL = "manual";
	
	static final String ACTIVI = "actividad";
	static final String USUARIO = "usuario";
	
	/*	Farmacias */
	static final String FARMA_MANDA = "manda";
	static final String FARMA_ID = "farma";
	static final String FARMA_RAZON = "razon";
	static final String FARMA_PROVIN = "provin";
	static final String FARMA_LOCA = "loca";
	static final String EFECTOR = "efector";	
	static final String TIENE_VINCULABLE = "tieneVinculable";	

	
	static final String SUCUR = "sucur";
	static final String AUTO = "auto";
	static final String NOMEN = "nomen";
	static final String PRESTACION = "prestacion";
	static final String FECHA_CONSUMO = "fechaConsumo";
	
	String PATH_CONSULTA_SOLICITUDES_PENDIENTES = "/solicitudes-pendientes/{" + AFI_PREPAGA_PARAM + "}/{" + AFI_CONTRA_PARAM + "}/{" + AFI_INTE_PARAM + "}/";
	String PATH_CONSULTA_SOLICITUD_X_ETAPA_ID = "/solicitudes/{" + SOL_WORKFLOW_ID + "}/etapas/{" + SOL_ETAPA_ID + "}/";


	static final String COMMON_PATH = "/autorizaciones/{" + SUCURSAL_ID + "}/{" + NUMERO_AUTORIZACION + "}";
	
//	String PATH_VALIDAR_PRORROGA = COMMMON_PATH + "/prorroga?q=validar";
	String PATH_VALIDAR_PRORROGA = COMMON_PATH + "/{" + AFI_PREPAGA_PARAM + "}/" + "{" + AFI_CONTRA_PARAM + "}/" + "{" + AFI_INTE_PARAM + "}/validarProrroga";
	String PATH_PRORROGAS = COMMON_PATH + "/prorrogas";
	String PATH_SALAS = "/salas";
	String PATH_INTERNACIONES = COMMON_PATH + "/internaciones";
	String PATH_DATOS_EMAIL_SOCIO = COMMON_PATH + "/datos-email-socio";
	String PATH_AUTORIZACIONES_PRESTADOR = "/autorizaciones/prestador";
	String PATH_AUTORIZACIONES_AFILIADO = "/autorizaciones/afiliado/{" + AFI_PREPAGA_PARAM + "}/" + "{" + AFI_CONTRA_PARAM + "}/" + "{" + AFI_INTE_PARAM + "}" ;
	
	String PATH_AUTORIZACIONES_FARMACIA = "/autorizaciones/farmacia";
	String PATH_AUTORIZACIONES_FARMACIA_LOCA = PATH_AUTORIZACIONES_FARMACIA + "/ubicacion";
	String COMMON_PATH_EQUIPO = "/autorizaciones/{" + SUCURSAL_ID + "}/{" + NUMERO_AUTORIZACION + "}/equipos";
	String COMMON_PATH_AUTORIZACIONES = "/autorizaciones";
	String COMMON_PATH_AUTORIZACIONES_ODO = "/autorizacionesOdo";
	String COMMON_PATH_MODULOS = "/autorizaciones/{" + SUCURSAL_ID + "}/{" + NUMERO_AUTORIZACION + "}/modulos";
    String COMMON_PATH_AUTORIZACIONES_TRAE="/autorizaciones/trae";
    String COMMON_PATH_AUTORIZACIONES_PRESTADOR_TRAE="/autorizaciones/prestador/trae";
	String AUTO_PATH_PARAM = "/{auto}";
	String AUTO_PARAM = "auto";
	String SUCUR_PATH_PARAM = "/{sucur}";
	String SUCUR_PARAM = "sucur";
	String PRESTACIONES_PATH = "/prestaciones";
	String AUTORIZACIONES_SOLAPADAS_PATH = "/solapamiento-auto";
	String VALIDAR_CONVENIO = "/validaconvenio";
	String VALIDAR_CONVENIO_INTER = "/validaconvenio-inter";
	String VALIDAR_CONVENIO_CIRU = "/validaconvenio-ciru";
	String VALIDAR_COBERTURA = "/validacobertura";
	String VALIDAR_COBERTURA_MADRE ="/validacoberturaMadre";
	String VALIDAR_GENERO_Y_EDAD = "/validargeneroyedad";
	String VALIDAR_CARENCIA = "/validar-carencia";
	String PLNS_CONFIG_MODULOS = "/plns-config-modulos";
	String COPAGO_PRESTACION = "/copago-prestacion";
	String DOCUMENTACION_ADJUNTA_AUTORIZACION = "/documentacionadjunta";
	String DESCUENTO_PRODUCTO = "/descuento-producto";
	String VALIDAR_PRODUCTO = "/validar-producto";
	String REGISTRA_OPERACIONES_FALLIDAS_FARMALINK = "/registra-fallida-farmalink";
	
	String PATH_SITUACIONES_TERAPEUTICAS = "/situacion-terapeutica/{"+ PRECARGA_ID +"}/trae-documentacion";
	
	String PATH_VALOR_LEY = "/autorizaciones/valor-ley/{"+NOMEN+"}/{"+PRESTACION+"}/historial";
	
	String PATH_CLASIFICACIONES = COMMON_PATH + "/clasificaciones";
	String PATH_PRESTACIONES_AUTORIZACION = "/autorizaciones" + SUCUR_PATH_PARAM + AUTO_PATH_PARAM + "/prestaciones";
	
	String PATH_DESCRIPCION_RECHA = COMMON_PATH_AUTORIZACIONES  + "/descripcionrecha";

	String PATH_AUDITAR_AUTORIZACION = "/auditarAutorizacion";
	
	String PATH_REGISTRAR_ENVIO_FARMALINK = "/registraEnvioAFarmalink";
	
	String RAZONES_PATH = "/razones";
	String MOTIVOS_PATH = "/motivos";
	String PRODUCTOS_CONVER_SAP = "/productos-conver-sap";
	
	
	//Responses
	static final int STATUS_CODE_OK = 200;
	static final int STATUS_CODE_BAD_REQUEST = 400;
	static final int STATUS_CODE_NOT_FOUND = 404;
	static final int STATUS_CODE_INTERNAL_SERVER_ERROR = 500;
	static final int STATUS_CODE_SERVICE_UNAVAILABLE = 503;

	static final String STATUS_MSG_OK = "OK";
	static final String STATUS_MSG_BAD_REQUEST = "Bad Request";
	static final String STATUS_MSG_NOT_FOUND = "Not Found";
	static final String STATUS_MSG_INTERNAL_SERVER_ERROR = "Internal Server Error";
	static final String STATUS_MSG_SERVICE_UNAVAILABLE = "Service Unavailable";

	static final String GET_METHOD = "GET";
	static final String PUT_METHOD = "PUT";
	static final String PATCH_METHOD = "PATCH";
	static final String POST_METHOD = "POST";
	static final String DELETE_METHOD = "DELETE";
	
	@GET
	@Path (PATH_CONSULTA_SOLICITUDES_PENDIENTES)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Trae Solicitudes Pendientes", notes = "Trae solicitudes de autorizaciones pendientes", httpMethod = GET_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerSolicitudesPendientes(@ApiParam (value="Codigo de prepaga") @PathParam(AFI_PREPAGA_PARAM) Integer prepaga, 
	@ApiParam (value="Contrato") @PathParam(AFI_CONTRA_PARAM) String contra,
	@ApiParam (value =" Inte") @PathParam(AFI_INTE_PARAM) String inte);
	
	@GET
	@Path (PATH_CONSULTA_SOLICITUD_X_ETAPA_ID)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Trae Solicitud por etapaID", notes = "Trae solicitud por ID de etapa", httpMethod = GET_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerSolicitudByEtapaId(@ApiParam (value="Business Key") @PathParam(SOL_WORKFLOW_ID) Integer businessKey, 
	@ApiParam (value ="Etapa ID") @PathParam(SOL_ETAPA_ID) Integer etapaId);

	@GET
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Validar prorroga", notes = "Trae la validacion de la prorroga", httpMethod = GET_METHOD)
	@Path (PATH_VALIDAR_PRORROGA)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse validarProrroga(@ApiParam(value="ID de sucursal") @PathParam(SUCURSAL_ID) Integer sucursalId,
	@ApiParam(value="Numero autoriz") @PathParam(NUMERO_AUTORIZACION) Integer numeroAutorizacion,
	@ApiParam(value="Prepaga") @PathParam (AFI_PREPAGA_PARAM) Integer prepaga,
	@ApiParam(value="Contrato") @PathParam(AFI_CONTRA_PARAM) String contrato,
	@ApiParam (value = "Integrante") @PathParam(AFI_INTE_PARAM) String integrante);
	
	@GET
	@Path (PATH_PRORROGAS)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer autorizaciones asociadas", notes = "Trae la autorizacion asociada", httpMethod = GET_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerAutorizacionesAsociadas(@ApiParam(value="ID de sucursal") @PathParam(SUCURSAL_ID) Integer sucursalId,
	@ApiParam(value="numero autorizacion") @PathParam(NUMERO_AUTORIZACION) Integer numeroAutorizacion);
	
	@GET
	@Path (PATH_SALAS)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer salas", notes = "Trae la sala de acuerdo a su fecha de baja", httpMethod = GET_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerSalas(@ApiParam(value="Fecha de baja") @QueryParam(BAJA_FECHA) String bajaFecha);
	
	@GET
	@Path (PATH_INTERNACIONES)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer internaciones", notes = "Trae internaciones por sucursal y numero de autoriacion", httpMethod = GET_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerInternaciones(@ApiParam(value="Sucursal") @PathParam(SUCURSAL_ID) Integer sucursalId,
	@ApiParam(value="Numero Autorizacion") @PathParam(NUMERO_AUTORIZACION) Integer numeroAutorizacion);
	
	@GET
	@Path (PATH_DATOS_EMAIL_SOCIO)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer datos del mail del socio", notes = "Trae datos de un mail a partir de sucursal y numero autoriz", httpMethod = GET_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerDatosEmailSocio(@ApiParam(value="ID de sucursal") @PathParam(SUCURSAL_ID) Integer sucursalId, 
	 @ApiParam(value="Numero de autorizacion") @PathParam(NUMERO_AUTORIZACION) Integer numeroAutorizacion);
	
	@GET
	@Path (PATH_SITUACIONES_TERAPEUTICAS)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer Documentacion Situacion Terapeutica", notes = "A partir del ID de precarga trae la doc de situacion terapeutica", httpMethod = GET_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerDocumentacionSituacionTerapeutica(@PathParam(PRECARGA_ID) Integer precargaId);

	@GET
		@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer Documentacion Situacion Terapeutica", notes = "Trae la doc de situacion terapeutica", httpMethod = GET_METHOD)
	@Path (PATH_AUTORIZACIONES_PRESTADOR)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	
	TResponse traerAutorizacionesPorPrestador(@ApiParam (value="Fecha desde") @QueryParam(FECHA_DESDE) String fechaDesde,
	@ApiParam (value="Fecha hasta") @QueryParam(FECHA_HASTA) String fechaHasta, 
	@ApiParam (value="ID Prestador") @QueryParam(PRESTADORID) String prestadorId,
	@ApiParam (value="Tipo") @QueryParam(TIPO) String tipo, 
	@ApiParam (value="Estado") @QueryParam(ESTADO) String estado,
	@ApiParam (value="Dias") @QueryParam(DIAS) String dias,
	@ApiParam (value="Pagina") @QueryParam(PAGINA) String pagina,
	@ApiParam (value="Cantidad Reg por pagina") @QueryParam(CANT_REG_POR_PAG) String cantRegPorPag);

	@GET
	@Path (PATH_AUTORIZACIONES_AFILIADO)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	TResponse traerAutorizacionesPorAfiliado(
			@PathParam(AFI_PREPAGA_PARAM) Integer prepaga, 
			@PathParam(AFI_CONTRA_PARAM) String contra, 
			@PathParam(AFI_INTE_PARAM) String inte,		
			@QueryParam(EFECTOR) String efectorId,
			@QueryParam(TIENE_VINCULABLE) String tieneVinculable,
			@QueryParam(SUCUR) Integer sucur,
			@QueryParam(AUTO) Integer auto,
			@QueryParam(NOMEN) Integer nomen,
			@QueryParam(PRESTACION) String prestacion,
			@QueryParam(FECHA_CONSUMO) String fechaConsumo);
	
	@GET
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Trae farmacias", notes = "Trae farmacias por mandatario, id o razon", httpMethod = GET_METHOD)
	@Path (PATH_AUTORIZACIONES_FARMACIA)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerFarmacias(@ApiParam (value="Mandatario") @QueryParam(FARMA_MANDA) Integer manda, 
	@ApiParam (value="ID Farmacia") @QueryParam(FARMA_ID) Integer id,
	@ApiParam (value="Razon") @QueryParam(FARMA_RAZON) String razon);
	
	@GET
	@Path (PATH_AUTORIZACIONES_FARMACIA_LOCA)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer farmacias por ubicacion", notes = "Trae farmacias por ubicacion", httpMethod = GET_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerFarmaciasByUbicacion(@ApiParam (value ="Mandatario") @QueryParam(FARMA_MANDA) Integer manda, 
	@ApiParam (value ="Provincia") @QueryParam(FARMA_PROVIN) String provin, 
	@ApiParam (value="Localidad") @QueryParam(FARMA_LOCA) String loca, 
	@ApiParam (value="Razon social") @QueryParam(FARMA_RAZON) String razon);


	@GET
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Trae equipos por Autorizacion", notes = "Trae equipos por autorizacion", httpMethod = GET_METHOD)
	@Path (COMMON_PATH_EQUIPO)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerEquiposXAutorizacion(
			@ApiParam(value="ID Sucursal") @PathParam(SUCURSAL_ID) String sucursalId, 
			@ApiParam (value="ID Autorizacion") @PathParam(NUMERO_AUTORIZACION) String autorizacionId);
	
	
	@GET
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer Autorizacion con equipos", notes = "Trae autorizaciones con equipos", httpMethod = GET_METHOD)
	@Path (COMMON_PATH_AUTORIZACIONES)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerAtorizacionesConEqupipos(@QueryParam("tipo") String tipo, @QueryParam("equipos") String equipos);
	
	@GET
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Trae modulos por autorizacion", notes = "Trae autorizaciones por autorizacion", httpMethod = GET_METHOD)
	@Path (COMMON_PATH_MODULOS)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerModulosXAutorizacion(
				@ApiParam (value="Sucursal") @PathParam(SUCURSAL_ID) String sucursalId, 
				@ApiParam (value="Numero autorizacion") @PathParam(NUMERO_AUTORIZACION) String autorizacionId);

	
	@GET
	@Path (COMMON_PATH_AUTORIZACIONES_TRAE)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer Autorizacion de Afiliado por tipo", notes = "Trae autorizaciones de afiliado por tipo", httpMethod = GET_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerAutorizacionesAfiliadoPorTipo(
			@ApiParam (value="Contrato") @QueryParam(AFI_CONTRA_PARAM) String contra, 
			@ApiParam (value="Prepaga")@QueryParam(AFI_PREPAGA_PARAM) Integer prepaga,
			@ApiParam (value="Inte")@QueryParam(AFI_INTE_PARAM) String inte,
			@ApiParam (value="Tipo Autorizacion")@QueryParam(TIPO_AUTORIZACION) String tipo,
			@ApiParam (value="Fecha desde")@QueryParam(FECHA_DESDE) String fechaDesde,
			@ApiParam (value="fecha hasta")@QueryParam(FECHA_HASTA) String fechaHasta
			);
	
	@GET
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer Datos Cab Autorizaciones", notes = "Trae datos Cab Autorizaciones", httpMethod = GET_METHOD)
	@Path (COMMON_PATH_AUTORIZACIONES_ODO + SUCUR_PATH_PARAM + AUTO_PATH_PARAM)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerDatosCabAutorizacionesOdo(
		@ApiParam (value="Autorizacion")	@PathParam(AUTO_PARAM) Integer auto,
		@ApiParam (value="Sucursal")	@PathParam(SUCUR_PARAM) Integer sucur
			);
	@GET
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer Prestaciones por Autorizacion", notes = "Traer Prestaciones por Autorizacion", httpMethod = GET_METHOD)
	@Path (COMMON_PATH_AUTORIZACIONES_ODO + SUCUR_PATH_PARAM + AUTO_PATH_PARAM + PRESTACIONES_PATH)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerPrestacionesAutorizacionesOdo(
			@ApiParam (value="Autorizacion ID")  @PathParam(AUTO_PARAM) Integer auto,
			@ApiParam (value="Sucursal")  @PathParam(SUCUR_PARAM) Integer sucur
			);

	@GET
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer Autorizacion Prestador por Tipo", notes = "Traer Autorizacion Prestador por Tipo", httpMethod = GET_METHOD)
	@Path (COMMON_PATH_AUTORIZACIONES_PRESTADOR_TRAE)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerAutorizacionesPrestadorPorTipo(
			@ApiParam (value="Codigo prestador") @QueryParam(value="Codigo prestador") Integer codigoPrestador,
			@ApiParam (value="Fecha desde") @QueryParam(value="Fecha desde") String fechaDesde,
			@ApiParam (value="Fecha hasta") @QueryParam(value="Fecha Hasta") String fechaHasta,
			@ApiParam (value="Tipo") @QueryParam(value="Tipo") String tipo
			);


	@POST
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Validar Autorizaciones solapadas", notes = "Validar Autorizaciones solapadas", httpMethod = POST_METHOD)
	@Path (AUTORIZACIONES_SOLAPADAS_PATH)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse validarAutorizacionesSolapadas(@ApiParam (value="Parametros") HashMap<String, Object> params);


	
	@POST
	@Path (VALIDAR_CONVENIO)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Validar Convenio", notes = "Validar convenio", httpMethod = POST_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse validarConvenio(@ApiParam (value="Parametros") HashMap<String, Object> params);
	

	

	@POST
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Validar Convenio Internacion", notes = "Validar Convenio Internacion", httpMethod = POST_METHOD)
	@Path (VALIDAR_CONVENIO_INTER)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse validarConvenioInter(@ApiParam (value="Parametros") HashMap<String, Object> params);

	@POST
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Validar Convenio Cirugia", notes = "Validar Convenio Cirugia", httpMethod = POST_METHOD)
	@Path (VALIDAR_CONVENIO_CIRU)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse validarConvenioCiru(@ApiParam (value="Parametros") HashMap<String, Object> params);
	
	@POST
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Validar Genero y Edad", notes = "Validar Genero y Edad", httpMethod = POST_METHOD)
	@Path (VALIDAR_GENERO_Y_EDAD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse validarGeneroYEdad(@ApiParam (value="Parametros") HashMap<String, Object> params);
	@POST
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Validar Carencia", notes = "Validar Carencia", httpMethod = POST_METHOD)
	@Path (VALIDAR_CARENCIA)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse validarCarencia(@ApiParam (value="Parametros") HashMap<String, Object> params);


	
	@POST	
	@Path (COPAGO_PRESTACION)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Trae Copago Prestacion", notes = "Trae Copago Prestacion", httpMethod = POST_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traeCopagoPrestacion(@ApiParam (value="Parametros") HashMap<String, Object> params);
	
	@GET
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer Historial Valor Ley ", notes = "Trae historial de valor por ley", httpMethod = GET_METHOD)
	@Path (PATH_VALOR_LEY)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerHistorialValorLey(@ApiParam (value="Nomenclatura")  @PathParam(NOMEN) Integer nomen,@ApiParam (value="Prestacion")  @PathParam(PRESTACION) String prestacion);


	@POST
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Validar Cobertura", notes = "Validar Cobertura", httpMethod = POST_METHOD)
	@Path (VALIDAR_COBERTURA)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	TResponse validarCobertura(@ApiParam (value="Parametros") HashMap<String, Object> params);
	@GET
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer estudios tramite", notes = "Trae estudios tramite", httpMethod = GET_METHOD)
	@Path ("{workflowId}/estudios-tramite")
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	
	TResponse traerEstudiosTramite(@ApiParam (value="Workflow")  @PathParam("workflowId") Integer workflowId);

	@POST
	@Path (PLNS_CONFIG_MODULOS)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "PlnsConfigModulos", notes = "PlnsConfigModulos", httpMethod = GET_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse plnsConfigModulos(@ApiParam (value="parametros") HashMap<String, Object> params);
	
	
	@GET
	@Path (PATH_CLASIFICACIONES)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer Clasificaciones por sucursal y numero autorizacion", notes = "Traer Clasificaciones por sucursal y numero autorizacion", httpMethod = GET_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerClasificaciones(@ApiParam (value="ID Sucursal")  @PathParam(SUCURSAL_ID) Integer sucursalId, @ApiParam (value="Numero Autorizacion")  @PathParam(NUMERO_AUTORIZACION) Integer numeroAutorizacion);

	@GET
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Trae Prestaciones Autorizacion", notes = "Trae Prestaciones Autorizacion", httpMethod = GET_METHOD)
	@Path (PATH_PRESTACIONES_AUTORIZACION)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traePrestacionesAutorizacion(@ApiParam (value="ID Sucursal")  @PathParam("sucur") Integer sucursalId, @ApiParam (value="Autorizacion")  @PathParam("auto") Integer numeroAutorizacion);
	
	@GET
	@Path (PATH_DESCRIPCION_RECHA)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Traer Autorizacion por Afiliado", notes = "Trae autorizaciones por afiliado", httpMethod = GET_METHOD)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerDescripcionRecha(@ApiParam (value="Recha") @QueryParam(RECHA) Integer recha, @ApiParam (value="Manual") @QueryParam(MANUAL) Integer manual);
	
	
	@Path (PATH_AUDITAR_AUTORIZACION)
	void auditarAutorizacion(@ApiParam (value="Sucursal") @QueryParam(SUCURSAL_ID) Integer sucursal, 
	@ApiParam (value="Numero autorizacion") @QueryParam(NUMERO_AUTORIZACION) Integer numeroAutorizacion,
	@ApiParam (value="Actividad") @QueryParam(ACTIVI) String actividad, 
	@ApiParam (value="Usuario") @QueryParam(USUARIO) String usuario);

	@POST
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Obtener descuento producto", notes = "Obtener descuento producto", httpMethod = POST_METHOD)
	@Path (DESCUENTO_PRODUCTO)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse obtenerProceDtoProducto(@ApiParam (value="Parametros") HashMap<String, Object> params);
	
	@POST
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Validar producto", notes = "Validar producto", httpMethod = POST_METHOD)
	@Path (VALIDAR_PRODUCTO)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse validarProducto(@ApiParam (value="Parametros") HashMap<String, Object> params);

	@GET
	@Path (PATH_REGISTRAR_ENVIO_FARMALINK)
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Registra envio a Farmalink", notes = "Registra envio a Farmalink", httpMethod = GET_METHOD)
	void registraEnvioAFarmalink(@ApiParam (value="Auto") @QueryParam("auto") Integer auto,
	@ApiParam (value="Nro Autorizacion") @QueryParam("sucur") Integer sucur, 
	@ApiParam (value="produ") @QueryParam("produ") String produ,
	@ApiParam (value="envio") @QueryParam("envio") Integer envio,
	@ApiParam (value="usuario") @QueryParam("usuario") String usuario);
	
	@GET
	@ApiOperation(tags = "Razones", value = "Traer Razones", notes = "Traer Razones", httpMethod = GET_METHOD)
	@Path (COMMON_PATH_AUTORIZACIONES_ODO + RAZONES_PATH)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerRazonesOdo();
	
	@GET
	@ApiOperation(tags = "Motivos", value = "Traer Motivos", notes = "Traer Motivos", httpMethod = GET_METHOD)
	@Path (COMMON_PATH_AUTORIZACIONES_ODO + MOTIVOS_PATH)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerMotivosOdo(@ApiParam (value="presupAprobado") @QueryParam("presupAprobado") Integer presupAprobado);
	
	@POST
	@ApiOperation(tags = "ProductosConverSap", value = "Obtener codigo de producto sap segun codigo de material", notes = "Obtener codigo de producto sap segun codigo de material", httpMethod = POST_METHOD)
	@Path (PRODUCTOS_CONVER_SAP)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse obtenerConversionProductoSap(@ApiParam (value="Parametros") HashMap<String, Object> params);

	@POST
	@ApiOperation(tags = "SolicitudAutorizaciones", value = "Validar Cobertura Madre", notes = "Validar Cobertura Madre", httpMethod = POST_METHOD)
	@Path (VALIDAR_COBERTURA_MADRE)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	TResponse validarCoberturaMadre(@ApiParam (value="Parametros") HashMap<String, Object> params);
	
}
