package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


import ar.com.smg.smmp.model.global.Documentacion;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "DocumentacionSituacionesTeraputicas")
public class ListaDocumentacionSituacionTerapeuticaWrapper {
	
	@XmlElement(required = true, name = "documentacionSituacionesTeraputicas")
	private List<Documentacion> situacionesTerapeuticas = new ArrayList<Documentacion>();
	
	public List<Documentacion> getDocumentacionSituacionTerapeutica() {
		return situacionesTerapeuticas;
	}

	public void setDocumentacionSituacionTerapeutica(List<Documentacion> situacionesTerapeuticas) {
		this.situacionesTerapeuticas = situacionesTerapeuticas;
	}

}