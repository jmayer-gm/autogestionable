package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoExcepcion;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MotivosExcepcion")
public class ListaMotivosExcepcionWrapper {

	@XmlElement(required = true, name = "motivosExcepcion")
	private  List<MotivoExcepcion> motivosExcepcion = new ArrayList<MotivoExcepcion>();

	public List<MotivoExcepcion> getMotivosExcepcion() {
		return motivosExcepcion;
	}

	public void setMotivosExcepcion(List<MotivoExcepcion> lista) {
		this.motivosExcepcion = lista;
	}

}
