package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.common.DatosEmail;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "DatosEmail")
public class ListaDatosEmailWrapper {

	@XmlElement(required = true, name = "datosEmail")
	private List<DatosEmail> datosEmail = new ArrayList<DatosEmail>();

	public List<DatosEmail> getDatosEmail() {
		return datosEmail;
	}

	public void setDatosEmail(List<DatosEmail> datosEmail) {
		this.datosEmail = datosEmail;
	}

}
