package ar.com.smg.smmp.autorizaciones.service.restapi;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.io.Serializable;
import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

public interface PresupuestoRest<TResponse> extends Serializable {

	String APPLICATION_JSON = "application/json";
	String APPLICATION_XML = "application/xml";

	static final int STATUS_CODE_OK = 200;
	static final int STATUS_CODE_BAD_REQUEST = 400;
	static final int STATUS_CODE_NOT_FOUND = 404;
	static final int STATUS_CODE_INTERNAL_SERVER_ERROR = 500;
	static final int STATUS_CODE_SERVICE_UNAVAILABLE = 503;

	static final String STATUS_MSG_OK = "OK";
	static final String STATUS_MSG_BAD_REQUEST = "Bad Request";
	static final String STATUS_MSG_NOT_FOUND = "Not Found";
	static final String STATUS_MSG_INTERNAL_SERVER_ERROR = "Internal Server Error";
	static final String STATUS_MSG_SERVICE_UNAVAILABLE = "Service Unavailable";

	static final String GET_METHOD = "GET";
	static final String PUT_METHOD = "PUT";
	static final String PATCH_METHOD = "PATCH";
	static final String POST_METHOD = "POST";
	static final String DELETE_METHOD = "DELETE";

	

	/**
	 * Trae los motivos de presupuesto
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Presupuesto", value = "MotivosPresupuesto", notes = "Trae los motivos de presupuesto", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/presupuesto/motivos-presupuesto/{tipoTrm}")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response motivosPresupuesto(@ApiParam(value="MotivosPresupuesto") @PathParam("tipoTrm") String tipoTrm);
	
	/**
	 * Graba los datos de un tramite que requiere presupuesto 
	 * 
	 * @return
	 */
	@POST
	@ApiOperation(tags = "Presupuesto", value = "GrabaRequierePresupuesto", notes = "Graba Requiere presupuesto", httpMethod = POST_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/presupuesto/req-trm-presupuesto")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response saveReqTrmPresupuesto(@ApiParam(value="DataReqPresupuesto") HashMap<String, Object> dataReqPresupuesto);

	/**
	 * Actualiza los datos de un tramite que requiere presupuesto 
	 * 
	 * @return
	 */
	@PUT
	@ApiOperation(tags = "Presupuesto", value = "ActualizaRequierePresupuesto", notes = "Actualiza Requiere presupuesto", httpMethod = PUT_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/presupuesto/req-trm-presupuesto")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response updateReqTrmPresupuesto(@ApiParam(value="DataReqPresupuesto") HashMap<String, Object> dataReqPresupuesto);

	
	/**
	 * Trae los datos de requiere presupuesto
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Presupuesto", value = "dataReqTrmPresupuesto", notes = "Trae los datos de requiere presupuesto", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/presupuesto/req-trm-presupuesto/{processId}")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response reqTrmPresupuesto(@ApiParam(value="DataReqTrmPresupuesto") @PathParam("processId") Long processId);
	
	/**
	 * Trae los datos de tramites de presupuesto asociados al processId indicado
	 * 
	 * @return
	 */
	@GET
	@ApiOperation(tags = "Presupuesto", value = "Tramites presupuesto asociados", notes = "Tramites presupuesto asociados", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/presupuesto/tramite-presupuesto-asociado/{processId}")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response tramitesPresupuestoAsociado(@ApiParam(value="DataReqTrmPresupuesto") @PathParam("processId") Long processId);

	
}
