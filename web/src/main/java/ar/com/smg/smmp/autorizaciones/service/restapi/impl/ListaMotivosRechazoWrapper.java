package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoRechazo;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MotivosRechazo")
public class ListaMotivosRechazoWrapper {

	@XmlElement(required = true, name = "motivosRechazo")
	private  List<MotivoRechazo> motivosRechazo = new ArrayList<MotivoRechazo>();

	public List<MotivoRechazo> getMotivosRechazo() {
		return motivosRechazo;
	}

	public void setMotivosRechazo(List<MotivoRechazo> lista) {
		this.motivosRechazo = lista;
	}
	
}
