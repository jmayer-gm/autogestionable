package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.solicitud.SolicitudAutorizacion;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SolicitudesAutorizacion")
public class ListaSolicitudAutorizacionWrapper {

	@XmlElement(required = true, name = "solicitudAutorizacion")
	private  List<SolicitudAutorizacion> solicitudesAutorizacion;

	public List<SolicitudAutorizacion> getSolicitudesAutorizacion() {
		return solicitudesAutorizacion;
	}

	public void setSolicitudesAutorizacion(List<SolicitudAutorizacion> solicitudesAutorizacion) {
		this.solicitudesAutorizacion = solicitudesAutorizacion;
	}
}
