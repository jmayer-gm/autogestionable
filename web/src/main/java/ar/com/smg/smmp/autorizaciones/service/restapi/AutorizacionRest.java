package ar.com.smg.smmp.autorizaciones.service.restapi;

import java.io.Serializable;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;



import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

public interface AutorizacionRest<TResponse> extends Serializable {

	String APPLICATION_JSON = "application/json";
	String APPLICATION_XML = "application/xml";
	
	/** REST API Field request. */
	String KEY_FIELD = "key";
	String SUCURSAL_ID = "sucursalId";
	String NUMERO_AUTORIZACION = "numeroAutorizacion";
	String COMMON_PATH = "/autorizaciones/{" + SUCURSAL_ID + "}/{" + NUMERO_AUTORIZACION + "}";
	String COMMON_PATH_LOGS = "/autorizaciones/{" + NUMERO_AUTORIZACION + "}/{" + SUCURSAL_ID + "}/auditoria";
	String COMMON_PATH_EQUIPO = "/autorizaciones/{" + SUCURSAL_ID + "}/{" + NUMERO_AUTORIZACION + "}/equipos";
	String COMMON_PATH_AUTORIZACIONES = "/autorizaciones";
	String COMMON_PATH_AUTO_ODO = "/autorizacionesOdo";
	String COMMON_PATH_AUTORIZACIONES_MAIL_DATA = "/autorizaciones/mail-data";
	String AUTO_PATH_PARAM = "/{auto}";
	String SUCUR_PATH_PARAM = "/{sucur}";
	String PATH_EXPEDIENTES_AUTORIZACION = "/autorizaciones" + SUCUR_PATH_PARAM + AUTO_PATH_PARAM + "/expedientes";
	String PATH_VALIDA_TOPES_PRESTACION = "/valida-tope-prestacion";
	String PATH_MANDATARIA_BY_RUDI_ID = "/mandataria/{rudiId}";

	
	//Constantes para Swagger Annotations
	static final int STATUS_CODE_OK = 200;
	static final int STATUS_CODE_BAD_REQUEST = 400;
	static final int STATUS_CODE_NOT_FOUND = 404;
	static final int STATUS_CODE_INTERNAL_SERVER_ERROR = 500;
	static final int STATUS_CODE_SERVICE_UNAVAILABLE = 503;

	static final String STATUS_MSG_OK = "OK";
	static final String STATUS_MSG_BAD_REQUEST = "Bad Request";
	static final String STATUS_MSG_NOT_FOUND = "Not Found";
	static final String STATUS_MSG_INTERNAL_SERVER_ERROR = "Internal Server Error";
	static final String STATUS_MSG_SERVICE_UNAVAILABLE = "Service Unavailable";

	static final String GET_METHOD = "GET";
	static final String PUT_METHOD = "PUT";
	static final String PATCH_METHOD = "PATCH";
	static final String POST_METHOD = "POST";
	static final String DELETE_METHOD = "DELETE";
	
	
	
	
	@GET
	@Path (COMMON_PATH)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiOperation(tags = "Autorizaciones", value="Traer autorizaciones por Sucursal e ID Autorizacion", notes ="Trae autorizaciones por sucursal e ID Autorizacion", httpMethod= GET_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse traerAutorizacionXSucursalYId(
			@ApiParam(value="Sucursal") @PathParam(SUCURSAL_ID) String sucursalId, 
			@ApiParam(value="Numero Autorizacion") @PathParam(NUMERO_AUTORIZACION) String autorizacionId);
	
	@GET
	@Path (COMMON_PATH_LOGS)
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@ApiOperation(tags = "Autorizaciones", value="Obtener Logs de Autorizacion por sucursal e ID Autorizacion", notes ="Trae autorizaciones por sucursal e ID Autorizacion", httpMethod= GET_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	TResponse obtenerLogsAutorizacionXSucursalYId(
			@ApiParam(value="Sucursal") @PathParam(SUCURSAL_ID) String sucursalId, 
			@ApiParam(value="Numero Autorizacion") @PathParam(NUMERO_AUTORIZACION) String autorizacionId);

	@GET
	@Path (COMMON_PATH_AUTORIZACIONES + "/{workflowId}/autorizacion")
	@ApiOperation(tags = "Autorizaciones", value="Trae Autorizacion por WorkflowID", notes ="Trae Autorizacion por WorkflowID", httpMethod= GET_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	TResponse recoverAutoByWorkflowId(
			@ApiParam(value="WorkflowID") @PathParam("workflowId") Integer workflowId);

	@POST
	@Path (COMMON_PATH_AUTORIZACIONES_MAIL_DATA)
	@ApiOperation(tags = "Autorizaciones", value="Trae los datos del mail del prestador", notes ="Trae los datos del mail del prestador", httpMethod= POST_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	@Consumes ({APPLICATION_JSON, APPLICATION_XML})
	TResponse getMailDataPrestador(@ApiParam(value="Mail") HashMap<String, Object> body);

	
	@GET
	@Path (PATH_EXPEDIENTES_AUTORIZACION)
	@ApiOperation(tags = "Autorizaciones", value="Trae los expedientes de autorizacion por sucursal o ID Autorizacion", notes ="Trae los expedientes de autorizacion pro sucursal o ID Autorizacion", httpMethod= GET_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	TResponse expedientesAutorizacion(@ApiParam(value="ID Sucursal") @PathParam("sucur") Integer sucur, 
			@ApiParam(value="ID Autorizacion") @PathParam("auto") Integer auto);

	@POST
	@Path (PATH_VALIDA_TOPES_PRESTACION)
	@ApiOperation(tags = "Autorizaciones", value="Valida los topes de prestacion", notes ="Valida los topes de prestacion", httpMethod= POST_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	TResponse validaTopesPrestacion(@ApiParam(value="Cuerpo")  HashMap<String, Object> body);
	
	
	@GET
	@Path (PATH_MANDATARIA_BY_RUDI_ID)
	@ApiOperation(tags = "Autorizaciones", value="Trae la mandataria por rudi id", notes ="Trae la mandataria por rudi id", httpMethod= GET_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Produces ({APPLICATION_JSON, APPLICATION_XML})
	TResponse mandatariaByRudiId(@ApiParam(value="Rudi Id") @PathParam("rudiId") Integer rudiId);
}
