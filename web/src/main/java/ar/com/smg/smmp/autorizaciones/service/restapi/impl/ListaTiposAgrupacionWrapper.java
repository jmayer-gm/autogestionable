package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.tipo.TipoAgrupacion;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "TiposAgrupacion")
public class ListaTiposAgrupacionWrapper {

	@XmlElement(required = true, name = "tipoAgrupacion")
	private  List<TipoAgrupacion> tiposAgrupacion = new ArrayList<TipoAgrupacion>();

	public List<TipoAgrupacion> getTiposAgrupacion() {
		return tiposAgrupacion;
	}

	public void setTiposAgrupacion(List<TipoAgrupacion> lista) {
		this.tiposAgrupacion = lista;
	}

}
