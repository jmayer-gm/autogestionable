package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.tipo.TipoResolucion;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "TiposResolucion")
public class ListaTiposResolucionWrapper {

	@XmlElement(required = true, name = "tiposResolucion")
	private  List<TipoResolucion> tiposResolucion = new ArrayList<TipoResolucion>();

	public List<TipoResolucion> getTiposResolucion() {
		return tiposResolucion;
	}

	public void setTiposResolucion(List<TipoResolucion> lista) {
		this.tiposResolucion = lista;
	}
	
}
