package ar.com.smg.smmp.autorizaciones.service.restapi;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.io.Serializable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

public interface Productos<TResponse> extends Serializable {

	String APPLICATION_JSON = "application/json";
	String APPLICATION_XML = "application/xml";

	static final int STATUS_CODE_OK = 200;
	static final int STATUS_CODE_BAD_REQUEST = 400;
	static final int STATUS_CODE_NOT_FOUND = 404;
	static final int STATUS_CODE_INTERNAL_SERVER_ERROR = 500;
	static final int STATUS_CODE_SERVICE_UNAVAILABLE = 503;

	static final String STATUS_MSG_OK = "OK";
	static final String STATUS_MSG_BAD_REQUEST = "Bad Request";
	static final String STATUS_MSG_NOT_FOUND = "Not Found";
	static final String STATUS_MSG_INTERNAL_SERVER_ERROR = "Internal Server Error";
	static final String STATUS_MSG_SERVICE_UNAVAILABLE = "Service Unavailable";

	static final String GET_METHOD = "GET";
	static final String PUT_METHOD = "PUT";
	static final String PATCH_METHOD = "PATCH";
	static final String POST_METHOD = "POST";
	static final String DELETE_METHOD = "DELETE";

	/**
	 * Buscador de drogas
	 * 
	 * @param denoDroga
	 */
	
	@GET
	@ApiOperation(tags = "Productos", value = "monodroga", notes = "Trae drogas", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/productos/drogas")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response monodroga ( @ApiParam(value = "Producto") @QueryParam("denoDroga") String denoDroga);
	
	/**
	 * Buscador de productos
	 * 
	 * @param productoDeno
	 * @param troquel
	 */

	@GET
	@ApiOperation(tags = "Productos", value = "productosSearch", notes = "Trae productos", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR),
			@ApiResponse(code = STATUS_CODE_BAD_REQUEST, message = STATUS_MSG_BAD_REQUEST) })
	@Path("/productos/search")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response productosSearch(
			@ApiParam(value = "Producto") @QueryParam("productoDeno") String productoDeno,
			@ApiParam(value = "Troquel") @QueryParam("troquel") String troquel);
	
	/**
	 * Buscador de productos en presmed - difiere del anterior por traer cobertura de solped
	 * 
	 * @param productoCodigo
	 */

	@GET
	@ApiOperation(tags = "Productos", value = "productosPresmedSearch", notes = "Trae productos de Presmed", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR),
			@ApiResponse(code = STATUS_CODE_BAD_REQUEST, message = STATUS_MSG_BAD_REQUEST) })
	@Path("/productos/searchPresmed")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response productosPresmedSearch(
			@ApiParam(value = "ProductoCodigo") @QueryParam("productoCodigo") String productoCodigo);
	

	/**
	 * Buscador de productos por droga
	 * 
	 * @param droga
	 */
	
	@GET
	@ApiOperation(tags = "Productos", value = "productosByDroga", notes = "Trae productos por droga", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/productos/productos-droga")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response productosByDroga( @ApiParam(value = "Droga") @QueryParam("droga") String droga);
	
	
	/**
	 * Buscador de producto por codigo
	 * 
	 * @param produ
	 */
	@GET
	@ApiOperation(tags = "Productos", value = "ObtenerProductosFarmaciaAutorizacionesSGI", notes = "Trae productos de farmacia de la Autorizacion", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/{sucur}/{auto}/productos")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response obtenerProductosFarmaciaAutorizacionesSGI(
			@ApiParam(value = "Sucursal") @PathParam("sucur") String sucur,
			@ApiParam(value = "Autorizacion") @PathParam("auto") String auto);

	
	/**
	 * Trae productos de farmacia de la Autorizacion
	 * 
	 * @param autorizaciones
	 */
	@GET
	@ApiOperation(tags = "Producto", value = "productoByCodigo", notes = "Trae producto por codigo", httpMethod = GET_METHOD)
	@ApiResponses(value = {
			@ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR) })
	@Path("/{produ}/producto-codigo")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	Response productoByCodigo(
			@ApiParam(value = "Codigo") @PathParam("produ") String produ);


}
