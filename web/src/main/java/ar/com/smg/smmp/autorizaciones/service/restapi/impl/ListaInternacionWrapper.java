package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.autorizacion.Internacion;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Internaciones")
public class ListaInternacionWrapper {

	@XmlElement(required = true, name = "internaciones")
	private List<Internacion> internaciones = new ArrayList<Internacion>();

	public List<Internacion> getInternaciones() {
		return internaciones;
	}

	public void setInternaciones(List<Internacion> internaciones) {
		this.internaciones = internaciones;
	}

}
