package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import ar.com.smg.smmp.autorizaciones.service.interfaces.AutorizacionService;
import ar.com.smg.smmp.autorizaciones.service.interfaces.SolicitudAutorizacionService;
import ar.com.smg.smmp.autorizaciones.service.restapi.AutorizacionRest;
import ar.com.smg.smmp.model.autorizaciones.common.DatosEmail;
import ar.com.smg.smmp.model.autorizaciones.gestion.AutorizacionNormalizado;

@Path("/rest-api")
public class AutorizacionRestImpl implements AutorizacionRest<Response> {

	private static final long serialVersionUID = -771927016280008047L;

	private static final Logger LOGGER = Logger.getLogger(AutorizacionRestImpl.class);

	private static final String CANT_REG_POR_PAG_DEFAULT = "40";
	private static final String PAGINA_DEFAULT = "1";
	private static final String ERROR_PRESTADORID = "El parametro dias no puede ser nulo ni cero";
	private static final String ERROR_DIAS = "El parametro dias no puede ser nulo ni cero";
	
	private transient SolicitudAutorizacionService ejbService;
	private transient AutorizacionService ejbAutorizacionService;

	public AutorizacionRestImpl(@Context ServletContext servletContext) {
		super();
		this.loadResources(servletContext);
	}
	
	/*
	 * Trae la autorizacion por sucursal y ID 
	 * 
	 * Store: prs_autorizacion_crm @ksucur, @kauto
	 * 
	 * NO TRAE EL LLAMADO DE AUTORIZACION
	 */
	public Response traerAutorizacionXSucursalYId(String sucursalId, String autorizacionId){
		try {			
			AutorizacionNormalizado autoriz = this.ejbAutorizacionService.traerAutorizacionXSucursalYId(Integer.decode(sucursalId), Integer.decode(autorizacionId));
			LOGGER.debug("Autorizacion: " + autoriz);
						
			return Response.status(Response.Status.OK).entity(autoriz).build();
		}
		catch (NumberFormatException nfe){
			LOGGER.error(nfe);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(nfe).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response recoverAutoByWorkflowId(Integer workflowId) {
		try {			
			Map<String, Object> autoriz = this.ejbAutorizacionService.recoverAutoByWorkflowId(workflowId);
			LOGGER.debug("Autorizacion: " + autoriz);
						
			return Response.status(Response.Status.OK).entity(autoriz).build();
		}
		catch (NumberFormatException nfe){
			LOGGER.error(nfe);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(nfe).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	public Response obtenerLogsAutorizacionXSucursalYId(String sucursalId, String autorizacionId){
		try {			
			List<Map<String, Object>> logsAuditoria = this.ejbAutorizacionService.obtenerLogsAutorizacionXSucursalYId(Integer.decode(sucursalId), Integer.decode(autorizacionId));
			LOGGER.debug("Logs Auditoria: " + logsAuditoria);
						
			return Response.status(Response.Status.OK).entity(logsAuditoria).build();
		}
		catch (NumberFormatException nfe){
			LOGGER.error(nfe);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(nfe).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	private void loadResources(ServletContext servletContext) {
		// Spring application context.
		ApplicationContext appCtx = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
		// Get the EJB Service.
		this.ejbService = (SolicitudAutorizacionService) appCtx.getBean("solicitudAutorizacionEJBService");
		this.ejbAutorizacionService = (AutorizacionService) appCtx.getBean("autorizacionEJBService");
	}

	@Override
	public Response getMailDataPrestador(HashMap<String, Object> body) {
		try {
			DatosEmail datosEmail = this.ejbAutorizacionService.getMailDataPrestador(body);

			return Response.status(Response.Status.OK).entity(datosEmail.getDatosEmail()).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response expedientesAutorizacion(Integer sucur, Integer auto) {
		try {
			Map<String, Object> result = this.ejbAutorizacionService.expedientesAutorizacion(sucur, auto);
			return Response.status(Response.Status.OK).entity(result).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response validaTopesPrestacion(HashMap<String, Object> body) {
		try {
			Map<String, Object> result = this.ejbAutorizacionService.validaTopesPrestacion(body);
			return Response.status(Response.Status.OK).entity(result).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response mandatariaByRudiId(Integer rudiId) {
		try {
			Map<String, Object> result = this.ejbAutorizacionService.mandatariaByRudiId(rudiId);
			return Response.status(Response.Status.OK).entity(result).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}


}
