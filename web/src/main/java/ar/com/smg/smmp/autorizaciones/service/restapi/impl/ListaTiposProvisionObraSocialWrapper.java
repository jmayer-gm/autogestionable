package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.tipo.TipoProvisionObraSocial;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "TiposProvisionObraSocial")
public class ListaTiposProvisionObraSocialWrapper {

	@XmlElement(required = true, name = "tipoProvisionObraSocial")
	private  List<TipoProvisionObraSocial> tiposProvisionObraSocial = new ArrayList<TipoProvisionObraSocial>();

	public List<TipoProvisionObraSocial> getTiposProvisionObraSocial() {
		return tiposProvisionObraSocial;
	}

	public void setTiposProvisionObraSocial(List<TipoProvisionObraSocial> lista) {
		this.tiposProvisionObraSocial = lista;
	}

}
