package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import ar.com.smg.smmp.autorizaciones.service.interfaces.AutogestionService;
import ar.com.smg.smmp.autorizaciones.service.interfaces.AutorizacionService;
import ar.com.smg.smmp.autorizaciones.service.interfaces.SolicitudAutorizacionService;
import ar.com.smg.smmp.autorizaciones.service.restapi.SolicitudAutorizacionRest;
import ar.com.smg.smmp.autorizaciones.service.restapi.impl.ListaAutorizacionPorPrestadorWrapper;
import ar.com.smg.smmp.autorizaciones.service.restapi.impl.ListaAutorizacionPorAfiliadoEfectorWrapper;
import ar.com.smg.smmp.core.SMMPRuntimeException;
import ar.com.smg.smmp.model.autorizaciones.autorizacion.Prorroga;
import ar.com.smg.smmp.model.autorizaciones.common.DatosEmail;
import ar.com.smg.smmp.model.autorizaciones.solicitud.SolicitudAutorizacion;
import ar.com.smg.smmp.model.prestadores.farmacia.Farmacia;

@Path("/rest-api")
public class SolicitudAutorizacionRestImpl implements SolicitudAutorizacionRest<Response> {

	private static final long serialVersionUID = -771927016280008047L;

	private static final Logger LOGGER = Logger.getLogger(SolicitudAutorizacionRestImpl.class);

	private static final String CANT_REG_POR_PAG_DEFAULT = "40";
	private static final String PAGINA_DEFAULT = "1";
	private static final String ERROR_PRESTADORID = "El parametro dias no puede ser nulo ni cero";
	private static final String ERROR_DIAS = "El parametro dias no puede ser nulo ni cero";
	private static final String TODAS = "todas";
	private static final String ODONTOLOGICAS = "odontologicas";
	private static final String MEDICAS = "medicas";
	
	private transient SolicitudAutorizacionService ejbService;
	private transient AutorizacionService ejbAutorizacionService;
	private transient AutogestionService ejbAutogestionService;

	public SolicitudAutorizacionRestImpl(@Context ServletContext servletContext) {
		super();
		this.loadResources(servletContext);
	}

	@Override
	public Response traerSolicitudesPendientes(Integer prepaga, String contra, String inte) {
		try {
			ListaSolicitudAutorizacionWrapper lista = new ListaSolicitudAutorizacionWrapper();
			lista.setSolicitudesAutorizacion(this.ejbService.traerSolicitudesPendientes(prepaga, contra, inte, null, null));

			for (SolicitudAutorizacion solicitud : lista.getSolicitudesAutorizacion()) {
				if (solicitud.getPrestador().getId() == null) {
					solicitud.setPrestador(null);
				}
			}
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerSolicitudByEtapaId(Integer businessKey, Integer etapaId) {
		try {
			SolicitudAutorizacionWrapper solicitudAutorizacion = new SolicitudAutorizacionWrapper();
			solicitudAutorizacion.setSolicitudAutorizacion(this.ejbService.traerSolicitudAutorizacion(etapaId));
			return Response.status(Response.Status.OK).entity(solicitudAutorizacion).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response validarProrroga(Integer sucursalId, Integer numeroAutorizacion, Integer prepaga, String contrato, String integrante) {
		try {
			ListaProrrogaWrapper lista = new ListaProrrogaWrapper();
			Prorroga prorroga = this.ejbService.validarProrroga(sucursalId, numeroAutorizacion, prepaga, contrato, integrante);
			List<Prorroga> prorrogas = new ArrayList<Prorroga>();
			prorrogas.add(prorroga);
			lista.setProrrogas(prorrogas);
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerAutorizacionesAsociadas(Integer sucursalId, Integer numeroAutorizacion) {
		try {
			ListaProrrogaWrapper lista = new ListaProrrogaWrapper();
			lista.setProrrogas(this.ejbService.traerAutorizacionesAsociadas(sucursalId, numeroAutorizacion));

			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerSalas(String bajaFecha) {
		try {
			
			Date bajaFechaDate = null;
			
			if (bajaFecha != null && !"".equals(bajaFecha)) {
				try {
					bajaFechaDate = new SimpleDateFormat("yyyy-MM-dd").parse(bajaFecha);
				}
				catch (ParseException e) {
					LOGGER.error("El formato de la fecha de baja debe ser yyyy-MM-dd ", e);
					return Response.status(Response.Status.BAD_REQUEST).build();
				}
			}
			
			
			ListaSalaWrapper lista = new ListaSalaWrapper();
			lista.setSalas(this.ejbService.traerSalas(bajaFechaDate));

			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerInternaciones(Integer sucursalId, Integer numeroAutorizacion) {
		try {
			ListaInternacionWrapper lista = new ListaInternacionWrapper();
			lista.setInternaciones(this.ejbService.traerInternaciones(sucursalId, numeroAutorizacion));

			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerDatosEmailSocio(Integer sucursalId, Integer numeroAutorizacion) {
		try {
			DatosEmail datosEmail = this.ejbAutorizacionService.traerDatosEmailAutorizacion(sucursalId, numeroAutorizacion);

			return Response.status(Response.Status.OK).entity(datosEmail.getDatosEmail()).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	private void loadResources(ServletContext servletContext) {
		// Spring application context.
		ApplicationContext appCtx = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
		// Get the EJB Service.
		this.ejbService = (SolicitudAutorizacionService) appCtx.getBean("solicitudAutorizacionEJBService");
		this.ejbAutorizacionService = (AutorizacionService) appCtx.getBean("autorizacionEJBService");
		this.ejbAutogestionService = (AutogestionService) appCtx.getBean("autogestionEJBService");
	}
	
	@Override
	public Response traerDocumentacionSituacionTerapeutica(Integer precarga_id) {
		try {
			ListaDocumentacionSituacionTerapeuticaWrapper lista = new ListaDocumentacionSituacionTerapeuticaWrapper();
			lista.setDocumentacionSituacionTerapeutica(this.ejbService.traerDocumentacionSituacionTerapeutica(precarga_id));
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerAutorizacionesPorPrestador(String fechaDesde, String fechaHasta, String prestadorId, String tipo, String estado, String dias, String pagina, String cantRegPorPag) {
		try {
				
			if (cantRegPorPag == null || cantRegPorPag.equals("") || cantRegPorPag.equals("0")){
				cantRegPorPag = CANT_REG_POR_PAG_DEFAULT;
			}
			if (pagina == null || pagina.equals("") || pagina.equals("0")) {
				pagina = PAGINA_DEFAULT; 
			}
			if (prestadorId == null || prestadorId.equals("") || prestadorId.equals("0")){
				return wrapAndBuildValidationMessageGenerada(ERROR_PRESTADORID);				
			}
			if (dias == null || dias.equals("") || dias.equals("0")) {
				return wrapAndBuildValidationMessageGenerada(ERROR_DIAS);				
			}

			Integer prestadorIdInteger = Integer.valueOf(prestadorId)  ;
			Integer diasInteger = Integer.valueOf(dias)  ;
			Integer cantRegPorPagInteger = Integer.valueOf(cantRegPorPag) ;
			Integer paginaInteger = Integer.valueOf(pagina)  ;

			ListaAutorizacionPorPrestadorWrapper lista = new ListaAutorizacionPorPrestadorWrapper();
			lista.setAutorizacionPorPrestador(this.ejbService.traerAutorizacionesPorPrestador(fechaDesde, fechaHasta, prestadorIdInteger, tipo, estado, diasInteger, paginaInteger, cantRegPorPagInteger));
			
			return Response.status(Response.Status.OK).entity(lista).build();
		} catch (RuntimeException e) {
			return wrapAndBuildValidationMessage(e);
		} catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response traerAutorizacionesPorAfiliado(Integer prepaga, String contra, String inte, String efectorId, String tieneVinculable, Integer sucur, Integer auto, Integer nomen, String prestacion, String fechaConsumo) {
		try {
			if (tieneVinculable != null) {
				if (efectorId != null || sucur != null || auto != null || nomen != null || prestacion != null || fechaConsumo != null) {
					return wrapAndBuildValidationMessageGenerada("Si se usa el parámetro efector, sucur, auto, nomen, prestacion o fechaConsumo no se puede usar el parámetro tieneVinculable");	
				}
				
				Map<String, Object> map = this.ejbService.tieneAutorizacionVinculable(prepaga, contra, inte);
				
				return Response.status(Response.Status.OK).entity(map).build();			

			} else {			
			
				if (efectorId != null && efectorId.trim().equals("")) {
					efectorId = null; 
				}
				
				ListaAutorizacionPorAfiliadoEfectorWrapper lista = new ListaAutorizacionPorAfiliadoEfectorWrapper();
				lista.setAutorizacionPorAfiliadoEfector(this.ejbService.traerAutorizacionesPorAfiliadoEfector(prepaga, contra, inte, efectorId, sucur, auto, nomen, prestacion, fechaConsumo));
				
				return Response.status(Response.Status.OK).entity(lista).build();
			}	
		} catch (RuntimeException e) {
			return wrapAndBuildValidationMessage(e);
		} catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}


	
	@Override
	public Response traerFarmacias(Integer manda, Integer id, String razon) {
		try {
			List<Farmacia> farmacias = this.ejbService.traerFarmacias(manda, id, razon);
			return Response.status(Response.Status.OK).entity(farmacias).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response traerFarmaciasByUbicacion(Integer manda, String provin, String loca, String razon) {
		try {
			List<Farmacia> farmacias = this.ejbService.traerFarmaciasByUbicacion(manda, provin, loca, razon);
			return Response.status(Response.Status.OK).entity(farmacias).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	private Response wrapAndBuildValidationMessage(RuntimeException re){
		if(re.getCause() != null && (re.getCause()  instanceof SMMPRuntimeException)) {
			SMMPRuntimeException smmpRE = (SMMPRuntimeException) re.getCause();
			Map	<String, Object> response = new HashMap<String, Object>();
			response.put("mensaje", smmpRE.getMostSpecificCause().getMessage());
			return Response
				.status(Response.Status.NOT_ACCEPTABLE)
				.entity(response)
				.build();
		}else{
			throw re;
		}
	}

	private Response wrapAndBuildValidationMessageGenerada(String mensaje){
		Map	<String, Object> response = new HashMap<String, Object>();
		response.put("mensaje", mensaje);
		return Response
			.status(Response.Status.NOT_ACCEPTABLE)
			.entity(response)
			.build();
	}
	
	@Override
	public Response traerEquiposXAutorizacion(String sucursalId,
			String autorizacionId) {
		try {
			
			List<Map<String,Object>> autorizaciones = this.ejbAutorizacionService.traeEquiposXAutorizacion(new Integer(sucursalId), new Integer(autorizacionId.trim()));
			return Response.status(Response.Status.OK).entity(autorizaciones).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerAtorizacionesConEqupipos(String tipo, String equipos) {
		try {
			List<Map<String,Object>> autorizaciones = this.ejbAutorizacionService.traeAutorizacionConEquipos();
			return Response.status(Response.Status.OK).entity(autorizaciones).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	
	@Override
	public Response traerModulosXAutorizacion(String sucursalId,
			String autorizacionId) {
		try {
			
			List<Map<String,Object>> autorizaciones = this.ejbAutorizacionService.traeModulosXAutorizacion(new Integer(sucursalId), new Integer(autorizacionId.trim()));
			return Response.status(Response.Status.OK).entity(autorizaciones).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerAutorizacionesAfiliadoPorTipo(String contra, Integer prepaga, String inte, String tipo, String fechaDesde, String fechaHasta) {
		try {
			if (tipo == null) {
				tipo = TODAS;
			}
			if (ODONTOLOGICAS.equals(tipo)) {
				List<Map<String,Object>> autorizaciones = this.ejbService.traerAutorizacionesOdontoAfiliado(contra, prepaga, inte, fechaDesde, fechaHasta);
				return Response.status(Response.Status.OK).entity(autorizaciones).build();
			}
			else if (MEDICAS.endsWith(tipo)) {
				throw new Exception("No se ha implementado el servicio para recuperar autorizaciones medicas");
			}
			else {//todas
				throw new Exception("No se ha implementado el servicio para recuperar todas las autorizaciones");
			}
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerDatosCabAutorizacionesOdo(Integer auto, Integer sucur) {
		try {
			Map<String,Object> cabecera = this.ejbService.traerDatosCabAutorizacionesOdo(auto, sucur);
			return Response.status(Response.Status.OK).entity(cabecera).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerPrestacionesAutorizacionesOdo(Integer auto, Integer sucur) {
		try {
			List<Map<String,Object>> prestaciones = this.ejbService.traerPrestacionesAutorizacionesOdo(auto, sucur);
			return Response.status(Response.Status.OK).entity(prestaciones).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response traerAutorizacionesPrestadorPorTipo(Integer codigoPrestador, String fechaDesde, String fechaHasta, String tipo) {
		try {
			if (ODONTOLOGICAS.equals(tipo)) {
				List<Map<String,Object>> autorizaciones = this.ejbService.traerAutorizacionesOdontoPrestador(codigoPrestador, fechaDesde, fechaHasta);
				return Response.status(Response.Status.OK).entity(autorizaciones).build();
			} else {
				throw new Exception("No se ha implementado el servicio para recuperar todas las autorizaciones");
			}
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response validarAutorizacionesSolapadas(HashMap<String, Object> params) {
		try {
			if(!this.validParams(params)){
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			List<Map<String, Object>> result = this.ejbService.validarAutorizacionesSolapadas(params);
			return Response.status(Response.Status.OK).entity(result).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response validarConvenio(HashMap<String, Object> params) {
		try {
			
			params.put("ciruGastos", null);
			params.put("urgenDiurna", null);
			params.put("urgenNoctur", null);
			params.put("vierPlus", null);
			params.put("valorUni", null);
			params.put("conve", null);
			params.put("instruPorce", null);
			params.put("instruValor", null);
			params.put("miniValor", null);
			params.put("globalValor", null);
			params.put("garanMini", null);
			params.put("erro", null);
			params.put("ciruInsumos", null);
			params.put("conveExcep", null);
			
			Map<String, Object> response = this.ejbService.validarConvenio(params);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response validarConvenioInter(HashMap<String, Object> params) {
		try {
			
			params.put("realValor", null);
			params.put("ciruEspe", null);
			params.put("ciruAyu", null);
			params.put("ciruGastos", null);
			params.put("ciruInsta", null);
			params.put("ciruInstl", null);
			params.put("ambuInter", null);
			params.put("carac", null);
			params.put("conve", null);
			params.put("dias", null);
			params.put("erro", null);
			params.put("conveExcep", null);
			
			Map<String, Object> response = this.ejbService.validarConvenioInter(params);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response validarConvenioCiru(HashMap<String, Object> params) {
		try {
			
			params.put("realValor", null);
			params.put("ciruEspe", null);
			params.put("ciruAyu", null);
			params.put("ciruGastos", null);
			params.put("ciruInsta", null);
			params.put("ciruInstl", null);
			params.put("urgenDiurna", null);
			params.put("urgenNoctur", null);
			params.put("vierPlus", null);
			params.put("valorUni", null); 
			params.put("conve", null);  	
			params.put("instruPorce", null);
			params.put("instruValor", null);
			params.put("miniValor", null);  
			params.put("globalValor", null);
			params.put("garanMini", null);  
			params.put("erro", null);  	
			params.put("ciruInsumos", null);
			params.put("conveExcep", null);
			
			Map<String, Object> response = this.ejbService.validarConvenioCiru(params);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response validarGeneroYEdad(HashMap<String, Object> params) {
		try {
			
			params.put("recha", null);
			params.put("descRecha", null);
			
			Map<String, Object> response = this.ejbService.validarGeneroYEdad(params);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response validarCarencia(HashMap<String, Object> params) {
		try {
			
			params.put("estado", null);
			params.put("observaciones", null);
			
			Map<String, Object> response = this.ejbService.validarCarencia(params);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traeCopagoPrestacion(HashMap<String, Object> params) {
		try {
			
			params.put("descri_copa", null);
			params.put("pago_copa", null);
			params.put("valor_copa", null);
			params.put("uni", null);
			
			Map<String, Object> response = this.ejbService.traeCopagoPrestacion(params);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response validarCobertura(HashMap<String, Object> params) {
		try {
			
			setParamsNull(params);
			Map<String, Object> response = this.ejbService.validarCobertura(params);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response validarCoberturaMadre(HashMap<String, Object> params) {
		try {
			setParamsNull(params);
			Map<String, Object> response = this.ejbService.validarCobertura(params);
			Map<String, Object> autogestionable = this.ejbAutogestionService.getAutogestionable(params);
			List<Map<String,Object>> coberturasMadre = obtenerCoberturasMadre(autogestionable);
			if(tieneCobertura(coberturasMadre)) {
				response = validoCoberturaMadre(coberturasMadre, params);
			}
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}


	private List<Map<String, Object>> obtenerCoberturasMadre(Map<String, Object> autogestionable) {
		if(autogestionable != null) {
			return (List<Map<String, Object>>) autogestionable.get("coberturas_madre");
		}
		return null;
	}

	private void setParamsNull(HashMap<String, Object> params) {
		params.put("cober", null);
		params.put("coberAuto", null);
		params.put("prescri", null);
		params.put("beneCate", null);
		params.put("grupo", null);
		params.put("topeDesde", null);
		params.put("indiGrupo", null);
		params.put("asoCalen", null);
		params.put("aniosCanti", null);
		params.put("tope", null);
		params.put("ajusFactor", null);
		params.put("erro", null);
	}
	

	private boolean tieneCobertura(List<Map<String, Object>> coberturasMadre) {
		if(coberturasMadre == null || coberturasMadre.isEmpty() )
			return false;
		return true;
	}
	
	private Map<String, Object> validoCoberturaMadre(List<Map<String, Object>> coberturasMadre, HashMap<String, Object> params) {
		Map<String, Object> response = new HashMap<>();
		for(Map<String, Object> c : coberturasMadre) {
			params.put("nomen", c.get("nomen_madre"));
			params.put("prestac", c.get("prestac_madre"));
			response = this.ejbService.validarCobertura(params);
			BigDecimal error = (BigDecimal) response.get("erro");
			if(error == null || error != BigDecimal.ZERO) {
				return response; 
			}
		}
		return response;
	}
	
	private boolean validacionExitosa(Map<String, Object> response) {
		if(response == null) 
			return false;

		Integer error = (Integer) response.get("erro");
		if(error == null || error != 0)
			return false;
		
		return true;
	}

	private boolean validParams(HashMap<String, Object> params) {
		return 
		params.get("prepaga") != null &&	
		params.get("contra") != null && 
		params.get("inte") != null &&
		params.get("interFecha") != null &&
		params.get("origen") != null;
	}
	
	@Override
	public Response traerHistorialValorLey(Integer nomen, String prestacion) {
		try {
			List<Map<String, Object>> result = this.ejbService.traerHistorialValorLey(nomen,prestacion);
			return Response.status(Response.Status.OK).entity(result).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerEstudiosTramite(Integer workflowId) {
		try {
			List<Map<String, Object>> result = this.ejbService.traerEstudiosTramite(workflowId);
			return Response.status(Response.Status.OK).entity(result).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response plnsConfigModulos(HashMap<String, Object> params) {
		try {
			List<Map<String, Object>> result = this.ejbService.plnsConfigModulos(params);
			return Response.status(Response.Status.OK).entity(result).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response traerClasificaciones(Integer sucursalId, Integer numeroAutorizacion) {
		try {
			List<Map<String, Object>> result = this.ejbService.traerClasificaciones(sucursalId,numeroAutorizacion);
			return Response.status(Response.Status.OK).entity(result).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traePrestacionesAutorizacion(Integer sucursalId, Integer numeroAutorizacion) {
		try {
			List<Map<String, Object>> result = this.ejbService.traePrestacionesAutorizacion(sucursalId, numeroAutorizacion);
			return Response.status(Response.Status.OK).entity(result).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response traerDescripcionRecha(Integer recha, Integer manual) {
		try {
			List<Map<String, Object>> result = this.ejbService.traerDescripcionRecha(recha,manual);
			return Response.status(Response.Status.OK).entity(result).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public void auditarAutorizacion(Integer sucursal, Integer numeroAutorizacion, String actividad, String usuario) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sucur", sucursal);
		map.put("auto", numeroAutorizacion);
		map.put("fecha_hora", new Date());
		map.put("activi", actividad);
		map.put("usuario", usuario);	
		try {
			this.ejbService.auditarAutorizacion(map);
		}
		catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	@Override
	public Response obtenerProceDtoProducto(HashMap<String, Object> params) {
		try {
			Map<String, Object> response = this.ejbService.obtenerProceDtoProducto(params);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response validarProducto(HashMap<String, Object> params) {
		try {
			Map<String, Object> response = this.ejbService.validarProducto(params);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public void registraEnvioAFarmalink(Integer auto, Integer sucur, String produ, Integer envio, String usuario) {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("auto", auto);
			map.put("sucur", sucur);
			map.put("produ", produ);
			map.put("envio", envio);
			map.put("usuario", usuario);
			
			this.ejbService.registraEnvioAFarmalink(map);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Response traerRazonesOdo() {
		List<Map<String, Object>> result = this.ejbService.traerRazonesOdo();
		return Response.status(Response.Status.OK).entity(result).build();
	}

	@Override
	public Response traerMotivosOdo(Integer presupAprobado) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("presupAprobado", presupAprobado);
		List<Map<String, Object>> result = this.ejbService.traerMotivosOdo(map);
		return Response.status(Response.Status.OK).entity(result).build();
	}
	
	@Override
	public Response obtenerConversionProductoSap(HashMap<String, Object> params) {
		try {
			Map<String, Object> response = this.ejbService.obtenerConversionProductoSap(params);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
}
