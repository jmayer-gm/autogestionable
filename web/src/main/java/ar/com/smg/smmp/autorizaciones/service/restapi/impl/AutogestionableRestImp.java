package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import ar.com.smg.smmp.autorizaciones.service.interfaces.AutogestionService;
import ar.com.smg.smmp.autorizaciones.service.interfaces.AutorizacionService;
import ar.com.smg.smmp.autorizaciones.service.interfaces.SolicitudAutorizacionService;
import ar.com.smg.smmp.autorizaciones.service.restapi.AutogestionableRest;

@Path("/rest-api")
public class AutogestionableRestImp implements AutogestionableRest<Response> {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(AutorizacionRestImpl.class);
	private static final String MSG_NOT_FOUND = "Recurso inexistente";
	
	private transient AutogestionService ejbAutogestionService;
	private transient SolicitudAutorizacionService ejbService;
	private transient AutorizacionService ejbAutorizacionService;

	public AutogestionableRestImp(@Context ServletContext servletContext) {
		super();
		this.loadResources(servletContext);
	}

	@Override
	public Response traerAutogestionables(Integer nomenclador, String prestacion) {
		try {
			Map<String, Object> map = new HashMap<>();
			map.put("nomen", nomenclador);
			map.put("prestac", prestacion); 
			Map<String, Object> autoge = this.ejbAutogestionService.getAutogestionable(map);
			if (autoge == null)
				return createErrorResponse(MSG_NOT_FOUND, Response.Status.NOT_FOUND);
			LOGGER.debug("Autogestion: " + autoge);
			return Response.status(Response.Status.OK).entity(autoge).build();
		} catch (NumberFormatException nfe) {
			LOGGER.error(nfe);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(nfe).build();
		} catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	private void loadResources(ServletContext servletContext) {
		// Spring application context.
		ApplicationContext appCtx = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
		// Get the EJB Service.
		this.ejbService = (SolicitudAutorizacionService) appCtx.getBean("solicitudAutorizacionEJBService");
		this.ejbAutogestionService = (AutogestionService) appCtx.getBean("autogestionEJBService");
	}

	private Response createErrorResponse(String message, Response.Status status) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("message", "Error no esperado");
		result.put("error", message);
		return Response.status(status).entity(result).build();
	}

	@Override
	public Response traerTramite(HashMap<String, Object> body, String tramite) {
		try {
			Map<String, Object> tram = this.ejbAutogestionService.getTramite(body, tramite);
			LOGGER.debug("Autogestion: " + tram);
			if (tram == null)
				return createErrorResponse(MSG_NOT_FOUND, Response.Status.NOT_FOUND);
			return Response.status(Response.Status.OK).entity(tram).build();
		} catch (NumberFormatException nfe) {
			LOGGER.error(nfe);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(nfe).build();
		} catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response traerTodoAutogestionables() {
		try {
			List<Map<String, Object>> autoge = this.ejbAutogestionService.getAutogestionables();
			LOGGER.debug("Autogestion: " + autoge);
			if (autoge == null)
				return createErrorResponse(MSG_NOT_FOUND, Response.Status.NOT_FOUND);
			Map<String, Object> map = new HashMap<>();
			List<Map<String, Object>> diagnosticos = this.ejbAutogestionService.getDiagnosticos(map);
			List<Map<String, Object>> coberturaMadre = this.ejbAutogestionService.getCoberturaMadre(map);
			addDiag(autoge, diagnosticos);
			addCoberturas(autoge,coberturaMadre);
			return Response.status(Response.Status.OK).entity(autoge).build();
		} catch (NumberFormatException nfe) {
			LOGGER.error(nfe);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(nfe).build();
		} catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}


	private List<Map<String, Object>> addDiag(List<Map<String, Object>> autoge,
			List<Map<String, Object>> diagnosticos) {
		for (Map<String, Object> o : autoge) {
			List<Map<String, Object>> diagResult = new ArrayList<>();

			for (Map<String, Object> d : diagnosticos) {
				String prestacAuto = (String) o.get("prestac");
				String prestacDiag = (String) d.get("prestac");
				if (o.get("nomen") == d.get("nomen") && prestacAuto.trim().equals(prestacDiag.trim()))
					diagResult.add(createDiagObject(d));
			}
			o.put("icds_asoc", diagResult);
		}
		return autoge;
	}
	
	private List<Map<String, Object>> addCoberturas(List<Map<String, Object>> autoge, List<Map<String, Object>> coberturaMadre) {
		for (Map<String, Object> o : autoge) {
			List<Map<String, Object>> cobResult = new ArrayList<>();

			for (Map<String, Object> c : coberturaMadre) {
				String prestacAuto = (String) o.get("prestac");
				String prestacDiag = (String) c.get("prestac");
				if (o.get("nomen") == c.get("nomen") && prestacAuto.trim().equals(prestacDiag.trim()))
					cobResult.add(c);
			}
			o.put("coberturas_madre", cobResult);
		}
		return autoge;
		
	}


	private Map<String, Object> createDiagObject(Map<String, Object> d) {
		Map<String, Object> result = new HashMap<>();
		String desc = (String) d.get("sinonimo");
		if (desc == null)
			desc = (String) d.get("deno");
		result.put("icd", d.get("icd"));
		result.put("sinonimo", desc);
		return result;

	}

}
