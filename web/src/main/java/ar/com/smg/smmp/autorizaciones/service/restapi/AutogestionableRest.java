package ar.com.smg.smmp.autorizaciones.service.restapi;

import java.io.Serializable;
import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

public interface AutogestionableRest<TResponse> extends Serializable {
	String APPLICATION_JSON = "application/json";
	String APPLICATION_XML = "application/xml";

	String NOMEN = "nomenclador";
	String PRESTAC = "prestacion";
	String TRAMITE = "tramite";
	String COMMON_PATH_AUTOGESTIONABLE = "/autogestionables/{" + NOMEN + "}/{" + PRESTAC + "}";
	String COMMON_PATH_AUTOGESTIONABLE_TODAS = "/autogestionables";
	String COMMON_PATH_AUTOGESTIONABLE_ESTADO = "/gestion-autorizaciones/{" + TRAMITE + "}/estado";


	// Constantes para Swagger Annotations
	static final int STATUS_CODE_OK = 200;
	static final int STATUS_CODE_BAD_REQUEST = 400;
	static final int STATUS_CODE_NOT_FOUND = 404;
	static final int STATUS_CODE_INTERNAL_SERVER_ERROR = 500;
	static final int STATUS_CODE_SERVICE_UNAVAILABLE = 503;

	static final String STATUS_MSG_OK = "OK";
	static final String STATUS_MSG_BAD_REQUEST = "Bad Request";
	static final String STATUS_MSG_NOT_FOUND = "Not Found";
	static final String STATUS_MSG_INTERNAL_SERVER_ERROR = "Internal Server Error";
	static final String STATUS_MSG_SERVICE_UNAVAILABLE = "Service Unavailable";

	static final String GET_METHOD = "GET";
	static final String PUT_METHOD = "PUT";
	static final String PATCH_METHOD = "PATCH";
	static final String POST_METHOD = "POST";
	static final String DELETE_METHOD = "DELETE";

	@GET
	@ApiOperation(tags = "autogestion", value = "GetTiposGestion", notes = "Trae los autogestionados", httpMethod = GET_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR),
			@ApiResponse(code = STATUS_CODE_NOT_FOUND, message = STATUS_MSG_NOT_FOUND) })
	@Path(COMMON_PATH_AUTOGESTIONABLE)
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse traerAutogestionables(@ApiParam(value = "Nomenclador") @PathParam(NOMEN) Integer nomen,
			@ApiParam(value = "Prestacion") @PathParam(PRESTAC) String prestacion);
	
	@GET
	@ApiOperation(tags = "autogestion", value = "GetTiposGestion", notes = "Trae los autogestionados", httpMethod = GET_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR),
			@ApiResponse(code = STATUS_CODE_NOT_FOUND, message = STATUS_MSG_NOT_FOUND) })
	@Path(COMMON_PATH_AUTOGESTIONABLE_TODAS)
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse traerTodoAutogestionables();
	
	@POST
	@ApiOperation(tags = "autogestion", value = "GetTiposGestion", notes = "Trae los autogestionados", httpMethod = GET_METHOD)
	@ApiResponses(value = { @ApiResponse(code = STATUS_CODE_OK, message = STATUS_MSG_OK, response = Response.class),
			@ApiResponse(code = STATUS_CODE_INTERNAL_SERVER_ERROR, message = STATUS_MSG_INTERNAL_SERVER_ERROR),
			@ApiResponse(code = STATUS_CODE_NOT_FOUND, message = STATUS_MSG_NOT_FOUND) })
	@Path(COMMON_PATH_AUTOGESTIONABLE_ESTADO)
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	TResponse traerTramite(@ApiParam(value="Cuerpo")  HashMap<String, Object> body, 
			@ApiParam(value = "Prestacion") @PathParam(TRAMITE) String tramite);

	

}
