package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import ar.com.smg.smmp.autorizaciones.service.interfaces.ProductosServiceLocal;
import ar.com.smg.smmp.autorizaciones.service.restapi.Productos;

@Path("/rest-api")
public class ProductosImpl implements Productos<Response> {

	private static final long serialVersionUID = 4652764409860365939L;

	private static final Logger LOGGER = Logger.getLogger(ProductosImpl.class);

	private transient ProductosServiceLocal productosEJBService;

	public ProductosImpl(@Context ServletContext servletContext) {
		super();
		this.loadResources(servletContext);
	}

	@Override
	public Response monodroga(String denoDroga) {
		List<Map<String,Object>> drogas = null;
		try {
			drogas = this.productosEJBService.monodroga(denoDroga);
			return  Response.status(Response.Status.OK).entity(drogas).build();
		}
		catch	(Exception e){
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response productosSearch(String productoDeno, String troquel) {
		List<Map<String,Object>> productos = null;
		try {
			if (productoDeno != null && productoDeno.trim().isEmpty()) {
				productoDeno = null;
			}
			if (troquel != null && troquel.trim().isEmpty()) {
				troquel = null;
			}
			
			if (productoDeno == null && troquel == null) {
				LOGGER.error("Debe ingresar algun criterio de busqueda");
				return Response.status(Response.Status.BAD_REQUEST).entity("Debe ingresar algun criterio de busqueda").build();
			}
			
			productos = this.productosEJBService.productosSearch(productoDeno, troquel);
			return  Response.status(Response.Status.OK).entity(productos).build();
		}
		catch	(Exception e){
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response productosPresmedSearch(String productoCodigo) {
		List<Map<String,Object>> productos = null;
		try {
			if (productoCodigo != null && productoCodigo.trim().isEmpty()) {
				productoCodigo = null;
			}
			
			if (productoCodigo == null) {
				LOGGER.error("Debe ingresar algun producto de busqueda");
				return Response.status(Response.Status.BAD_REQUEST).entity("Debe ingresar algun criterio de busqueda").build();
			}
			
			productos = this.productosEJBService.productosPresmedSearch(productoCodigo);
			return  Response.status(Response.Status.OK).entity(productos).build();
		}
		catch	(Exception e){
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response productosByDroga(String droga) {
		List<Map<String,Object>> productos = null;
		try {
			productos = this.productosEJBService.productosByDroga(droga);
			return  Response.status(Response.Status.OK).entity(productos).build();
		}
		catch	(Exception e){
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response obtenerProductosFarmaciaAutorizacionesSGI(String sucur, String auto) {
		List<Map<String,Object>> productosFarmacia = null;
		try{
			productosFarmacia = this.productosEJBService.obtenerProductosFarmaciaAutorizacionesSGI(Integer.decode(sucur), Integer.decode(auto));
			return  Response.status(Response.Status.OK).entity(productosFarmacia).build();
		}
		catch	(Exception e){
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response productoByCodigo(String produ) {
		Map<String,Object> producto = null;
		try{
			producto = this.productosEJBService.productoByCodigo(produ);
			return  Response.status(Response.Status.OK).entity(producto).build();
		}
		catch	(Exception e){
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	private void loadResources(ServletContext servletContext) {
		// Spring application context.
		ApplicationContext appCtx = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

		// Get the EJB Service.
		productosEJBService = (ProductosServiceLocal) appCtx.getBean("productosEJBService");
	}

}