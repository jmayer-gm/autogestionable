package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.smg.smmp.model.autorizaciones.tipo.MotivoFinalizacion;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MotivosFinalizacion")
public class ListaMotivosFinalizacionWrapper {

	@XmlElement(required = true, name = "motivosFinalizacion")
	private  List<MotivoFinalizacion> motivosFinalizacion = new ArrayList<MotivoFinalizacion>();

	public List<MotivoFinalizacion> getMotivosFinalizacion() {
		return motivosFinalizacion;
	}

	public void setMotivosFinalizacion(List<MotivoFinalizacion> lista) {
		this.motivosFinalizacion = lista;
	}
	
}
