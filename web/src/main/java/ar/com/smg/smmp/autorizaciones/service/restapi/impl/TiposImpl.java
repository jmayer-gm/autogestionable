package ar.com.smg.smmp.autorizaciones.service.restapi.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import ar.com.smg.smmp.autorizaciones.service.interfaces.TiposServiceLocal;
import ar.com.smg.smmp.autorizaciones.service.restapi.Tipos;

@Path("/rest-api")
public class TiposImpl implements Tipos<Response> {

	private static final long serialVersionUID = 8617466696466649249L;

	private static final Logger LOGGER = Logger.getLogger(TiposImpl.class);

	private transient TiposServiceLocal tiposEJBService;

	public TiposImpl(@Context ServletContext servletContext) {
		super();
		this.loadResources(servletContext);
	}

	@Override
	public Response getTiposAutorizacion() {
		try {
			ListaTiposAutorizacionWrapper lista = new ListaTiposAutorizacionWrapper();
			lista.setTiposAutorizacion(this.tiposEJBService.getTiposAutorizaciones());
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}

	}

	@Override
	public Response getTiposGestion() {
		try {
			ListaTiposGestionWrapper lista = new ListaTiposGestionWrapper();
			lista.setTiposGestion(this.tiposEJBService.getTiposGestion());
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response getTiposPaciente() {
		try {
			ListaTiposPacienteWrapper lista = new ListaTiposPacienteWrapper();
			lista.setTiposPaciente(this.tiposEJBService.getTiposPaciente());
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response getTiposProvisionObraSocial() {
		try {
			ListaTiposProvisionObraSocialWrapper lista = new ListaTiposProvisionObraSocialWrapper();
			lista.setTiposProvisionObraSocial(this.tiposEJBService.getTiposProvisionObraSocial());
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response getTiposAgrupacion() {
		try {
			ListaTiposAgrupacionWrapper lista = new ListaTiposAgrupacionWrapper();
			lista.setTiposAgrupacion(this.tiposEJBService.getTiposAgrupacion());
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response getMotivosExcepcion() {
		try {
			ListaMotivosExcepcionWrapper lista = new ListaMotivosExcepcionWrapper();
			lista.setMotivosExcepcion(this.tiposEJBService.getMotivosExcepcion());
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response getTiposResolucion() {
		try {
			ListaTiposResolucionWrapper lista = new ListaTiposResolucionWrapper();
			lista.setTiposResolucion(this.tiposEJBService.getTiposResolucion());
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response getTiposResolucionST() {
		try {
			ListaTiposResolucionWrapper lista = new ListaTiposResolucionWrapper();
			lista.setTiposResolucion(this.tiposEJBService.getTiposResolucionST());
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response getMotivosRechazo() {
		try {
			ListaMotivosRechazoWrapper lista = new ListaMotivosRechazoWrapper();
			lista.setMotivosRechazo(this.tiposEJBService.getMotivosRechazo());
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@Override
	public Response getMotivosFinalizacion(@PathParam("tarea") String tarea) {
		try {
			ListaMotivosFinalizacionWrapper lista = new ListaMotivosFinalizacionWrapper();
			lista.setMotivosFinalizacion(this.tiposEJBService.getMotivosFinalizacion(tarea));
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response getTiposPracticaOdo() {
		try {
			List<Map<String,Object>> lista = null;
			lista = this.tiposEJBService.getTiposPracticaOdo();
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response getMotivosFinalizacionOdo(@QueryParam("codigo") Integer codigo, @QueryParam("grupo") Integer grupo) {
		try {
			List<Map<String,Object>> lista = null;
			lista = this.tiposEJBService.getMotivosFinalizacionOdo(codigo, grupo);
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response getResolucionesOdo(@QueryParam("codigo") Integer codigo) {
		try {
			List<Map<String,Object>> lista = null;
			lista = this.tiposEJBService.getResolucionesOdo(codigo);
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response traeMotivosNoReprogramacionCirugia() {
		try {
			List<Map<String,Object>> lista = null;
			lista = this.tiposEJBService.traeMotivosNoReprogramacionCirugia();
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response traeEstudiosAdjuntosMdc(@QueryParam("fecha") Long fecha) {
		try {
			List<Map<String,Object>> lista = null;
			lista = this.tiposEJBService.traeEstudiosAdjuntosMdc(fecha);
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response traeObrasSocialesMdc() {
		try {
			List<Map<String,Object>> lista = null;
			lista = this.tiposEJBService.traeObrasSocialesMdc();
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response estadoAutorizacionesSGI() {
		try {
			List<Map<String,Object>> lista = this.tiposEJBService.estadoAutorizacionesSGI();
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response clasificacionAutorizacionesSGI() {
		try {
			List<Map<String, Object>> lista = this.tiposEJBService.clasificacionAutorizacionesSGI();
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response obtenerMandatariasAutorizacionesSGI(String autorizaciones) {
		try {
			List<Map<String, Object>> lista = this.tiposEJBService.obtenerMandatariasAutorizacionesSGI(autorizaciones);
			return Response.status(Response.Status.OK).entity(lista).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response obtenerEstudiosPorTipoAutorizacion(Long fecha, String tipoAutorizacion) {
		try {
			List<Map<String,Object>> response = null;
			response = this.tiposEJBService.obtenerEstudiosPorTipoAutorizacionSGI(fecha, tipoAutorizacion);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	@Override
	public Response getSubmotivosFinalizacion(String tarea) {
		try {
			List<Map<String,Object>> response = null;
			response = this.tiposEJBService.getSubmotivosFinalizacion(tarea);
			return Response.status(Response.Status.OK).entity(response).build();
		}
		catch (Exception e) {
			LOGGER.error(e);
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	private void loadResources(ServletContext servletContext) {
		// Spring application context.
		ApplicationContext appCtx = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

		// Get the EJB Service.
		tiposEJBService = (TiposServiceLocal) appCtx.getBean("tiposEJBService");
	}

}