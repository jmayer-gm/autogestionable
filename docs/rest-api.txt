
#######################################################################################################################
   REST API entidades-negocio
   
   Para todos los metodos:
   
   Si falla del lado del Server:
   HTTP STATUS: 500 INTERNAL SERVER ERROR (y la exception en el Payload)
   
   Si los parametros provistos son incorrectos:
   HTTP STATUS: 400 BAD REQUEST (y la explicacion en el Payload)
   
   
#######################################################################################################################


## Listar TODAS las entidades-negocio existentes:

GET http://localhost:8080/ws.entidades-negocio/api/entidades-negocio/
Accept: application/{json, xml}

HTTP STATUS: 
   200 OK
   Si no hay entidades a mostrar, devuelve 200 OK con una lista vacia

-----------------------------------------------------------------------------------------------------------------------


## Listar TODAS las entidades-negocio existentes filtradas por descripcion y/o prioridad

GET http://localhost:8080/ws.entidades-negocio/api/entidades-negocio?descripcion=lalala&prioridad=27
Accept: application/{json, xml}

HTTP STATUS: 
   200 OK
   Si no hay entidades a mostrar, devuelve 200 OK con una lista vacia

-----------------------------------------------------------------------------------------------------------------------


## OBTENER LA entidad-negocio por ID (PK)

GET http://localhost:8080/ws.entidades-negocio/api/entidades-negocio/{id}
Accept: application/{json, xml}

HTTP STATUS: 
   200 OK
   404 NOT FOUND : Si no se encuentra


-----------------------------------------------------------------------------------------------------------------------


## GRABAR NUEVA ENTIDAD (CREATE)

POST http://localhost:8080/ws.entidades-negocio/api/entidades-negocio/
Content-Type: application/json
Accept: application/json
PAYLOAD JSON:
{
    "descripcion": "Descripcion de la entidad de negocio",
    "prioridad": 27
}
PAYLOAD XML:
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<entidadNegocio>
    <descripcion>Descripcion de la entidad de negocio</descripcion>
    <prioridad>27</prioridad>
</entidadNegocio>

HTTP STATUS:
   200 OK   y la entidad registrada en el Payload (con la correspondiente PK de la nueva entidad)
   400 BAD REQUEST si algun valor es incorrecto o falta

-----------------------------------------------------------------------------------------------------------------------


## ACTUALIZAR ENTIDAD (UPDATE)

PUT http://localhost:8080/ws.entidades-negocio/api/entidades-negocio/{id}
Content-Type: application/json
Accept: application/json
PAYLOAD JSON:
{
    "descripcion": "Descripcion de la entidad de negocio (actualizada)",
    "prioridad": 28
}
PAYLOAD XML:
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<entidadNegocio>
    <descripcion>Descripcion de la entidad de negocio (actualizada)</descripcion>
    <prioridad>28</prioridad>
</entidadNegocio>

HTTP STATUS:
   200 OK   y la entidad actualizada en el Payload
   400 BAD REQUEST si algun valor es incorrecto o falta


-----------------------------------------------------------------------------------------------------------------------


## ELIMINAR ENTIDAD (DELETE)

DELETE http://localhost:8080/ws.entidades-negocio/api/entidades-negocio/{id}

HTTP STATUS:
   204 OK (NO CONTENT)
   404 NOT FOUND : Si no se encuentra
